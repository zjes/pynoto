#!/usr/bin/env python3
import sys, os
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__))+"/srv.zip")
from srv.server import Server
from srv.rope.RopeNoto import Rope
from srv.pylint.PyLintNoto import PyLint

def main():
    srv = Server()
    srv.registerInstance(Rope())
    srv.registerInstance(PyLint())
    srv.run()
    
if __name__ == "__main__":
    main()