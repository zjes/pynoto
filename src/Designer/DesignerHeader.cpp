#include "DesignerHeader.h"
#include "Include/IIconProvider.h"
#include "Include/IProjectPlugin.h"
#include "Include/IProject.h"
#include "Include/PynotoApplication.h"
#include "ui_DesignerHeader.h"
#include "Include/Aux.h"

namespace Designer {

DesignerHeader::DesignerHeader(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::DesignerHeader)
{
    _ui->setupUi(this);

    _ui->closeBtn->setIcon(Aux::icons()->icon("pclose"));
    connect(_ui->closeBtn, SIGNAL(clicked()), SIGNAL(closeEditor()));
    connect(_ui->editWidgetsBtn, SIGNAL(clicked()), SIGNAL(switchWidgets()));
    connect(_ui->editTabOrderBtn, SIGNAL(clicked()), SIGNAL(switchTabOrder()));
    connect(_ui->editBuddiesBtn, SIGNAL(clicked()), SIGNAL(switchBuddies()));
    _ui->modifyLbl->setVisible(false);
    _ui->editWidgetsBtn->setIcon(Aux::icons()->icon("widgettool"));
    _ui->editTabOrderBtn->setIcon(Aux::icons()->icon("tabordertool"));
    _ui->editBuddiesBtn->setIcon(Aux::icons()->icon("buddytool"));
}

DesignerHeader::~DesignerHeader()
{
    delete _ui;
}

void DesignerHeader::setFileName(const QString & fileName)
{
    Project::IProjectPlugin *plug = Aux::plugin<Project::IProjectPlugin*>("project");
    if (plug){
        _ui->fileName->setText(plug->project()->fileDisplayName(fileName));
    } else {
        _ui->fileName->setText(fileName);
    }
}

void DesignerHeader::modificationChanged(bool modify)
{
    _ui->modifyLbl->setVisible(modify);
}

}
