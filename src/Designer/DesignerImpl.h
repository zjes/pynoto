#ifndef _DESIGNERIMPL_H_
#define _DESIGNERIMPL_H_

#include "Include/IEditor.h"

namespace Designer {

class DesignerWidget;
class DesignerImpl: public Editors::IEditor
{
    Q_OBJECT
    Q_INTERFACES(Editors::IEditor)
public:
    DesignerImpl(const QString& fileName, QWidget* parent, bool sample = false);
    virtual ~DesignerImpl();

    virtual bool isModified();
    virtual void updateStatus();
    virtual QString fileName();
    virtual QString selectedText();
    virtual void saveState();
    virtual QAbstractTableModel* errorListModel();
protected:
    void focusInEvent(QFocusEvent *);
public slots:
    virtual bool save();
    virtual bool load();
    virtual bool reload();

    virtual void undo();
    virtual void redo();
    virtual void copy();
    virtual void cut();
    virtual void paste();
    virtual void del();
    virtual void reset();
    virtual void checkIssues();
protected:
    DesignerWidget *_widget;
};

}

#endif
