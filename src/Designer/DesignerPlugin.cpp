#include <QtPlugin>
#include "DesignerPlugin.h"
#include "DesignerImpl.h"
#include "DesignerWidget.h"

namespace Designer {

bool DesignerPlugin::initialize()
{
    initialized = true;
    return true;
}

Editors::IEditor * DesignerPlugin::createEditor(const QString& fileName, QWidget* parent, bool sample)
{
    return new DesignerImpl(fileName, parent, sample);
}

void DesignerPlugin::renderPyFile(Project::IProject * project, const QString& fileName)
{
    DesignerWidget::renderPyFile(project, fileName);
}

}

