#ifndef _DESIGNERPLUGIN_H_
#define _DESIGNERPLUGIN_H_

#include "Include/IDesignerPlugin.h"

namespace Editors { class IEditor; }
namespace Designer {

class DesignerPlugin: public Designer::IDesignerPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IDesignerPlugin")
    Q_INTERFACES(Designer::IDesignerPlugin)
public:
    virtual ~DesignerPlugin(){}
    virtual bool initialize();
public:
    virtual Editors::IEditor * createEditor(const QString& fileName, QWidget* parent, bool sample = false);
    virtual void renderPyFile(Project::IProject * project, const QString& fileName);
};

}
#endif
