#ifndef _DESIGNERWIDGET_H_
#define _DESIGNERWIDGET_H_

#include <QWidget>
#include <QAbstractButton>

namespace Ui {
    class DesignerWidget;
}

#if QT_VERSION >= 0x050000
    class QDesignerFormWindowInterface;
    class QDesignerIntegration;
#else
namespace qdesigner_internal {
    class QDesignerIntegration;
    class FormWindowBase;
}
class QDesignerFormWindowInterface;
class QDesignerFormEditorInterface;
#endif

class QAbstractTableModel;
class StatusBar;
class QDesignerWidgetBoxInterface;
class QDesignerObjectInspectorInterface;
class QDesignerPropertyEditorInterface;
class QDesignerActionEditorInterface;

namespace Project { class IProject; }

namespace Designer
{

class DesignerWidget : public QWidget
{
    Q_OBJECT
public:
    DesignerWidget(const QString& fileName, QWidget *parent = 0);
    virtual ~DesignerWidget();
public:
    void init();
    QWidget* header();
    QString fileName();
    virtual bool save();
    virtual bool load();
    virtual bool reload();
    bool isModified();

    bool isUndoAvailable();
    bool isRedoAvailable();
    bool canCopy();
    bool canPaste();

    void activate();
    static void renderPyFile(Project::IProject * project, const QString& fileName);
public slots:
    void undo();
    void redo();
    void copy();
    void cut();
    void paste();
    void del();
signals:
    void modificationChanged(bool);

protected:
    void showEvent(QShowEvent *);
private slots:
    void onUndoAvailable();
    void onRedoAvailable();
    void onCopyAvailable();
    void onPasteAvailable();

    void onFormChanged();
    void onSwitchWidgets();
    void onSwitchTabOrder();
    void onSwitchBuddies();
    void onNavigateToSlot(const QString&, const QString& , const QStringList&);
signals:
    void copyAvailable(bool);
    void undoAvailable(bool);
    void redoAvailable(bool);
    void pasteAvailable(bool);
    void severityChanged(int sev);
    void errorListChanged(QAbstractTableModel*);
    void activated(QDesignerFormWindowInterface*);
private:
    Ui::DesignerWidget *_ui;
#if QT_VERSION >= 0x050000
    QDesignerIntegration *_designer;
#else
    qdesigner_internal::QDesignerIntegration *_designer;
#endif
    QAction *_editTabAction;
    QAction *_editBuddies;
    QString _fileName;
#if QT_VERSION >= 0x050000
    QDesignerFormWindowInterface *_form;
#else
    qdesigner_internal::FormWindowBase *_form;
#endif
    QObject * _menu;

    QDesignerWidgetBoxInterface *_wb;
    QDesignerObjectInspectorInterface *_oi;
    QDesignerPropertyEditorInterface * _pe;
    QDesignerActionEditorInterface *_ae;

    Project::IProject * _project;
};

}

#endif
