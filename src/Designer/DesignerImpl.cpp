#include <QVBoxLayout>
#include <QDebug>
#include "DesignerImpl.h"
#include "DesignerWidget.h"

namespace Designer {

DesignerImpl::DesignerImpl(const QString& fileName, QWidget* parent, bool):
    IEditor(parent),
    _widget(new DesignerWidget(fileName))
{
    _widget->init();
    QVBoxLayout *box = new QVBoxLayout(this);
    setLayout(box);
    QWidget *header = _widget->header();
    layout()->setContentsMargins(0, 0, 0, 0);
    layout()->setSpacing(0);
    layout()->addWidget(header);
    connect(header, SIGNAL(closeEditor()), SIGNAL(closeEditor()));
    layout()->addWidget(_widget);

    connect(_widget, SIGNAL(copyAvailable(bool)), this, SIGNAL(copyAvailable(bool)));
    connect(_widget, SIGNAL(pasteAvailable(bool)), this, SIGNAL(pasteAvailable(bool)));
    connect(_widget, SIGNAL(redoAvailable(bool)), this, SIGNAL(redoAvailable(bool)));
    connect(_widget, SIGNAL(undoAvailable(bool)), this, SIGNAL(undoAvailable(bool)));
    connect(_widget, SIGNAL(modificationChanged(bool)), this, SIGNAL(modificationChanged(bool)));
    connect(_widget, SIGNAL(errorListChanged(QAbstractTableModel*)), this, SIGNAL(errorListChanged(QAbstractTableModel*)));
}

DesignerImpl::~DesignerImpl()
{

}

bool DesignerImpl::save()
{ return _widget->save(); }

bool DesignerImpl::load()
{ return _widget->load(); }

bool DesignerImpl::reload()
{ return _widget->reload(); }

void DesignerImpl::undo()
{ _widget->undo(); }

void DesignerImpl::redo()
{ _widget->redo(); }

void DesignerImpl::copy()
{ _widget->copy(); }

void DesignerImpl::cut()
{ _widget->cut(); }

void DesignerImpl::paste()
{ _widget->paste(); }

void DesignerImpl::del()
{ _widget->del(); }

bool DesignerImpl::isModified()
{
    return _widget->isModified();
}

void DesignerImpl::updateStatus()
{
    emit undoAvailable(_widget->isUndoAvailable());
    emit redoAvailable(_widget->isRedoAvailable());
    emit modificationChanged(_widget->isModified());
    emit copyAvailable(_widget->canCopy());
    emit pasteAvailable(_widget->canPaste());
    _widget->activate();
}

void DesignerImpl::focusInEvent(QFocusEvent *)
{
    _widget->setFocus();
    _widget->activate();
}

QString DesignerImpl::fileName()
{
    return _widget->fileName();
}

void DesignerImpl::saveState()
{
}

QAbstractTableModel* DesignerImpl::errorListModel()
{
    return NULL;
}

void DesignerImpl::reset()
{
}

void DesignerImpl::checkIssues()
{}

QString DesignerImpl::selectedText()
{
    return "";
}

}
