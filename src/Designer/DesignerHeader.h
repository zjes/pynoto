#ifndef _DESIGNERHEADER_H_
#define _DESIGNERHEADER_H_

#include <QWidget>

namespace Ui {
    class DesignerHeader;
}

namespace Designer {

class DesignerHeader : public QWidget
{
    Q_OBJECT
public:
    explicit DesignerHeader(QWidget *parent = 0);
    virtual ~DesignerHeader();

    void setFileName(const QString & fileName);
signals:
    void switchWidgets();
    void switchTabOrder();
    void switchBuddies();
    void closeEditor();
public slots:
    void modificationChanged(bool);
private:
    Ui::DesignerHeader *_ui;
};

}

#endif
