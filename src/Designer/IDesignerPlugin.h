#ifndef _IDESIGNERPLUGIN_H_
#define _IDESIGNERPLUGIN_H_

#include "Include/IEditorPlugin.h"

namespace Project {
class IProject;
}
namespace Designer {

class IDesignerPlugin : public Editors::IEditorPlugin
{
    Q_OBJECT
public:
    virtual ~IDesignerPlugin();
    virtual void renderPyFile(Project::IProject * project, const QString& fileName) = 0;
signals:
    
public slots:
    
};

}
Q_DECLARE_INTERFACE(Designer::IDesignerPlugin, "Pynoto.IDesignerPlugin")
#endif
