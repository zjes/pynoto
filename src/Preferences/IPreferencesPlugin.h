#ifndef _IPREFERENCESPLUGIN_H_
#define _IPREFERENCESPLUGIN_H_
#include "Include/IPlugin.h"

namespace Preferences {

class IPreferencesPlugin: public IPlugin
{
    Q_OBJECT
public:
    virtual ~IPreferencesPlugin(){}
signals:
    void changed();
public slots:
    virtual void editPreferences(QWidget * parent) = 0;
};

}

Q_DECLARE_INTERFACE(Preferences::IPreferencesPlugin, "Pynoto.IPreferencesPlugin")

#endif
