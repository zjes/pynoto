#ifndef _PREFERENCESWINDOW_H_
#define _PREFERENCESWINDOW_H_
#include <QDialog>
#include "Include/IPreferencesPage.h"

namespace Ui {
    class PreferencesWindow;
}
class QAbstractButton;
class QListWidgetItem;

namespace Preferences {

class PreferencesWindow: public QDialog
{
    Q_OBJECT
public:
    PreferencesWindow(QWidget * parent);
    virtual ~PreferencesWindow();

    void addPage(IPreferencesPage *page);
signals:
    void changed();
protected:
    virtual void showEvent(QShowEvent *);
private slots:
    void onBtnClicked(QAbstractButton*);
    void onPageChanged(QListWidgetItem*);
    bool save();
private:
    Ui::PreferencesWindow *_ui;
};

}
#endif
