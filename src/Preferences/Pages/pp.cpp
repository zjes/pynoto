#include "MimeTypes.h"
#include "ui_MimeTypes.h"

namespace Preferences {

MimeTypes::MimeTypes(QWidget * parent):
    IPreferencesPage(parent),
    _ui(new Ui::MimeTypes)
{
    _ui->setupUi(this);
    init();
}

MimeTypes::~MimeTypes()
{
    delete _ui;
}

QString MimeTypes::title()
{
    return tr("Mime types");
}

QIcon MimeTypes::icon()
{
    return QIcon();
}

bool MimeTypes::save()
{
    return true;
}

void MimeTypes::init()
{

}

}
