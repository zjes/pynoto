#ifndef _MIMETYPES_H_
#define _MIMETYPES_H_
#include "IPreferencesPage.h"

namespace Ui {
    class MimeTypes;
}

namespace Preferences {

class MimeTypes: public IPreferencesPage
{
    Q_OBJECT
public:
    MimeTypes(QWidget * parent = NULL);
    virtual ~MimeTypes();

    virtual QString title();
    virtual QIcon icon();
    virtual bool save();
private:
    void init();
private:
    Ui::MimeTypes * _ui;
};

}
#endif
