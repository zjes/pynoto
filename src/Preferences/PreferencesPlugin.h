#ifndef _PREFERENCESPLUGIN_H_
#define _PREFERENCESPLUGIN_H_

#include "IPreferencesPlugin.h"

namespace Preferences {

class PreferencesPlugin: public IPreferencesPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IPreferencesPlugin")
    Q_INTERFACES(Preferences::IPreferencesPlugin)
public:
    virtual ~PreferencesPlugin(){}
    virtual bool initialize();
public slots:
    virtual void editPreferences(QWidget *parent);
};
}
#endif
