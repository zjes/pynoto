#include <QtPlugin>
#include <QApplication>
#include "Include/Aux.h"
#include "PreferencesPlugin.h"
#include "PreferencesWindow.h"

namespace Preferences {

bool PreferencesPlugin::initialize()
{
    initialized = true;
    return true;
}

void PreferencesPlugin::editPreferences(QWidget *parent)
{
    PreferencesWindow wnd(parent);

    QMultiMap<int, IPreferencesPage*> map;
    foreach(IPlugin * p, Aux::manager()->plugins()){
        foreach(IPreferencesPage* page, p->preferences()){
            map.insert(page->order(), page);
        }
    }
    QMultiMap<int, IPreferencesPage*>::const_iterator it = map.constBegin();
    while (it != map.constEnd()){
        wnd.addPage(it.value());
        ++it;
    }
    wnd.exec();
}

}
