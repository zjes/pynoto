#include <QDebug>
#include <QAbstractButton>
#include "Include/Aux.h"
#include "Include/IIconProvider.h"
#include "PreferencesWindow.h"
#include "ui_PreferencesWindow.h"

namespace Preferences {

PreferencesWindow::PreferencesWindow(QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::PreferencesWindow)
{
    _ui->setupUi(this);
    connect(_ui->buttonBox, SIGNAL(clicked(QAbstractButton*)), SLOT(onBtnClicked(QAbstractButton*)));
    connect(_ui->items, SIGNAL(itemActivated(QListWidgetItem*)), SLOT(onPageChanged(QListWidgetItem*)));

    //connect(ui->buttonBox, SIGNAL(clicked(QAbstractButton*)), SLOT(doBoxAction(QAbstractButton*)));
}

PreferencesWindow::~PreferencesWindow()
{
    delete _ui;
}

void PreferencesWindow::addPage(IPreferencesPage *page)
{
    int it = _ui->place->addWidget(page);
    QIcon ico = page->icon();
    if (ico.isNull()){
        ico = Aux::icons()->icon("emblem-system");
    }
    QListWidgetItem * item = new QListWidgetItem(ico, page->title());
    item->setData(Qt::UserRole, it);
    _ui->items->addItem(item);
    connect(page, SIGNAL(changed()), SIGNAL(changed()));
}

void PreferencesWindow::showEvent(QShowEvent *)
{
    _ui->items->setCurrentRow(0);
}

bool PreferencesWindow::save()
{
    bool result = true;
    for(int i = 0; i < _ui->place->count(); ++i){
        IPreferencesPage* page = dynamic_cast<IPreferencesPage*>(_ui->place->widget(i));
        result &= page->save();
    }
    return result;
}

void PreferencesWindow::onBtnClicked(QAbstractButton* btn)
{
    switch(_ui->buttonBox->buttonRole(btn)){
    case QDialogButtonBox::AcceptRole:
        if (save())
            accept();
        break;
    case QDialogButtonBox::RejectRole:
        reject();
        break;
    case QDialogButtonBox::ApplyRole:
        save();
        break;
    default:
        break;
    }
}

void PreferencesWindow::onPageChanged(QListWidgetItem* it)
{
    _ui->place->setCurrentIndex(it->data(Qt::UserRole).toInt());
}

}

