find_package(Qt5Widgets)

set(SRC
    IPreferencesPlugin.h
    PreferencesWindow.h
    PreferencesWindow.cpp
    PreferencesPlugin.h
    PreferencesPlugin.cpp
)

set(UI
    PreferencesWindow.ui
)

qt5_wrap_ui(UIC ${UI})

add_library(preferences
    SHARED
    ${SRC}
    ${UIC}
)

target_link_libraries(preferences core)
qt5_use_modules(preferences Widgets)
