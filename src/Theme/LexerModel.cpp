#include <QDebug>
#include <QApplication>
#include <QPalette>
#include "LexerModel.h"
#include "Include/ITheme.h"

namespace Theme {

LexerModel::LexerModel(ITheme* theme, const QString& lexer, QObject* parent):
    QAbstractListModel(parent),
    _theme(theme),
    _lexer(lexer)
{
}

int LexerModel::rowCount(const QModelIndex &) const
{
    return _names.length();
}

QVariant LexerModel::data(const QModelIndex & index, int role) const
{
    if (!index.isValid())
        return QVariant();

    switch(role) {
    case BackgroundRole:
        return _backgrounds[index.row()];
    case ForegroundRole:
        return _colors[index.row()];
    case FontRole: {
        QFont font(QApplication::font());
        font.setBold(_bold[index.row()]);
        font.setItalic(_italic[index.row()]);
        font.setUnderline(_underline[index.row()]);
        return font;
    }
    case DisplayRole:
        return _titles[index.row()];
    case BoldRole:
        return _bold[index.row()];
    case ItalicRole:
        return _italic[index.row()];
    case UnderscoreRole:
        return _underline[index.row()];
    case BTransparentRole:
        return _backgrounds[index.row()].alpha() == 0;
    }
    return QVariant();
}

void LexerModel::setColor(const QModelIndex& index, const QColor& color)
{
    _colors[index.row()] = color;
    _theme->setColor(_lexer, _names[index.row()], color);
    emit dataChanged(index, index);
}

void LexerModel::setBackground(const QModelIndex& index, const QColor& color)
{
    _backgrounds[index.row()] = color;
    _theme->setBackground(_lexer, _names[index.row()], color);
    emit dataChanged(index, index);
}

void LexerModel::setTransparent(const QModelIndex& index, bool on)
{
    if (on)
        setBackground(index, Qt::transparent);
}

void LexerModel::setBold(const QModelIndex& index, bool on)
{
    _bold[index.row()] = on;
    _theme->setBold(_lexer, _names[index.row()], on);
    emit dataChanged(index, index);
}

void LexerModel::setItalic(const QModelIndex& index, bool on)
{
    _italic[index.row()] = on;
    _theme->setItalic(_lexer, _names[index.row()], on);
    emit dataChanged(index, index);
}

void LexerModel::setUnderline(const QModelIndex& index, bool on)
{
    _underline[index.row()] = on;
    _theme->setUnderline(_lexer, _names[index.row()], on);
    emit dataChanged(index, index);
}

void LexerModel::save()
{
    for(int i = 0; i < rowCount(); ++i){
        _theme->setColor(_lexer, _names[i], _colors[i]);
        _theme->setBackground(_lexer, _names[i], _backgrounds[i]);
        _theme->setBold(_lexer, _names[i], _bold[i]);
        _theme->setItalic(_lexer, _names[i], _italic[i]);
        _theme->setUnderline(_lexer, _names[i], _underline[i]);
    }
}

}
