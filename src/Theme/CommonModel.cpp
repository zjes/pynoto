#include <QApplication>
#include <QPalette>
#include "CommonModel.h"
#include "Include/ITheme.h"

namespace Theme {

CommonModel::CommonModel(ITheme* theme, QObject* parent):
    QAbstractListModel(parent),
    _theme(theme)
{
    _names << "dsNormal" << "dsMargine" << "dsHightlight";

    QPalette pal = QApplication::palette();

    QColor mainColor = _theme->color("common", "dsNormal", pal.color(QPalette::WindowText));
    _colors << mainColor
            << _theme->color("common", "dsMargine", pal.color(QPalette::Text))
            << mainColor;

    _backgrounds << _theme->background("common", "dsNormal", pal.color(QPalette::Base))
                 << _theme->background("common", "dsMargine", pal.color(QPalette::Window))
                 << _theme->background("common", "dsHightlight", pal.color(QPalette::AlternateBase));
}

int CommonModel::rowCount(const QModelIndex &) const
{
    return 3;
}

QVariant CommonModel::data(const QModelIndex & index, int role) const
{
    if (!index.isValid())
        return QVariant();

    QPalette pal = QApplication::palette();
    switch(role) {
    case Qt::BackgroundRole:
        return _backgrounds[index.row()];
    case Qt::ForegroundRole:
        return _colors[index.row()];
    case Qt::DisplayRole:
        switch(index.row()){
        case 0: return tr("Text");
        case 1: return tr("Margine");
        case 2: return tr("Current line");
        }
    }
    return QVariant();
}

void CommonModel::setColor(const QModelIndex& index, int role, const QColor& color)
{
    if (role == Qt::ForegroundRole && index.row() != 2){
        _colors[index.row()] = color;
        if (index.row() == 0)
            _colors[2] = color;
        _theme->setColor("common", _names[index.row()], color);
    } else if (role == Qt::BackgroundRole) {
        _backgrounds[index.row()] = color;
        _theme->setBackground("common", _names[index.row()], color);
    }
}

void CommonModel::save()
{
    for(int i = 0; i < rowCount(); ++i){
        _theme->setColor("common", _names[i], _colors[i]);
        _theme->setBackground("common", _names[i], _backgrounds[i]);
    }
}

}
