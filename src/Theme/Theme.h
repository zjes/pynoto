#ifndef _THEME_H_
#define _THEME_H_
#include <QSettings>
#include "ITheme.h"


namespace Theme {

class ThemeImp: public ITheme
{
    Q_OBJECT
public:
    ThemeImp(const QString& name, bool temp = false);
    virtual ~ThemeImp();

    virtual QTextCharFormat format(const QString& lexer, const QString& role, const QColor& defColor = Qt::black, bool bold = false);
    virtual QTextCharFormat defFormat(const QString& lexer, const QString& role, const QString& defRole = "dsNormal", bool bold = false);

    virtual QColor color(const QString& lexer, const QString& role, const QColor& def = Qt::black);
    virtual QColor background(const QString& lexer, const QString& role, const QColor& def = Qt::transparent);

    virtual void setColor(const QString& lexer, const QString& role, const QColor& color);
    virtual void setBackground(const QString& lexer, const QString& role, const QColor& color);

    virtual bool italic(const QString& lexer, const QString& role, bool def = false);
    virtual bool bold(const QString& lexer, const QString& role, bool def = false);
    virtual bool underline(const QString& lexer, const QString& role, bool def = false);

    virtual void setItalic(const QString& lexer, const QString& role, bool on);
    virtual void setBold(const QString& lexer, const QString& role, bool on);
    virtual void setUnderline(const QString& lexer, const QString& role, bool on);

    virtual QString themePath()
    { return _themePath; }

    virtual void save();
private:
    QString _name;
    QSettings *_theme;
    struct ThemeData{
        QString roleName;
        QColor color;
        QColor background;
        bool bold;
        bool italic;
        bool underline;
        ThemeData():
            roleName("unknown"), color(Qt::black), background(Qt::transparent), bold(false), italic(false), underline(false)
        {}
        ThemeData(const QString& name, const QColor& forecolor, const QColor& backcolor = Qt::transparent):
            roleName(name), color(forecolor), background(backcolor), bold(false), italic(false), underline(false)
        {}
        ThemeData(const QString& name, const QColor& forecolor, bool isbold, bool isitalic = false, bool isuderline = false):
            roleName(name), color(forecolor), background(Qt::transparent), bold(isbold), italic(isitalic), underline(isuderline)
        {}
    };

    inline QString colorName(const QColor& color);
    QString _themePath;
};

}

#endif
