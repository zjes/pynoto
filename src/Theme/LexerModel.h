#ifndef _LEXERMODEL_H_
#define _LEXERMODEL_H_
#include <QAbstractListModel>
#include <QStringList>
#include <QColor>

namespace Theme {
class ITheme;

enum LexerRoles {
    DisplayRole = Qt::DisplayRole,
    BackgroundRole = Qt::BackgroundRole,
    ForegroundRole = Qt::ForegroundRole,
    FontRole = Qt::FontRole,
    BTransparentRole = Qt::UserRole + 1,
    BoldRole = Qt::UserRole + 2,
    ItalicRole = Qt::UserRole + 3,
    UnderscoreRole = Qt::UserRole + 4
};

class LexerModel : public QAbstractListModel
{
    Q_OBJECT
public:
    LexerModel(ITheme* theme, const QString& lexer, QObject* parent = NULL);
    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = DisplayRole) const;

    void setColor(const QModelIndex& index, const QColor& color);
    void setBackground(const QModelIndex& index, const QColor& color);

    void setTransparent(const QModelIndex& index, bool on);
    void setBold(const QModelIndex& index, bool on);
    void setItalic(const QModelIndex& index, bool on);
    void setUnderline(const QModelIndex& index, bool on);

    virtual void save();
protected:
    ITheme* _theme;
    QString _lexer;
    QStringList _names;
    QStringList _titles;
    QList<QColor> _colors;
    QList<QColor> _backgrounds;
    QList<bool> _bold;
    QList<bool> _italic;
    QList<bool> _underline;
};

}

#endif
