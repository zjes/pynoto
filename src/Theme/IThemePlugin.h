#ifndef _ITHEMEPLUGIN_H_
#define _ITHEMEPLUGIN_H_

#include "Include/IPlugin.h"
#include "Include/ITheme.h"

namespace Theme {

class IThemePlugin: public IPlugin
{
    Q_OBJECT
public:
    virtual ~IThemePlugin(){}
    virtual ITheme * theme(const QString& name) = 0;
    virtual QStringList themesList() = 0;
    virtual bool editTheme(const QString& name) = 0;
    virtual QString copyTheme(const QString& themeName) = 0;
    virtual void deleteTheme(const QString& themeName) = 0;
};

}

Q_DECLARE_INTERFACE(Theme::IThemePlugin, "Pynoto.IThemePlugin")

#endif
