#include <QDebug>
#include <QApplication>
#include <QPalette>
#include <QDir>
#include <QStringBuilder>
#include "Theme.h"

namespace Theme {

ThemeImp::ThemeImp(const QString& name, bool temp):
    ITheme(),
    _name(name)
{
    QSettings set;
    QDir dir(set.fileName());
    dir.cdUp();
    dir.cd("themes");
    _themePath = dir.filePath(name+".conf"+(temp ? "~" : ""));
    _theme = new QSettings(_themePath, QSettings::IniFormat);
}

ThemeImp::~ThemeImp()
{
    delete _theme;
}

QTextCharFormat ThemeImp::format(const QString& lexer, const QString& role, const QColor& defColor, bool isbold)
{
    QTextCharFormat fmt;
    fmt.setForeground(color(lexer, role, defColor));
    fmt.setBackground(background(lexer, role));
    fmt.setFontWeight(bold(lexer, role) ? QFont::Bold: QFont::Normal);
    fmt.setFontItalic(italic(lexer, role));
    fmt.setFontUnderline(underline(lexer, role));
    return fmt;
}

QTextCharFormat ThemeImp::defFormat(const QString& lexer, const QString& role, const QString& defRole, bool isbold)
{
    QString fromLexer = lexer;
    QString fromRole = defRole;
    QStringList defs = defRole.split("/");
    if (defs.length() > 1){
        fromLexer = defs[0];
        fromRole = defs[1];
    }
    QTextCharFormat def = format(fromLexer, fromRole, Qt::black);
    QTextCharFormat fmt;
    fmt.setForeground(color(lexer, role, def.foreground().color()));
    fmt.setBackground(background(lexer, role, def.background().color()));
    fmt.setFontWeight(bold(lexer, role, isbold) ? QFont::Bold: QFont::Normal);
    fmt.setFontItalic(italic(lexer, role, def.fontItalic()));
    fmt.setFontUnderline(underline(lexer, role, def.fontUnderline()));
    return fmt;
}

QColor ThemeImp::color(const QString& lexer, const QString& role, const QColor& def)
{
    return QColor(_theme->value(lexer % "/" % role % "/color", colorName(def)).toString());
}

QColor ThemeImp::background(const QString& lexer, const QString& role, const QColor& def)
{
    return QColor(_theme->value(lexer % "/" % role % "/background", colorName(def)).toString());
}

QString ThemeImp::colorName(const QColor& color)
{
    if (color.alpha() == 0)
        return "transparent";
    return color.name();
}

void ThemeImp::setColor(const QString& lexer, const QString& role, const QColor& color)
{
    _theme->setValue(lexer % "/" % role % "/color", colorName(color));
}

void ThemeImp::setBackground(const QString& lexer, const QString& role, const QColor& color)
{
    QString name = colorName(color);
    if (name == "transparent")
        _theme->remove(lexer % "/" % role % "/background");
    else
        _theme->setValue(lexer % "/" % role % "/background", name);
}

bool ThemeImp::italic(const QString& lexer, const QString& role, bool def)
{
    return _theme->value(lexer % "/" % role % "/italic", def).toBool();
}

bool ThemeImp::bold(const QString& lexer, const QString& role, bool def)
{
    return _theme->value(lexer % "/" % role % "/bold", def).toBool();
}

bool ThemeImp::underline(const QString& lexer, const QString& role, bool def)
{
    return _theme->value(lexer % "/" % role % "/underline", def).toBool();
}

void ThemeImp::setItalic(const QString& lexer, const QString& role, bool on)
{
    if (!on)
        _theme->remove(lexer % "/" % role % "/italic");
    else
        _theme->setValue(lexer % "/" % role % "/italic", on);
}

void ThemeImp::setBold(const QString& lexer, const QString& role, bool on)
{
    if (!on)
        _theme->remove(lexer % "/" % role % "/bold");
    else
        _theme->setValue(lexer % "/" % role % "/bold", on);
}

void ThemeImp::setUnderline(const QString& lexer, const QString& role, bool on)
{
    if (!on)
        _theme->remove(lexer % "/" % role % "/underline");
    else
        _theme->setValue(lexer % "/" % role % "/underline", on);
}

void ThemeImp::save()
{
    _theme->sync();
}

}
