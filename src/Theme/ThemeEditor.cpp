#include <QDebug>
#include "ThemeEditor.h"
#include "ITheme.h"
#include "ui_ThemeEditor.h"
#include "CommonModel.h"
#include "DefaultModel.h"
#include "PythonModel.h"

namespace Theme {

ThemeEditor::ThemeEditor(ITheme* theme, QWidget* parent):
    QDialog(parent),
    _theme(theme),
    _ui(new Ui::ThemeEditor)
{
    _ui->setupUi(this);

    _ui->lexerView->setAlternatingRowColors(false);

    _lexers.insert(tr("Default"), new DefaultModel(_theme, this));
    _lexers.insert(tr("Python"), new PythonModel(_theme, this));
    foreach(QString lex, _lexers.keys()){
        _ui->lstLexers->addItem(lex);
    }

    connect(_ui->lstLexers, SIGNAL(activated(QString)), SLOT(onLexerActivated(QString)));
    _ui->lstLexers->setCurrentIndex(0);

    _ui->common->setItemDelegate(new ColorItemDelegate(this));
    _ui->lexerView->setItemDelegate(new ColorItemDelegate(this));

    _ui->ceditGroup->setEnabled(false);
    _ui->editGroup->setEnabled(false);

    _ui->common->setModel(new CommonModel(_theme, this));
    _ui->lexerView->setModel(_lexers[tr("Default")]);

    connect(_ui->common, SIGNAL(activated(QModelIndex)), SLOT(onCommonActivated(QModelIndex)));
    connect(_ui->lexerView, SIGNAL(activated(QModelIndex)), SLOT(onLexerActivated(QModelIndex)));

    connect(_ui->cforeground, SIGNAL(changed(QColor)), SLOT(onCommonForeChanged(QColor)));
    connect(_ui->cbackground, SIGNAL(changed(QColor)), SLOT(onCommonBackChanged(QColor)));

    connect(_ui->foreground, SIGNAL(changed(QColor)), SLOT(onLexerForeChanged(QColor)));
    connect(_ui->background, SIGNAL(changed(QColor)), SLOT(onLexerBackChanged(QColor)));
    connect(_ui->transparent, SIGNAL(clicked(bool)), SLOT(onTransparentChanged(bool)));
    connect(_ui->bold, SIGNAL(clicked(bool)), SLOT(onBoldChanged(bool)));
    connect(_ui->italic, SIGNAL(clicked(bool)), SLOT(onItalicChanged(bool)));
    connect(_ui->underline, SIGNAL(clicked(bool)), SLOT(onUnderlineChanged(bool)));
    connect(_ui->buttonBox, SIGNAL(accepted()), SLOT(onAcceped()));

    CommonModel *mod = qobject_cast<CommonModel*>(_ui->common->model());
    QPalette pal = _ui->lexerView->palette();
    pal.setColor(QPalette::Base, mod->data(mod->index(0), Qt::BackgroundRole).value<QColor>());
    pal.setColor(QPalette::AlternateBase, mod->data(mod->index(0), Qt::BackgroundRole).value<QColor>());
    _ui->lexerView->setPalette(pal);
}

ThemeEditor::~ThemeEditor()
{
    delete _ui;
}

void ThemeEditor::onLexerActivated(const QString& name)
{
    _ui->lexerView->setModel(_lexers[name]);
}

void ThemeEditor::onCommonActivated(const QModelIndex& index)
{
    _ui->ceditGroup->setEnabled(index.isValid());
    _ui->cforeground->setColor(index.data(Qt::ForegroundRole).value<QColor>());
    _ui->cbackground->setColor(index.data(Qt::BackgroundRole).value<QColor>());
    _ui->cforeLabel->setEnabled(index.row() != 2);
    _ui->cforeground->setEnabled(index.row() != 2);
}

void ThemeEditor::onCommonForeChanged(const QColor& color)
{
    qobject_cast<CommonModel*>(_ui->common->model())->setColor(_ui->common->currentIndex(), Qt::ForegroundRole, color);
}

void ThemeEditor::onCommonBackChanged(const QColor& color)
{
    qobject_cast<CommonModel*>(_ui->common->model())->setColor(_ui->common->currentIndex(), Qt::BackgroundRole, color);
    if (_ui->common->currentIndex().row() == 0) {
        QPalette pal = _ui->lexerView->palette();
        pal.setColor(QPalette::Base, color);
        pal.setColor(QPalette::AlternateBase, color);
        _ui->lexerView->setPalette(pal);
    }
}

void ThemeEditor::onLexerActivated(const QModelIndex& index)
{
    _ui->editGroup->setEnabled(index.isValid());
    _ui->foreground->setColor(index.data(ForegroundRole).value<QColor>());
    _ui->background->setColor(index.data(BackgroundRole).value<QColor>());
    _ui->transparent->setChecked(index.data(BTransparentRole).toBool());
    _ui->bold->setChecked(index.data(BoldRole).toBool());
    _ui->italic->setChecked(index.data(ItalicRole).toBool());
    _ui->underline->setChecked(index.data(UnderscoreRole).toBool());

    _ui->background->setEnabled(!index.data(BTransparentRole).toBool());
    _ui->backgroundLabel->setEnabled(!index.data(BTransparentRole).toBool());
}

void ThemeEditor::onLexerForeChanged(const QColor& color)
{
    qobject_cast<LexerModel*>(_ui->lexerView->model())->setColor(_ui->lexerView->currentIndex(), color);
}

void ThemeEditor::onLexerBackChanged(const QColor& color)
{
    qobject_cast<LexerModel*>(_ui->lexerView->model())->setBackground(_ui->lexerView->currentIndex(), color);
}

void ThemeEditor::onTransparentChanged(bool on)
{
    qobject_cast<LexerModel*>(_ui->lexerView->model())->setTransparent(_ui->lexerView->currentIndex(), on);

    _ui->background->setEnabled(!on);
    _ui->backgroundLabel->setEnabled(!on);

    if (!on){
        _ui->background->setColor(QColor("#dedede"));
        onLexerBackChanged(QColor("#dedede"));
    }
}

void ThemeEditor::onBoldChanged(bool on)
{
    qobject_cast<LexerModel*>(_ui->lexerView->model())->setBold(_ui->lexerView->currentIndex(), on);
}

void ThemeEditor::onItalicChanged(bool on)
{
    qobject_cast<LexerModel*>(_ui->lexerView->model())->setItalic(_ui->lexerView->currentIndex(), on);
}

void ThemeEditor::onUnderlineChanged(bool on)
{
    qobject_cast<LexerModel*>(_ui->lexerView->model())->setUnderline(_ui->lexerView->currentIndex(), on);
}

void ThemeEditor::onAcceped()
{
    qobject_cast<CommonModel*>(_ui->common->model())->save();
    foreach(LexerModel* model, _lexers.values()){
        model->save();
    }
    _theme->save();
    accept();
}

}
