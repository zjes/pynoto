#include <QDebug>
#include <QApplication>
#include <QPalette>
#include "DefaultModel.h"
#include "Include/ITheme.h"

namespace Theme {

DefaultModel::DefaultModel(ITheme* theme, QObject* parent):
    LexerModel(theme, "default", parent)
{
    _names << "dsKeyword" << "dsDataType" << "dsNumber" << "dsChar"
           << "dsString" << "dsComment" << "dsOthers" << "dsAlert"
           << "dsFunction" << "dsRegionMarker" << "dsError";

    _titles << tr("Keyword") << tr("Data type") << tr("Number")
            << tr("Char") << tr("String") << tr("Comment")
            << tr("Others") << tr("Alert") << tr("Function")
            << tr("Region marker") << tr("Error");

    _colors << _theme->color("default", "dsKeyword"     , QColor("#000000"))
            << _theme->color("default", "dsDataType"    , QColor("#0057AE"))
            << _theme->color("default", "dsNumber"      , QColor("#B08000"))
            << _theme->color("default", "dsChar"        , QColor("#FF80E0"))
            << _theme->color("default", "dsString"      , QColor("#BF0303"))
            << _theme->color("default", "dsComment"     , QColor("#898887"))
            << _theme->color("default", "dsOthers"      , QColor("#006E28"))
            << _theme->color("default", "dsAlert"       , QColor("#BF0303"))
            << _theme->color("default", "dsFunction"    , QColor("#644A9B"))
            << _theme->color("default", "dsRegionMarker", QColor("#DEDEDE"))
            << _theme->color("default", "dsError"       , QColor("#BF0303"));

    foreach(QString name, _names){
        _backgrounds << _theme->background("default", name);
        _bold << _theme->bold("default", name, name == "dsKeyword" ? true : false);
        _italic << _theme->italic("default", name);
        _underline << _theme->italic("default", name);
    }
}


}
