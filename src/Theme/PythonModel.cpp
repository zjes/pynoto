#include <QApplication>
#include <QPalette>
#include "PythonModel.h"
#include "Include/ITheme.h"

namespace Theme {

PythonModel::PythonModel(ITheme* theme, QObject* parent):
    LexerModel(theme, "python", parent)
{
    _names << "DefinitionKeyword" << "Operator" << "StringSubstitution"
           << "CommandKeyword" << "FlowControlKeyword"
           << "BuiltinFunction" << "SpecialVariable" << "Extensions"
           << "Exceptions" << "Overloaders" << "Preprocessor" << "StringChar"
           << "Number" << "Complex" << "Comment" << "String" << "RawString"
           << "Decorator" << "BlockRegion";

    _titles << tr("Definition Keyword") << tr("Operator") << tr("String Substitution")
            << tr("Command Keyword") << tr("Flow Control Keyword")
            << tr("Builtin Function") << tr("Special Variable") << tr("Extensions")
            << tr("Exceptions") << tr("Overloaders") << tr("Preprocessor") << tr("String Char")
            << tr("Number") << tr("Complex") << tr("Comment") << tr("String") << tr("Raw String")
            << tr("Decorator") << tr("BlockRegion");

    _colors << _theme->color("python", "DefinitionKeyword"  , QColor("#000000"))
            << _theme->color("python", "Operator"           , QColor("#000000"))
            << _theme->color("python", "StringSubstitution" , QColor("#0057ae"))
            << _theme->color("python", "CommandKeyword"     , QColor("#000000"))
            << _theme->color("python", "FlowControlKeyword" , QColor("#000000"))
            << _theme->color("python", "BuiltinFunction"    , QColor("#0057AE"))
            << _theme->color("python", "SpecialVariable"    , QColor("#006E28"))
            << _theme->color("python", "Extensions"         , QColor("#0095ff"))
            << _theme->color("python", "Exceptions"         , QColor("#054d00"))
            << _theme->color("python", "Overloaders"        , QColor("#000e52"))
            << _theme->color("python", "Preprocessor"       , QColor("#FF80E0"))
            << _theme->color("python", "StringChar"         , QColor("#FF80E0"))
            << _theme->color("python", "Number"             , QColor("#B08000"))
            << _theme->color("python", "Complex"            , QColor("#006E28"))
            << _theme->color("python", "Comment"            , QColor("#898887"))
            << _theme->color("python", "String"             , QColor("#BF0303"))
            << _theme->color("python", "RawString"          , QColor("#BF0303"))
            << _theme->color("python", "Decorator"          , QColor("#8f6b32"))
            << _theme->color("python", "BlockRegion"        , QColor("#DEDEDE"));

    QStringList bold;
    bold << "DefinitionKeyword" << "Operator" << "Extensions" << "Exceptions" << "Overloaders";
    foreach(QString name, _names){
        _backgrounds << _theme->background("python", name);
        _bold << _theme->bold("python", name, bold.contains(name));
        _italic << _theme->italic("python", name);
        _underline << _theme->italic("python", name);
    }
}


}
