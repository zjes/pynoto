#ifndef _DEFAULTMODEL_H_
#define _DEFAULTMODEL_H_
#include "LexerModel.h"

namespace Theme {
class ITheme;

class DefaultModel : public LexerModel
{
    Q_OBJECT
public:
    DefaultModel(ITheme* theme, QObject* parent = NULL);
};

}

#endif
