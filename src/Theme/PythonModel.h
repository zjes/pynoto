#ifndef _PYTHONMODEL_H_
#define _PYTHONMODEL_H_
#include "LexerModel.h"

namespace Theme {
class ITheme;

class PythonModel : public LexerModel
{
    Q_OBJECT
public:
    PythonModel(ITheme* theme, QObject* parent = NULL);
};

}

#endif
