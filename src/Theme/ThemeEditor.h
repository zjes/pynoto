#ifndef _THEMEEDITOR_H_
#define _THEMEEDITOR_H_
#include <QDialog>
#include <QAbstractListModel>
#include <QStyledItemDelegate>

namespace Ui {
    class ThemeEditor;
}

namespace Theme {
class ITheme;
class LexerModel;

class ThemeEditor: public QDialog
{
    Q_OBJECT
public:
    ThemeEditor(ITheme* theme, QWidget* parent = NULL);
    virtual ~ThemeEditor();
private slots:
    void onCommonActivated(const QModelIndex&);
    void onCommonForeChanged(const QColor&);
    void onCommonBackChanged(const QColor&);

    void onLexerActivated(const QModelIndex&);
    void onLexerForeChanged(const QColor&);
    void onLexerBackChanged(const QColor&);

    void onTransparentChanged(bool);
    void onBoldChanged(bool);
    void onItalicChanged(bool);
    void onUnderlineChanged(bool);

    void onLexerActivated(const QString&);

    void onAcceped();
private:
    ITheme * _theme;
    Ui::ThemeEditor * _ui;
    QMap<QString, LexerModel*> _lexers;
};

class ColorItemDelegate : public QStyledItemDelegate
{
public:
    ColorItemDelegate(QObject* parent = NULL) : QStyledItemDelegate(parent)
    {}

    QSize sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const
    { return QSize(100, 25); }
};


}
#endif
