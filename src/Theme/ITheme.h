#ifndef _ITHEME_H_
#define _ITHEME_H_
#include <QObject>
#include <QColor>
#include <QTextCharFormat>

namespace Theme {

class ITheme: public QObject
{
    Q_OBJECT
public:
    virtual ~ITheme(){}

    virtual QTextCharFormat format(const QString& lexer, const QString& role, const QColor& defColor = Qt::black, bool bold = false) = 0;
    virtual QTextCharFormat defFormat(const QString& lexer, const QString& role, const QString& defRole = "dsNormal", bool bold = false) = 0;

    virtual QColor color(const QString& lexer, const QString& role, const QColor& def = Qt::black) = 0;
    virtual QColor background(const QString& lexer, const QString& role, const QColor& def = Qt::transparent) = 0;

    virtual void setColor(const QString& lexer, const QString& role, const QColor& color) = 0;
    virtual void setBackground(const QString& lexer, const QString& role, const QColor& color) = 0;

    virtual bool italic(const QString& lexer, const QString& role, bool def = false) = 0;
    virtual bool bold(const QString& lexer, const QString& role, bool def = false) = 0;
    virtual bool underline(const QString& lexer, const QString& role, bool def = false) = 0;

    virtual void setItalic(const QString& lexer, const QString& role, bool on) = 0;
    virtual void setBold(const QString& lexer, const QString& role, bool on) = 0;
    virtual void setUnderline(const QString& lexer, const QString& role, bool on) = 0;

    virtual QString themePath() = 0;
    virtual void save() = 0;
};

}
#endif
