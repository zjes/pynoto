#include <QPushButton>
#include <QCompleter>
#include "AskThemeName.h"
#include "ui_AskThemeName.h"

namespace Theme {

AskThemeName::AskThemeName(const QString& from, const QStringList& list):
    QDialog(),
    _ui(new Ui::AskThemeName),
    _fromName(from),
    _names(list)
{
    _ui->setupUi(this);

    _ui->errLabel->setVisible(false);

    _ui->copyLabel->setText(tr("Copy theme \"%1\" to new theme").arg(_fromName));
    QRegExp exp("[a-zA-Z0-9 \\-_]+");
    _ui->edtName->setValidator(new QRegExpValidator(exp, this));
    _ui->edtName->setCompleter(new QCompleter(_names, this));

    connect(_ui->edtName, SIGNAL(textChanged(QString)), SLOT(onTextChanged(QString)));
    _ui->edtName->setText(_fromName);
}

AskThemeName::~AskThemeName()
{
    delete _ui;
}

QString AskThemeName::name()
{
    return _ui->edtName->text();
}

void AskThemeName::onTextChanged(const QString& text)
{
    int pos = 0;
    QString txt(text);
    _ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!_names.contains(text) && _ui->edtName->validator()->validate(txt, pos) == QValidator::Acceptable);
    if (_names.contains(text)){
        _ui->errLabel->setText(tr("Already exists"));
        _ui->errLabel->setVisible(true);
    } else if (_ui->edtName->validator()->validate(txt, pos) != QValidator::Acceptable){
        _ui->errLabel->setText(tr("Wrong name format"));
        _ui->errLabel->setVisible(true);
    } else {
        _ui->errLabel->setVisible(false);
    }
}

}
