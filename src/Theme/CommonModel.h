#ifndef _COMMONMODEL_H_
#define _COMMONMODEL_H_
#include <QAbstractListModel>
#include <QStringList>
#include <QColor>

namespace Theme {
class ITheme;

class CommonModel : public QAbstractListModel
{
    Q_OBJECT
public:
    CommonModel(ITheme* theme, QObject* parent = NULL);
    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    void setColor(const QModelIndex& index, int role, const QColor& color);

    void save();
private:
    ITheme* _theme;
    QStringList _names;
    QList<QColor> _colors;
    QList<QColor> _backgrounds;
};

}

#endif
