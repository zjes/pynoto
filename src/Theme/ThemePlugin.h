#ifndef _THEMEPLUGIN_H_
#define _THEMEPLUGIN_H_

#include "IThemePlugin.h"

namespace Theme {

class ThemePlugin : public IThemePlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IThemePlugin")
    Q_INTERFACES(Theme::IThemePlugin)
public:
    virtual ~ThemePlugin();

    virtual bool initialize();
    virtual ITheme * theme(const QString& name);
    virtual QStringList themesList();
    virtual bool editTheme(const QString& name);
    virtual QString copyTheme(const QString& themeName);
    virtual void deleteTheme(const QString& themeName);
signals:
    void themeChanged(const QString& what);
private:
    QMap<QString, ITheme*> _themes;
};

}
#endif
