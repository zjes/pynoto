#ifndef _ASKTHEMENAME_H_
#define _ASKTHEMENAME_H_
#include <QDialog>

namespace Ui { class AskThemeName; }
namespace Theme {

class AskThemeName: public QDialog
{
    Q_OBJECT
public:
    AskThemeName(const QString& from, const QStringList& list);
    virtual ~AskThemeName();

    QString name();
private slots:
    void onTextChanged(const QString& text);
private:
    Ui::AskThemeName *_ui;
    QString _fromName;
    QStringList _names;
};

}
#endif
