#include <QtPlugin>
#include <QDebug>
#include <QDir>
#include "ThemePlugin.h"
#include "Theme.h"
#include "ThemeEditor.h"
#include "Include/Aux.h"
#include "AskThemeName.h"

namespace Theme {

ThemePlugin::~ThemePlugin()
{
    qDeleteAll(_themes);
}

bool ThemePlugin::initialize()
{
    connect(this, SIGNAL(themeChanged(QString)), Aux::app(), SIGNAL(preferencesChanged(QString)));
    initialized = true;
    return true;
}

ITheme * ThemePlugin::theme(const QString& name)
{
    if (!_themes.contains(name)){
        _themes[name] = new ThemeImp(name);
    }
    return _themes[name];
}

QStringList ThemePlugin::themesList()
{
    QSettings set;
    QDir dir(set.fileName());
    dir.cdUp();
    dir.cd("themes");
    QStringList items;
    foreach(QFileInfo info, dir.entryInfoList(QStringList() << "*.conf")){
        items.append(info.baseName());
    }
    return items;
}

bool ThemePlugin::editTheme(const QString& name)
{
    ThemeImp *cur = new ThemeImp(name);
    if (QFile::exists(cur->themePath()+"~")){
        QFile::remove(cur->themePath()+"~");
    }
    QFile::copy(cur->themePath(), cur->themePath()+"~");
    ThemeEditor edit(new ThemeImp(name, true));
    if (edit.exec() == QDialog::Accepted){
        QFile::remove(cur->themePath());
        QFile::rename(cur->themePath()+"~", cur->themePath());
        delete _themes[name];
        _themes[name] = new ThemeImp(name);
        emit themeChanged("theme");
        return true;
    }
    QFile::remove(cur->themePath()+"~");
    return false;
}

QString ThemePlugin::copyTheme(const QString& themeName)
{
    AskThemeName ask(themeName, themesList());
    if (ask.exec() == QDialog::Accepted){
        ThemeImp *from = new ThemeImp(themeName);
        ThemeImp *to = new ThemeImp(ask.name());
        QFile::copy(from->themePath(), to->themePath());
        delete from;
        delete to;
        return ask.name();
    }
    return "";
}

void ThemePlugin::deleteTheme(const QString& themeName)
{
    ThemeImp *t = new ThemeImp(themeName);
    QFile::remove(t->themePath());
    delete t;
}

}
