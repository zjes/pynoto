#ifndef _BASEDOCUMENTLAYOUT_H_
#define _BASEDOCUMENTLAYOUT_H_

#include <QPlainTextDocumentLayout>
#include <QTextBlockUserData>


namespace TextEditor {

struct TextBlockData : QTextBlockUserData
{
    TextBlockData();
    virtual ~TextBlockData();

    int _foldingIndent;
    bool _foldingStartIncluded;
    bool _foldingEndIncluded;
    int _foldingIndentDelta;
    int _beginRegion;
    int _endRegion;


    inline void setFoldingIndent(int indent) { _foldingIndent = indent; }
    inline void setFoldingStartIncluded(bool included) { _foldingStartIncluded = included; }
    inline void setFoldingEndIncluded(bool included) { _foldingEndIncluded = included; }
};


class BaseDocumentLayout: public QPlainTextDocumentLayout
{
    Q_OBJECT
public:
    BaseDocumentLayout(QTextDocument *doc);
    virtual ~BaseDocumentLayout();
    virtual QSizeF documentSize() const;
public:
    void emitDocumentSizeChanged();
};

}
#endif
