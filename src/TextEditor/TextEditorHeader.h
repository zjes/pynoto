#ifndef _TEXTEDITORHEADER_H_
#define _TEXTEDITORHEADER_H_
#include <QWidget>

namespace Ui {
    class TextEditorHeader;
}

namespace TextEditor {

class TextEditorHeader: public QWidget
{
    Q_OBJECT
public:
    TextEditorHeader(QWidget * parent = NULL);
    virtual ~TextEditorHeader();

    void setRowCol(int row, int col);
    void setFileName(const QString& fileName);
signals:
    void closeEditor();
public slots:
    void cursorPositionChanged();
    void modificationChanged(bool);
private:
    Ui::TextEditorHeader * _ui;
};

}

#endif
