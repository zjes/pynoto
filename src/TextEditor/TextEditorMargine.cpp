#include <QDebug>
#include <QTextBlock>
#include <QPainter>
#include <QStyle>
#include <QTimer>
#include <QStyleOptionViewItemV2>
#include <QToolTip>
#include <QApplication>
#include "TextEditorMargine.h"
#include "TextEditorWidget.h"
#include "Settings.h"
#include "Include/ITheme.h"

namespace TextEditor {

TextEditorMargine::TextEditorMargine(TextEditorWidget *editor) :
    QWidget(editor),
    _editor(editor),
    _foldingWidth(15),
    _infoWidth(15)
{
    setMouseTracking(true);
    connect(_editor, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
    connect(_editor, SIGNAL(foldingChanged(QList<FoldInfo>)), this, SLOT(updateFoldings(QList<FoldInfo>)));
    connect(_editor, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));

    setThemeName();
    updateLineNumberAreaWidth(0);
}

QSize TextEditorMargine::sizeHint() const {
    return QSize(lineNumberAreaWidth(), 0);
}

void TextEditorMargine::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.fillRect(event->rect(), _background);
    QColor hoverColor = _background.darker();
    hoverColor.setAlpha(100);

    QTextBlock block = _editor->firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) _editor->blockBoundingGeometry(block).translated(_editor->contentOffset()).top();
    int bottom = top + (int) _editor->blockBoundingRect(block).height();

    int hoveredLength = 0;
    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && _marks.contains(blockNumber)){
            for(int i = 0; i < _marks[blockNumber].length(); ++i){
                _marks[blockNumber][i].ico.paint(&painter, i*3, top, width()-_foldingWidth, bottom-top-4, Qt::AlignLeft);
            }
        }

        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(_foreground);
            painter.drawText(_infoWidth, top, width() - _foldingWidth - _infoWidth, fontMetrics().height(), Qt::AlignRight, number);
        }

        if (block.isVisible() && foldings.contains(blockNumber)){
            if (foldings[blockNumber].hovered && foldings[blockNumber].closed == false){
                hoveredLength = foldings[blockNumber].count;
            }
            drawFoldingMarker(&painter, palette(), QRect(width()-_foldingWidth, top, _foldingWidth, fontMetrics().height()), !foldings[blockNumber].closed);
        }

        if (hoveredLength){
            painter.fillRect(width()-_foldingWidth, top, _foldingWidth, bottom-top, hoverColor);
            hoveredLength--;
        }

        block = block.next();
        top = bottom;
        bottom = top + (int) _editor->blockBoundingRect(block).height();
        blockNumber = block.blockNumber();
    }
}

void TextEditorMargine::drawFoldingMarker(QPainter *painter, const QPalette &pal, const QRect &rect, bool expanded) const
{
    QStyle *s = style();
    if (!qstrcmp(s->metaObject()->className(), "OxygenStyle")) {
        painter->save();
        painter->setPen(Qt::NoPen);
        int size = rect.size().width();
        int sqsize = 2*(size/2);

        QColor textColor = pal.buttonText().color();
        QColor brushColor = textColor;

        textColor.setAlpha(100);
        brushColor.setAlpha(100);

        QPolygon a;
        if (expanded) {
            // down arrow
            a.setPoints(3, 0, sqsize/3,  sqsize/2, sqsize  - sqsize/3,  sqsize, sqsize/3);
        } else {
            // right arrow
            a.setPoints(3, sqsize - sqsize/3, sqsize/2,  sqsize/2 - sqsize/3, 0,  sqsize/2 - sqsize/3, sqsize);
            painter->setBrush(brushColor);
        }
        painter->translate(0.5, 0.5);
        painter->setRenderHint(QPainter::Antialiasing);
        painter->translate(rect.topLeft());
        painter->setPen(textColor);
        painter->setBrush(textColor);
        painter->drawPolygon(a);
        painter->restore();
    } else {
        QStyleOptionViewItemV2 opt;
        opt.rect = rect;
        opt.state = QStyle::State_Active | QStyle::State_Item | QStyle::State_Children;
        if (expanded)
            opt.state |= QStyle::State_Open;

         // QGtkStyle needs a small correction to draw the marker in the right place
        if (!qstrcmp(s->metaObject()->className(), "QGtkStyle"))
           opt.rect.translate(-2, 0);
        else if (!qstrcmp(s->metaObject()->className(), "QMacStyle"))
            opt.rect.translate(-1, 0);

        s->drawPrimitive(QStyle::PE_IndicatorBranch, &opt, painter, this);
    }
}

int TextEditorMargine::lineNumberAreaWidth() const
{
    int digits = 1;
    int max = qMax(1, _editor->blockCount());
    while (max >= 10) {
        max /= 10;
        ++digits;
    }

    int space = 3 + fontMetrics().width(QLatin1Char('9')) * digits + _foldingWidth + _infoWidth;

    return space;
}

void TextEditorMargine::updateLineNumberAreaWidth(int /* newBlockCount */)
{
    _editor->setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
    //updateFoldings();
}

void TextEditorMargine::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        scroll(0, dy);
    else
        update(0, rect.y(), width(), rect.height());

    if (rect.contains(_editor->viewport()->rect()))
        updateLineNumberAreaWidth(0);
}

void TextEditorMargine::updateFoldings(const QList<FoldInfo>& folds)
{
    QMap<int, FoldInfo> newFoldings;
    foreach(FoldInfo i, folds){
        if (foldings.contains(i.line)) {
            newFoldings[i.line] = i;
            newFoldings[i.line].closed = foldings[i.line].closed;
            newFoldings[i.line].hovered = foldings[i.line].hovered;
        } else {
            newFoldings[i.line] = i;
        }
    }
    foreach(int line, foldings.keys()){
        if (!newFoldings.contains(line) && foldings[line].closed){
            newFoldings[line].closed = true;
            openCloseFolding(line);
        }
        if (_editor->_forceClosedFold.contains(line)){
            newFoldings[line].closed = true;
            openCloseFolding(line);
        }
    }

    foldings = newFoldings;
    for(int i = _editor->_forceClosedFold.length()-1; i >= 0; --i){
        int line = _editor->_forceClosedFold[i];
        if (foldings.contains(line)){
            openCloseFolding(line);
            _editor->_forceClosedFold.removeOne(line);
        }
    }
    update();
}

void TextEditorMargine::mousePressEvent(QMouseEvent * event)
{
    QTextCursor cursor = _editor->cursorForPosition(QPoint(0, event->pos().y()));
    int line = cursor.blockNumber();
    if (foldings.contains(line) && event->pos().x() > width() - _foldingWidth){
        openCloseFolding(line);
    }
}

void TextEditorMargine::mouseMoveEvent(QMouseEvent * event)
{
    QTextCursor cursor = _editor->cursorForPosition(QPoint(0, event->pos().y()));
    int line = cursor.blockNumber();
    if (foldings.contains(line) && event->pos().x() > width() - _foldingWidth){
        if (!foldings[line].hovered){
            clearFoldingHover();
            foldings[line].hovered = true;
            update();
        }
    } else {
        clearFoldingHover();
        update();
    }
}

void TextEditorMargine::leaveEvent(QEvent *)
{
    clearFoldingHover();
    update();
}

void TextEditorMargine::clearFoldingHover()
{
    foreach(int l, foldings.keys()){
        foldings[l].hovered = false;
    }
}

int TextEditorMargine::lineByPos(const QPoint& pos)
{
    QTextBlock block = _editor->firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) _editor->blockBoundingGeometry(block).translated(_editor->contentOffset()).top();
    int bottom = top + (int) _editor->blockBoundingRect(block).height();

    while (block.isValid()) {
        if (block.isVisible() && pos.y() >= top && pos.y() < bottom) {
            return blockNumber;
        }

        block = block.next();
        top = bottom;
        bottom = top + (int) _editor->blockBoundingRect(block).height();
        ++blockNumber;
    }
    return -1;
}

void TextEditorMargine::openCloseFolding(int line)
{
    QTextBlock block = _editor->document()->findBlockByNumber(line);
    if (!block.isValid())
        return;

    if (foldings[line].closed){
        block.setLineCount(1);
        for(int i = 0; i < foldings[line].count-1; ++i){
            block = block.next();
            block.setVisible(true);
            if (foldings.contains(block.blockNumber()) && foldings[block.blockNumber()].closed){
                i += foldings[block.blockNumber()].count-1;
                block = _editor->document()->findBlockByNumber(block.blockNumber()+foldings[block.blockNumber()].count-1);
            }
        }
        foldings[line].closed = false;
        emit foldingOpenClosed(line, false);
    } else {
        block.setLineCount(foldings[line].count);
        foldings[line].closed = true;
        for(int i = 0; i < foldings[line].count-1; ++i){
            block = block.next();
            block.setVisible(false);
        }
        emit foldingOpenClosed(line, true);
    }
    _editor->viewport()->update();
    //_editor->update();
    update();
}

void TextEditorMargine::setMark(int line, const QIcon& ico, const QString& message)
{
    Mark m;
    m.ico = ico;
    m.line = line;
    m.message = message;
    _marks[line].append(m);
    update();
}

void TextEditorMargine::clearMarks()
{
    _marks.clear();
    update();
}

bool TextEditorMargine::event(QEvent *e)
{
    if (e->type() == QEvent::ToolTip) {
        QHelpEvent *te = static_cast<QHelpEvent*>(e);
        int line = lineByPos(te->pos());
        if (_marks.contains(line)){
            QStringList text;
            foreach(Mark m, _marks[line]){
                text.append(m.message);
            }
            QFont fnt("Monospace");
            fnt.setStyleHint(QFont::TypeWriter);
            fnt.setPointSize(QApplication::font().pointSize());
            QToolTip::setFont(fnt);
            QToolTip::showText(te->globalPos(), text.join("\n"));
        } else {
            QToolTip::hideText();
            e->ignore();
        }
        return true;
    }
    return QWidget::event(e);
}

void TextEditorMargine::showFoldedLine(int line)
{
    int fline = line;
    QList<int> lines = foldings.keys();
    lines << _editor->blockCount();
    for(int i = 0; i < lines.length() - 1; ++i){
        if (line > lines[i] && line < lines[i+1]){
            fline = lines[i];
            break;
        }
    }
    if (foldings.contains(fline) && foldings[fline].closed)
        openCloseFolding(fline);
}

void TextEditorMargine::setThemeName(const QString& name)
{
    Settings set;
    Theme::ITheme *theme = set.themeByName(name.isEmpty() ? set.themeName() : name);
    QPalette pal = QApplication::palette();
    _background = theme->background("common", "dsMargine", pal.color(QPalette::Window));
    _foreground = theme->color("common", "dsMargine", pal.color(QPalette::Text));
    update();
}

}
