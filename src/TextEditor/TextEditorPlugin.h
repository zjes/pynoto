#ifndef _TEXTEDITORPLUGIN_H_
#define _TEXTEDITORPLUGIN_H_
#include "Include/IEditorPlugin.h"

namespace TextEditor {

class TextEditorPlugin: public Editors::IEditorPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IEditorPlugin")
    Q_INTERFACES(Editors::IEditorPlugin)
public:
    virtual ~TextEditorPlugin(){}
    virtual Editors::IEditor * createEditor(const QString& fileName, QWidget* parent, bool sample = false);
    virtual bool initialize();
    virtual QList<IPreferencesPage*> preferences();
};

}

#endif
