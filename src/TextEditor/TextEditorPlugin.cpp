#include <QtPlugin>
#include "TextEditorPlugin.h"
#include "TextEditorImpl.h"
#include "TextEditorWidget.h"
#include "Preferences/TextEditPrefs.h"

namespace TextEditor {

bool TextEditorPlugin::initialize()
{
    initialized = true;
    return true;
}

Editors::IEditor * TextEditorPlugin::createEditor(const QString& fileName, QWidget* parent, bool sample)
{
    return new TextEditorImpl(new TextEditorWidget(fileName), parent, sample);
}

QList<IPreferencesPage*> TextEditorPlugin::preferences()
{
    return (QList<IPreferencesPage*>() << new TextEditPrefs);
}

}
