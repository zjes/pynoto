#ifndef _FOLDINFO_H_
#define _FOLDINFO_H_

class FoldInfo
{
public:
    int line;
    bool closed;
    bool hovered;
    int count;
};

#endif
