#ifndef _TEXTEDITORIMPL_H_
#define _TEXTEDITORIMPL_H_

#include "Include/ITextEditor.h"

namespace TextEditor {

class TextEditorWidget;
class TextEditorImpl: public Editors::ITextEditor
{
    Q_OBJECT
    Q_INTERFACES(Editors::ITextEditor)
public:
    TextEditorImpl(TextEditorWidget * widget, QWidget* parent, bool sample = false);
    virtual ~TextEditorImpl();

    virtual bool isModified();
    virtual void updateStatus();
    virtual QString fileName();
    virtual QString selectedText();
    virtual void saveState();
    virtual QAbstractTableModel* errorListModel();
    virtual void jumpToLine(int line);
    virtual void jumpToOffset(int offset);

    virtual void setThemeName(const QString& name);
protected:
    void focusInEvent(QFocusEvent *);
public slots:
    virtual bool save();
    virtual bool load();
    virtual bool reload();

    virtual void undo();
    virtual void redo();
    virtual void copy();
    virtual void cut();
    virtual void paste();
    virtual void del();

    virtual void selectAll();
    virtual void upperCase();
    virtual void lowerCase();
    virtual void capitalize();
    virtual void camelize();
    virtual void underlize();
    virtual void reset();

    virtual void find(const QString& text, bool forward);
    virtual void replace(const QString& text, const QString& repl, bool findNext, bool forward);
    virtual void replaceAll(const QString& text, const QString& repl);
    virtual void startIncrementalFind();
    virtual void checkIssues();

private slots:
    void onModificationChanged(bool modified);
protected:
    TextEditorWidget *_widget;
};

}

#endif
