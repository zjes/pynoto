#include <QVBoxLayout>
#include <QDebug>
#include "TextEditorImpl.h"
#include "TextEditorWidget.h"

namespace TextEditor {

TextEditorImpl::TextEditorImpl(TextEditorWidget *widget, QWidget* parent, bool sample):
    ITextEditor(parent),
    _widget(widget)
{
    _widget->init();
    QVBoxLayout *box = new QVBoxLayout(this);
    setLayout(box);
    QWidget *header = _widget->header();
    layout()->setContentsMargins(0, 0, 0, 0);
    layout()->setSpacing(0);
    layout()->addWidget(header);
    connect(header, SIGNAL(closeEditor()), SIGNAL(closeEditor()));
    layout()->addWidget(_widget);

    if (sample){
        header->setVisible(false);
        _widget->setReadOnly(true);
        _widget->setFileName(":/sample.txt");
    }
    connect(_widget, SIGNAL(copyAvailable(bool)), this, SIGNAL(copyAvailable(bool)));
    connect(_widget, SIGNAL(pasteAvailable(bool)), this, SIGNAL(pasteAvailable(bool)));
    connect(_widget, SIGNAL(redoAvailable(bool)), this, SIGNAL(redoAvailable(bool)));
    connect(_widget, SIGNAL(undoAvailable(bool)), this, SIGNAL(undoAvailable(bool)));
    connect(_widget, SIGNAL(modificationChanged(bool)), this, SLOT(onModificationChanged(bool)));
    connect(_widget, SIGNAL(errorListChanged(QAbstractTableModel*)), this, SIGNAL(errorListChanged(QAbstractTableModel*)));
    connect(_widget, SIGNAL(jumpToFileOffset(QString,int)), this, SIGNAL(jumpToFileOffset(QString,int)));
}

TextEditorImpl::~TextEditorImpl()
{

}

void TextEditorImpl::onModificationChanged(bool modified)
{
    emit modificationChanged(modified);
    emit editorModificationChanged(this, modified);
}

bool TextEditorImpl::save()
{ return _widget->save(); }

bool TextEditorImpl::load()
{ return _widget->load(); }

bool TextEditorImpl::reload()
{ return _widget->reload(); }

void TextEditorImpl::undo()
{ _widget->undo(); }

void TextEditorImpl::redo()
{ _widget->redo(); }

void TextEditorImpl::copy()
{ _widget->copy(); }

void TextEditorImpl::cut()
{ _widget->cut(); }

void TextEditorImpl::paste()
{ _widget->paste(); }

void TextEditorImpl::del()
{ _widget->del(); }

void TextEditorImpl::selectAll()
{ _widget->selectAll(); }

void TextEditorImpl::upperCase()
{ _widget->uppercase(); }

void TextEditorImpl::lowerCase()
{ _widget->lowercase(); }

void TextEditorImpl::capitalize()
{ _widget->capitalize(); }

void TextEditorImpl::camelize()
{ _widget->camelize(); }

void TextEditorImpl::underlize()
{ _widget->underlize(); }

QString TextEditorImpl::selectedText()
{ return _widget->selectedText(); }

void TextEditorImpl::find(const QString& text, bool forward)
{ _widget->find(text, forward); }

void TextEditorImpl::replace(const QString& text, const QString& repl, bool findNext, bool forward)
{ _widget->replace(text, repl, findNext, forward); }

void TextEditorImpl::replaceAll(const QString& text, const QString& repl)
{ _widget->replaceAll(text, repl); }

void TextEditorImpl::startIncrementalFind()
{ _widget->startIncrementalFind(); }

bool TextEditorImpl::isModified()
{
    return _widget->isModified();
}

void TextEditorImpl::updateStatus()
{
    emit undoAvailable(_widget->document()->isUndoAvailable());
    emit redoAvailable(_widget->document()->isRedoAvailable());
    emit modificationChanged(_widget->document()->isModified());
    emit copyAvailable(!_widget->selectedText().isEmpty());
    emit pasteAvailable(_widget->canPaste());
}

void TextEditorImpl::focusInEvent(QFocusEvent *)
{
    _widget->setFocus();
}

QString TextEditorImpl::fileName()
{
    return _widget->fileName();
}

void TextEditorImpl::saveState()
{
    _widget->saveState();
}

QAbstractTableModel* TextEditorImpl::errorListModel()
{
    return NULL;
}

void TextEditorImpl::jumpToLine(int line)
{
    _widget->jump(line);
}

void TextEditorImpl::jumpToOffset(int offset)
{
    _widget->jumpOffset(offset);
}

void TextEditorImpl::reset()
{
    _widget->reset();
}

void TextEditorImpl::checkIssues()
{
}

void TextEditorImpl::setThemeName(const QString& name)
{
    _widget->setThemeName(name);
}

}
