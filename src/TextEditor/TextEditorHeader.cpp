#include <QDebug>
#include <QPainter>
#include <QPlainTextEdit>
#include "Include/IIconProvider.h"
#include "Include/IProjectPlugin.h"
#include "Include/IProject.h"
#include "Include/Aux.h"
#include "TextEditorHeader.h"
#include "ui_TextEditorHeader.h"

namespace TextEditor {

TextEditorHeader::TextEditorHeader(QWidget * parent):
    QWidget(parent),
    _ui(new Ui::TextEditorHeader)
{
    _ui->setupUi(this);
    _ui->btnClose->setIcon(Aux::icons()->icon("pclose"));
    connect(_ui->btnClose, SIGNAL(clicked()), SIGNAL(closeEditor()));
    _ui->btnClose->setToolTip(tr("Close")+" "+_ui->btnClose->shortcut().toString());
    _ui->lblMod->setVisible(false);
}

TextEditorHeader::~TextEditorHeader()
{
    delete _ui;
}

void TextEditorHeader::setRowCol(int row, int col)
{
    _ui->lblRow->setText(QString::number(row));
    _ui->lblCol->setText(QString::number(col));
}

void TextEditorHeader::cursorPositionChanged()
{
    QPlainTextEdit * edit = qobject_cast<QPlainTextEdit *>(sender());
    if (edit){
        setRowCol(edit->textCursor().blockNumber()+1, edit->textCursor().columnNumber()+1);
    }
}

void TextEditorHeader::setFileName(const QString& fileName)
{
    Project::IProjectPlugin *plug = Aux::plugin<Project::IProjectPlugin*>("project");
    if (plug){
        _ui->lblFileName->setText(plug->project()->fileDisplayName(fileName));
    } else {
        _ui->lblFileName->setText(fileName);
    }
}

void TextEditorHeader::modificationChanged(bool mod)
{
    _ui->lblMod->setVisible(mod);
}

}
