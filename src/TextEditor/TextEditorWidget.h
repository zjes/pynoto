#ifndef _TEXTEDITORWIDGET_H_
#define _TEXTEDITORWIDGET_H_
#include <QPlainTextEdit>
#include <QTimer>
#include "FoldInfo.h"

class QAbstractTableModel;
namespace TextEditor {

class CustomMenu;
class TextEditorMargine;
class Highlighter;

class TextEditorWidget: public QPlainTextEdit
{
    Q_OBJECT
public:
    explicit TextEditorWidget(const QString& fileName, QWidget* parent = NULL);
    virtual ~TextEditorWidget();
    friend class TextEditorMargine;
    void init();
    void saveState();
public:
    virtual bool load();
    virtual bool reload();
    bool isModified();
    bool save();
    QString selectedText();
    virtual void editorLoaded();
    virtual QWidget * header();
    virtual Highlighter* hightlighter();
    QString fileName();
    void setFileName(const QString& fileName);
    int visibleBlockCount();
    void setMark(int line, const QIcon& ico, const QString& message);
    void clearMarks();
    QString content();
    QPoint pointForPosition(int position);
    QString text(int from, int to);
    void setThemeName(const QString& name = "");
public slots:
    void del();
    void uppercase();
    void lowercase();
    void capitalize();
    virtual bool reset();
    void camelize();
    void underlize();
    void jump(int line, int col = 0);
    void jumpOffset(int offset);

    void find(const QString& text, bool forward);
    void replace(const QString& text, const QString& repl, bool findNext = false, bool forward = true);
    void replaceAll(const QString& text, const QString& repl);
    void startIncrementalFind();
signals:
    void contentLoaded();
    void foldingChanged(const QList<FoldInfo>& folds);
    void pasteAvailable(bool);
    void errorListChanged(QAbstractTableModel*);
    void jumpToFileOffset(const QString& file, int offset);
private slots:
    void highlightCurrentLine();
    void onClipboardChanged();
    void onCursorPositionChanged();
    void onCustomMenu(const QPoint&);
    void onFoldingOpenClosed(int line, bool closed);
    void restoreState();
    void editorTextChanged();
    void validateFolding();
protected slots:
    virtual void preferencesChanged(const QString& what);
protected:
    virtual void resizeEvent(QResizeEvent *e);
    virtual void keyPressEvent(QKeyEvent *e);
    virtual void paintEvent(QPaintEvent *e);
    virtual bool event(QEvent *e);
private:
    void indentOrUnindent(bool doIndent);
    void handleHomeKey(bool anchor);
    bool isOpenBrace(const QChar& chr);
    int foundNextBrace(const QChar& chr);
    int foundPrevBrace(const QChar& chr);
    void showBracesMatches(int found, int off = 0);
protected:
    enum ExtraKind {
        BracesExtraKind,
        LineExtraKind,
        HiddenBlock,
        LinkKind
    };
    void setExtraSelections(ExtraKind kind, const QList<QTextEdit::ExtraSelection>& sels);
    Highlighter * _highlighter;
private:
    TextEditorMargine *_margine;
    QColor _lineColor;
    QMap<ExtraKind, QList<QTextEdit::ExtraSelection> > _extraSelections;
    QString _fileName;
    QTextCursor _incFindCursor;
    CustomMenu *_menu;
    QList<int> _forceClosedFold;
    QTimer _updateTimer;
    int _rMarginWidth;
};

}
#endif
