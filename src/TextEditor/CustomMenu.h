#ifndef _CUSTOMMENU_H_
#define _CUSTOMMENU_H_
#include <QObject>
#include <QPoint>


namespace TextEditor {

class TextEditorWidget;

class CustomMenu: public QObject
{
    Q_OBJECT
public:
    CustomMenu(TextEditorWidget* parent = NULL);
    virtual ~CustomMenu();
public:
    void createMenu(const QPoint & point);
private:
    TextEditorWidget* _editor;
};

}
#endif
