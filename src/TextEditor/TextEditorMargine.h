#ifndef _TEXTEDITORMARGINE_H_
#define _TEXTEDITORMARGINE_H_

#include <QWidget>
#include <QMap>
#include <QIcon>
#include "FoldInfo.h"

namespace TextEditor {

class TextEditorWidget;

struct Mark {
    QIcon ico;
    int line;
    QString message;
};

class TextEditorMargine : public QWidget
{
    Q_OBJECT
public:
    TextEditorMargine(TextEditorWidget *editor);
    QSize sizeHint() const;

    int lineNumberAreaWidth() const;
    QMap<int, FoldInfo> foldings;
    void openCloseFolding(int line);

    void setMark(int line, const QIcon& ico, const QString& message);
    void clearMarks();
    void showFoldedLine(int line);
    void setThemeName(const QString& name = "");
    QColor &background()
    { return _background; }
signals:
    void foldingOpenClosed(int line, bool closed);
protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent * event);
    virtual void mouseMoveEvent(QMouseEvent * event);
    virtual void leaveEvent(QEvent *);

    int lineByPos(const QPoint& pos);
    virtual bool event(QEvent *e);
private slots:
    void updateLineNumberArea(const QRect &rect, int dy);
    void updateLineNumberAreaWidth(int);
    void updateFoldings(const QList<FoldInfo>& folds);
private:
    void clearFoldingHover();
    void drawFoldingMarker(QPainter *painter, const QPalette &pal, const QRect &rect, bool expanded) const;
private:
    TextEditorWidget *_editor;
    int _foldingWidth;
    QColor _background;
    QColor _foreground;
    QMap<int, QList<Mark> > _marks;
    int _infoWidth;
};

}
#endif
