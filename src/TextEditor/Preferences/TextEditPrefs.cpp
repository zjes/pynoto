#include <QDebug>
#include <QFont>
#include <QFontDialog>
#include "Include/Aux.h"
#include "Include/IThemePlugin.h"
#include "Include/IEditorsManagerPlugin.h"
#include "Include/ITextEditor.h"
#include "TextEditPrefs.h"
#include "ui_TextEditPrefs.h"
#include "Settings.h"

namespace TextEditor {

TextEditPrefs::TextEditPrefs(QWidget * parent):
    IPreferencesPage(parent),
    _ui(new Ui::TextEditPrefs)
{
    _ui->setupUi(this);

    connect(_ui->btnFont, SIGNAL(clicked()), SLOT(onChangeFont()));

    Settings set;
    _ui->edtFont->setText(set.fontFace()+" "+QString::number(set.fontSize())+"pt");
    _fnt = QFont(set.fontFace(), set.fontSize(), set.fontWeight());
    _fnt.setStyle(set.fontStyle());
    _ui->edtFont->setFont(_fnt);

    _ui->chkUseAntialias->setChecked(set.useAntialias());
    _ui->chkHiglightLine->setChecked(set.hightlight());
    _ui->chkShowWhite->setChecked(set.showWhitespace());
    _ui->chkShowBlock->setChecked(set.showIndentLine());
    _ui->identColor->setColor(set.identColor());
    _ui->identColor->setDefaultColor(Qt::red);

    Theme::IThemePlugin *theme = Aux::plugin<Theme::IThemePlugin *>("theme");
    EditorsManager::IEditorsManagerPlugin *editors = Aux::plugin<EditorsManager::IEditorsManagerPlugin *>("editorsmanager");
    foreach(QString themeName, theme->themesList()){
        _ui->themeList->addItem(themeName, themeName);
    }
    _ui->themeList->setCurrentIndex(_ui->themeList->findText(set.themeName()));
    _ui->lexList->addItem("Text", "text");
    _ui->lexList->addItem("Python", "python");
    _ui->preview->addWidget(editors->sampleEditor("text"));
    _ui->preview->addWidget(editors->sampleEditor("python"));

    connect(_ui->lexList, SIGNAL(activated(int)), SLOT(onLexerChanged(int)));
    connect(_ui->themeList, SIGNAL(activated(int)), SLOT(onThemeActivated(int)));
    connect(_ui->editTheme, SIGNAL(clicked()), SLOT(onThemeEdit()));
    connect(_ui->copyTheme, SIGNAL(clicked()), SLOT(onCopyTheme()));
    connect(_ui->deleteTheme, SIGNAL(clicked()), SLOT(onDeleteTheme()));
}

TextEditPrefs::~TextEditPrefs()
{
    delete _ui;
}

QString TextEditPrefs::title()
{
    return tr("Text editor");
}

QIcon TextEditPrefs::icon()
{
    return QIcon();
}

bool TextEditPrefs::save()
{
    Settings set;
    if (set.fontFace() != _fnt.family() || set.fontSize() != _fnt.pointSize() ||
            set.fontStyle() != _fnt.style() || set.fontWeight() != _fnt.weight()){
        set.setFontFace(_fnt.family());
        set.setFontSize(_fnt.pointSize());
        set.setFontStyle(_fnt.style());
        set.setFontWeight(_fnt.weight());
        emit changed("editor-font");
    }
    if (set.useAntialias() != _ui->chkUseAntialias->isChecked()){
        set.setUseAntialias(_ui->chkUseAntialias->isChecked());
        emit changed("editor-font");
    }
    if (set.hightlight() != _ui->chkHiglightLine->isChecked()){
        set.setHightlight(_ui->chkHiglightLine->isChecked());
        emit changed("highlight-line");
    }
    if (set.showIndentLine() != _ui->chkShowBlock->isChecked()){
        set.setShowIndentLine(_ui->chkShowBlock->isChecked());
        emit changed("show-ident-line");
    }
    if (set.themeName() != _ui->themeList->currentText()){
        set.setThemeName(_ui->themeList->currentText());
        emit changed("theme");
    }
    if (set.identColor() != _ui->identColor->color()){
        set.setIdentColor(_ui->identColor->color());
        emit changed("ident-color");
    }
    return true;
}

void TextEditPrefs::onChangeFont()
{
    bool ok = false;
    QFont fnt = QFontDialog::getFont(&ok, _fnt, this);
    if (ok){
        _fnt = fnt;
        _ui->edtFont->setFont(_fnt);
    }
}

void TextEditPrefs::onLexerChanged(int index)
{
    _ui->preview->setCurrentIndex(index);
}

void TextEditPrefs::onThemeActivated(int index)
{
    QString theme = _ui->themeList->itemData(index, Qt::UserRole).toString();
    for(int i = 0; i < _ui->preview->count(); ++i){
        Editors::ITextEditor *ed = qobject_cast<Editors::ITextEditor *>(_ui->preview->widget(i));
        ed->setThemeName(theme);
    }
}

void TextEditPrefs::onThemeEdit()
{
    Theme::IThemePlugin *theme = Aux::plugin<Theme::IThemePlugin *>("theme");
    if (theme->editTheme(_ui->themeList->itemData(_ui->themeList->currentIndex()).toString()))
        onThemeActivated(_ui->themeList->currentIndex());
}

void TextEditPrefs::onCopyTheme()
{
    Theme::IThemePlugin *theme = Aux::plugin<Theme::IThemePlugin *>("theme");
    QString newTheme = theme->copyTheme(_ui->themeList->currentText());
    if (!newTheme.isEmpty()){
        _ui->themeList->addItem(newTheme, newTheme);
        _ui->themeList->setCurrentIndex(_ui->themeList->findText(newTheme));
    }
}

void TextEditPrefs::onDeleteTheme()
{
    Theme::IThemePlugin *theme = Aux::plugin<Theme::IThemePlugin *>("theme");
    theme->deleteTheme(_ui->themeList->currentText());
    int index = _ui->themeList->currentIndex();
    _ui->themeList->setCurrentIndex(index > 0 ? index - 1 : 0);
    _ui->themeList->removeItem(index);
    onThemeActivated(_ui->themeList->currentIndex());
}

}
