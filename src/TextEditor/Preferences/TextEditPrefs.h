#ifndef _TEXTEDITPREFS_H_
#define _TEXTEDITPREFS_H_
#include "Include/IPreferencesPage.h"

namespace Ui {
    class TextEditPrefs;
}

namespace TextEditor {

class TextEditPrefs: public IPreferencesPage
{
    Q_OBJECT
public:
    TextEditPrefs(QWidget * parent = NULL);
    virtual ~TextEditPrefs();

    virtual QString title();
    virtual QIcon icon();
    virtual bool save();
    virtual int order() {return 4;}
private slots:
    void onChangeFont();
    void onLexerChanged(int);
    void onThemeActivated(int);
    void onThemeEdit();
    void onCopyTheme();
    void onDeleteTheme();
private:
    Ui::TextEditPrefs * _ui;
    QFont _fnt;
};

}

#endif
