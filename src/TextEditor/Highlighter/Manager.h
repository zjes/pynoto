#ifndef _MANAGER_H_
#define _MANAGER_H_

#include <QString>
#include <QSharedPointer>
#include <QMap>
#include "HighlightDefinition.h"

namespace TextEditor {

class Manager
{
public:
    static Manager & instance();
    QSharedPointer<HighlightDefinition> definition(const QString &fileName);
private:
    Manager();
    virtual ~Manager();
    void buildDatabase(const QStringList& files);
    QSharedPointer<HighlightDefinition> _definition(const QString &kateFileName);

    struct HightInfo
    {
        QString fileName;
        QString langName;
        QString memeType;
    };

    QMap<QString, HightInfo> _info;
    QMap<QString, QSharedPointer<HighlightDefinition> > _definitionsCache;
};

}
#endif
