/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#include "Context.h"
#include "Rule.h"
#include "Reuse.h"
#include "DynamicRule.h"
#include "HighlightDefinition.h"

namespace TextEditor {

Context::Context() :
    _fallthrough(false),
    _dynamic(false)
{}

Context::Context(const Context &context) :
    _id(context._id),
    _name(context._name),
    _lineBeginContext(context._lineBeginContext),
    _lineEndContext(context._lineEndContext),
    _fallthroughContext(context._fallthroughContext),
    _itemData(context._itemData),
    _fallthrough(context._fallthrough),
    _dynamic(context._dynamic),
    _instructions(context._instructions),
    _definition(context._definition)
{
    // Rules need to be deeply copied because of dynamic contexts.
    foreach (const QSharedPointer<Rule> &rule, context._rules)
        _rules.append(QSharedPointer<Rule>(rule->clone()));
}

const Context &Context::operator=(Context copy)
{
    swap(copy);
    return *this;
}

Context::~Context()
{
    _rules.clear();
}

void Context::swap(Context &context)
{
    qSwap(_id, context._id);
    qSwap(_name, context._name);
    qSwap(_lineBeginContext, context._lineBeginContext);
    qSwap(_lineEndContext, context._lineEndContext);
    qSwap(_fallthroughContext, context._fallthroughContext);
    qSwap(_itemData, context._itemData);
    qSwap(_fallthrough, context._fallthrough);
    qSwap(_dynamic, context._dynamic);
    qSwap(_rules, context._rules);
    qSwap(_instructions, context._instructions);
    qSwap(_definition, context._definition);
}

void Context::configureId(const int unique)
{
    _id.append(QString::number(unique));
}

const QString &Context::id() const
{
    return _id;
}

void Context::setName(const QString &name)
{
    _name = name;
    _id = name;
}

const QString &Context::name() const
{
    return _name;
}

void Context::setLineBeginContext(const QString &context)
{
    _lineBeginContext = context;
}

const QString &Context::lineBeginContext() const
{
    return _lineBeginContext;
}

void Context::setLineEndContext(const QString &context)
{
    _lineEndContext = context;
}

const QString &Context::lineEndContext() const
{
    return _lineEndContext;
}

void Context::setFallthroughContext(const QString &context)
{
    _fallthroughContext = context;
}

const QString &Context::fallthroughContext() const
{
    return _fallthroughContext;
}

void Context::setItemData(const QString &itemData)
{
    _itemData = itemData;
}

const QString &Context::itemData() const
{
    return _itemData;
}

void Context::setFallthrough(const QString &fallthrough)
{
    _fallthrough = toBool(fallthrough);
}

bool Context::isFallthrough() const
{
    return _fallthrough;
}

void Context::setDynamic(const QString &dynamic)
{
    _dynamic = toBool(dynamic);
}

bool Context::isDynamic() const
{
    return _dynamic;
}

void Context::updateDynamicRules(const QStringList &captures) const
{
    TextEditor::updateDynamicRules(_rules, captures);
}

void Context::addRule(const QSharedPointer<Rule> &rule)
{
    _rules.append(rule);
}

void Context::addRule(const QSharedPointer<Rule> &rule, int index)
{
    _rules.insert(index, rule);
}

const QList<QSharedPointer<Rule> > & Context::rules() const
{
    return _rules;
}

void Context::addIncludeRulesInstruction(const IncludeRulesInstruction &instruction)
{
    _instructions.append(instruction);
}

const QList<IncludeRulesInstruction> &Context::includeRulesInstructions() const
{
    return _instructions;
}

void Context::clearIncludeRulesInstructions()
{
    _instructions.clear();
}

void Context::setDefinition(const QSharedPointer<HighlightDefinition> &definition)
{
    _definition = definition;
}

const QSharedPointer<HighlightDefinition> &Context::definition() const
{
    return _definition;
}

}
