/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#include "ItemData.h"
#include "Reuse.h"

namespace TextEditor {

ItemData::ItemData() :
    _italic(false),
    _italicSpecified(false),
    _bold(false),
    _boldSpecified(false),
    _underlined(false),
    _underlinedSpecified(false),
    _strikedOut(false),
    _strikeOutSpecified(false),
    _isCustomized(false)
{}

void ItemData::setStyle(const QString &style)
{
    _style = style;
}

const QString &ItemData::style() const
{
    return _style;
}

void ItemData::setColor(const QString &color)
{
    if (!color.isEmpty()) {
        _color.setNamedColor(color);
        _isCustomized = true;
    }
}

const QColor &ItemData::color() const
{
    return _color;
}

void ItemData::setSelectionColor(const QString &color)
{
    if (!color.isEmpty()) {
        _selectionColor.setNamedColor(color);
        _isCustomized = true;
    }
}

const QColor &ItemData::selectionColor() const
{
    return _selectionColor;
}

void ItemData::setItalic(const QString &italic)
{
    if (!italic.isEmpty()) {
        _italic = toBool(italic);
        _italicSpecified = true;
        _isCustomized = true;
    }
}

bool ItemData::isItalic() const
{
    return _italic;
}

bool ItemData::isItalicSpecified() const
{
    return _italicSpecified;
}

void ItemData::setBold(const QString &bold)
{
    if (!bold.isEmpty()) {
        _bold = toBool(bold);
        _boldSpecified = true;
        _isCustomized = true;
    }
}

bool ItemData::isBold() const
{
    return _bold;
}

bool ItemData::isBoldSpecified() const
{
    return _boldSpecified;
}

void ItemData::setUnderlined(const QString &underlined)
{
    if (!underlined.isEmpty()) {
        _underlined = toBool(underlined);
        _underlinedSpecified = true;
        _isCustomized = true;
    }
}

bool ItemData::isUnderlined() const
{
    return _underlined;
}

bool ItemData::isUnderlinedSpecified() const
{
    return _underlinedSpecified;
}

void ItemData::setStrikeOut(const QString &strike)
{
    if (!strike.isEmpty()) {
        _strikedOut = toBool(strike);
        _strikeOutSpecified = true;
        _isCustomized = true;
    }
}

bool ItemData::isStrikeOut() const
{
    return _strikedOut;
}

bool ItemData::isStrikeOutSpecified() const
{
    return _strikeOutSpecified;
}

bool ItemData::isCustomized() const
{
    return _isCustomized;
}

}
