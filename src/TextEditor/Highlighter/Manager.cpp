#include <QDebug>
#include <QMutex>
#include <QDir>
#include <QApplication>
#include <QSettings>
#include <QXmlStreamReader>
#include "Manager.h"
#include "HighlightDefinitionHandler.h"
#include "HighlighterException.h"

namespace TextEditor
{

Manager::Manager()
{
    QStringList kate;

    QDir synDir(QApplication::applicationDirPath());
    if (synDir.cd("syntax"))
        kate << synDir.path();

    QSettings set;
    QDir dir(set.fileName());
    dir.cdUp();
    if (dir.cd("syntax"))
        kate << dir.path();

    QString katePath = "/usr/share/kde4/apps/katepart/syntax/";
    if (QFile::exists(katePath))
        kate << "/usr/share/kde4/apps/katepart/syntax/";

    QMap<QString, QString> files;
    foreach(QString path, kate){
        QDir synPath(path);
        foreach(QString file, synPath.entryList(QStringList() << "*.xml")){
            if (files.contains(file))
                continue;
            files.insert(file, synPath.filePath(file));
        }
    }
    buildDatabase(files.values());
}

Manager::~Manager()
{
    _info.clear();
    _definitionsCache.clear();
}

Manager & Manager::instance()
{
    static Manager instance;
    return instance;
}

void Manager::buildDatabase(const QStringList& files)
{
    foreach(QString file, files){
        QFile definitionFile(file);
        if (!definitionFile.open(QIODevice::ReadOnly | QIODevice::Text))
            continue;

        QXmlStreamReader reader(&definitionFile);
        while (!reader.atEnd() && !reader.hasError()){
            if (reader.readNext() == QXmlStreamReader::StartElement && reader.name() == "language") {
                const QXmlStreamAttributes &atts = reader.attributes();

                HightInfo info;
                QString extPattern = atts.value("extensions").toString();//.split(";", QString::SkipEmptyParts);
                info.fileName = file;
                info.langName = atts.value("name").toString();
                info.memeType = atts.value("mimetype").toString();
                _info.insert(extPattern, info);
            }
        }
        definitionFile.close();
    }
}

QSharedPointer<HighlightDefinition> Manager::definition(const QString &fileName)
{
    QRegExp exp;
    exp.setPatternSyntax(QRegExp::Wildcard);
    exp.setCaseSensitivity(Qt::CaseInsensitive);
    QFileInfo info(fileName);

    foreach(QString extPattern, _info.keys()){
        foreach(QString ext, extPattern.split(";", QString::SkipEmptyParts)){
            exp.setPattern(ext);
            if (exp.exactMatch(info.fileName())){
                return _definition(_info[extPattern].fileName);
            }
        }
    }
    return QSharedPointer<HighlightDefinition>();
}

QSharedPointer<HighlightDefinition> Manager::_definition(const QString &kateFileName)
{
    static QMutex mutex;
    mutex.lock();

    if (_definitionsCache.contains(kateFileName)){
        mutex.unlock();
        return _definitionsCache[kateFileName];
    }

    QFile definitionFile(kateFileName);
    if (!definitionFile.open(QIODevice::ReadOnly | QIODevice::Text)){
        mutex.unlock();
        return QSharedPointer<HighlightDefinition>();
    }

    QSharedPointer<HighlightDefinition> definition(new HighlightDefinition);
    HighlightDefinitionHandler handler(definition);

    QXmlInputSource source(&definitionFile);
    QXmlSimpleReader reader;
    reader.setContentHandler(&handler);
    try {
        reader.parse(source);
    } catch (HighlighterException &) {
        definition.clear();
    }
    definitionFile.close();
    _definitionsCache.insert(kateFileName, definition);

    mutex.unlock();
    return _definitionsCache.value(kateFileName);
}

}
