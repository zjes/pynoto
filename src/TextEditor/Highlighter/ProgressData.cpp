/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#include "ProgressData.h"
#include "Rule.h"

#include <QtGlobal>

namespace TextEditor {

ProgressData::ProgressData() :
    _offset(0),
    _savedOffset(-1),
    _onlySpacesSoFar(true),
    _openingBraceMatchAtFirstNonSpace(false),
    _closingBraceMatchAtNonEnd(false),
    _willContinueLine(false)
{}

ProgressData::~ProgressData()
{
    foreach (Rule *rule, _trackedRules)
        rule->progressFinished();
}

void ProgressData::setOffset(const int offset)
{
    _offset = offset;
}

int ProgressData::offset() const
{
    return _offset;
}

void ProgressData::incrementOffset()
{
    ++_offset;
}

void ProgressData::incrementOffset(const int increment)
{
    _offset += increment;
}

void ProgressData::saveOffset()
{
    _savedOffset = _offset;
}

void ProgressData::restoreOffset()
{
    Q_ASSERT(_savedOffset != -1);
    _offset = _savedOffset;
    _savedOffset = -1;
}

void ProgressData::setOnlySpacesSoFar(const bool onlySpaces)
{
    _onlySpacesSoFar = onlySpaces;
}

bool ProgressData::isOnlySpacesSoFar() const
{
    return _onlySpacesSoFar;
}

void ProgressData::setOpeningBraceMatchAtFirstNonSpace(const bool match)
{
    _openingBraceMatchAtFirstNonSpace = match;
}

bool ProgressData::isOpeningBraceMatchAtFirstNonSpace() const
{
    return _openingBraceMatchAtFirstNonSpace;
}

void ProgressData::setClosingBraceMatchAtNonEnd(const bool match)
{
    _closingBraceMatchAtNonEnd = match;
}

bool ProgressData::isClosingBraceMatchAtNonEnd() const
{
    return _closingBraceMatchAtNonEnd;
}

void ProgressData::clearBracesMatches()
{
    if (_openingBraceMatchAtFirstNonSpace)
        _openingBraceMatchAtFirstNonSpace = false;
    if (_closingBraceMatchAtNonEnd)
        _closingBraceMatchAtNonEnd = false;
}

void ProgressData::setWillContinueLine(const bool willContinue)
{
    _willContinueLine = willContinue;
}

bool ProgressData::isWillContinueLine() const
{
    return _willContinueLine;
}

void ProgressData::setCaptures(const QStringList &captures)
{
    _captures = captures;
}

const QStringList &ProgressData::captures() const
{
    return _captures;
}

void ProgressData::trackRule(Rule *rule)
{
    _trackedRules.append(rule);
}

}
