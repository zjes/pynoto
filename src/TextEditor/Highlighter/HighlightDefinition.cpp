/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/

#include "HighlightDefinition.h"
#include "HighlighterException.h"
#include "Context.h"
#include "KeywordList.h"
#include "ItemData.h"
#include "Reuse.h"

#include <QString>

namespace TextEditor {

HighlightDefinition::HighlightDefinition() :
    _singleLineCommentAfterWhiteSpaces(false),
    _keywordCaseSensitivity(Qt::CaseSensitive),
    _indentationBasedFolding(false)
{
    QString s(".():!+,-<=>%&/;?[]^{|}~\\*, \t");
    foreach (const QChar &c, s)
        _delimiters.insert(c);
}

HighlightDefinition::~HighlightDefinition()
{}

bool HighlightDefinition::isValid() const
{
    return !_initialContext.isEmpty();
}

template <class Element, class Container>
QSharedPointer<Element> HighlightDefinition::GenericHelper::create(const QString &name, Container &container)
{
    if (name.isEmpty())
        throw HighlighterException();

    if (container.contains(name))
        throw HighlighterException();

    return container.insert(name, QSharedPointer<Element>(new Element)).value();
}

template <class Element, class Container>
QSharedPointer<Element> HighlightDefinition::GenericHelper::find(const QString &name, const Container &container) const
{
    typename Container::const_iterator it = container.find(name);
    if (it == container.end())
        throw HighlighterException();

    return it.value();
}

QSharedPointer<KeywordList> HighlightDefinition::createKeywordList(const QString &list)
{
    return _helper.create<KeywordList>(list, _lists);
}

QSharedPointer<KeywordList> HighlightDefinition::keywordList(const QString &list)
{
    return _helper.find<KeywordList>(list, _lists);
}

QSharedPointer<Context> HighlightDefinition::createContext(const QString &context, bool initial)
{
    if (initial)
        _initialContext = context;

    QSharedPointer<Context> newContext = _helper.create<Context>(context, _contexts);
    newContext->setName(context);
    return newContext;
}

QSharedPointer<Context> HighlightDefinition::initialContext() const
{
    return _helper.find<Context>(_initialContext, _contexts);
}

QSharedPointer<Context> HighlightDefinition::context(const QString &context) const
{
    return _helper.find<Context>(context, _contexts);
}

const QHash<QString, QSharedPointer<Context> > &HighlightDefinition::contexts() const
{
    return _contexts;
}

QSharedPointer<ItemData> HighlightDefinition::createItemData(const QString &itemData)
{
    return _helper.create<ItemData>(itemData, _itemsData);
}

QSharedPointer<ItemData> HighlightDefinition::itemData(const QString &itemData) const
{
    return _helper.find<ItemData>(itemData, _itemsData);
}

void HighlightDefinition::setSingleLineComment(const QString &start)
{
    _singleLineComment = start;
}

const QString &HighlightDefinition::singleLineComment() const
{
    return _singleLineComment;
}

void HighlightDefinition::setCommentAfterWhitespaces(const QString &after)
{
    if (after == "afterwhitespace")
        _singleLineCommentAfterWhiteSpaces = true;
}

bool HighlightDefinition::isCommentAfterWhiteSpaces() const
{
    return _singleLineCommentAfterWhiteSpaces;
}

void HighlightDefinition::setMultiLineCommentStart(const QString &start)
{
    _multiLineCommentStart = start;
}

const QString &HighlightDefinition::multiLineCommentStart() const
{
    return _multiLineCommentStart;
}

void HighlightDefinition::setMultiLineCommentEnd(const QString &end)
{
    _multiLineCommentEnd = end;
}

const QString &HighlightDefinition::multiLineCommentEnd() const
{
    return _multiLineCommentEnd;
}

void HighlightDefinition::setMultiLineCommentRegion(const QString &region)
{
    _multiLineCommentRegion = region;
}

const QString &HighlightDefinition::multiLineCommentRegion() const
{
    return _multiLineCommentRegion;
}

void HighlightDefinition::removeDelimiters(const QString &characters)
{
    for (int i = 0; i < characters.length(); ++i)
        _delimiters.remove(characters.at(i));
}

void HighlightDefinition::addDelimiters(const QString &characters)
{
    for (int i = 0; i < characters.length(); ++i) {
        if (!_delimiters.contains(characters.at(i)))
            _delimiters.insert(characters.at(i));
    }
}

bool HighlightDefinition::isDelimiter(const QChar &character) const
{
    if (_delimiters.contains(character))
        return true;
    return false;
}

void HighlightDefinition::setKeywordsSensitive(const QString &sensitivity)
{
    if (!sensitivity.isEmpty())
        _keywordCaseSensitivity = toCaseSensitivity(toBool(sensitivity));
}

Qt::CaseSensitivity HighlightDefinition::keywordsSensitive() const
{
    return _keywordCaseSensitivity;
}

void HighlightDefinition::setIndentationBasedFolding(const QString &indentationBasedFolding)
{
    _indentationBasedFolding = toBool(indentationBasedFolding);
}

bool HighlightDefinition::isIndentationBasedFolding() const
{
    return _indentationBasedFolding;
}

}
