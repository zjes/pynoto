/**************************************************************************
**
** This file is part of Qt Creator
**
** Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**************************************************************************/
#include <QLatin1String>
#include <QLatin1Char>
#include <QDebug>

#include "Highlighter.h"
#include "HighlightDefinition.h"
#include "Context.h"
#include "Rule.h"
#include "ItemData.h"
#include "HighlighterException.h"
#include "ProgressData.h"
#include "Reuse.h"
#include "TabSettings.h"

#include "Settings.h"

namespace TextEditor {

namespace {
    static const QLatin1String kStay("#stay");
    static const QLatin1String kPop("#pop");
    static const QLatin1Char kBackSlash('\\');
    static const QLatin1Char kHash('#');
}

Highlighter::Highlighter(QTextDocument *parent, const QString& lexer) :
    SyntaxHighlighter(parent),
    _regionDepth(0),
    _indentationBasedFolding(false),
    //_tabSettings(0),
    _persistentObservableStatesCounter(PersistentsStart),
    _dynamicContextsCounter(0),
    _isBroken(false),
    _lexer(lexer)
{}

Highlighter::~Highlighter()
{}

void Highlighter::init(const QString& themeName)
{
    Settings set;
    Theme::ITheme *theme = themeName.isEmpty() ? set.theme() : set.themeByName(themeName);

    _formats.insert("dsNormal"      , theme->format(_lexer, "dsNormal", QColor("#000000")));
    _formats.insert("dsKeyword"     , theme->format(_lexer, "dsKeyword", QColor("#000000"), true));
    _formats.insert("dsDataType"    , theme->format(_lexer, "dsDataType", QColor("#0057AE")));
    _formats.insert("dsDecVal"      , theme->format(_lexer, "dsNumber", QColor("#B08000")));
    _formats.insert("dsBaseN"       , theme->format(_lexer, "dsNumber", QColor("#B08000")));
    _formats.insert("dsFloat"       , theme->format(_lexer, "dsNumber", QColor("#B08000")));
    _formats.insert("dsChar"        , theme->format(_lexer, "dsChar", QColor("#FF80E0")));
    _formats.insert("dsString"      , theme->format(_lexer, "dsString", QColor("#BF0303")));
    _formats.insert("dsComment"     , theme->format(_lexer, "dsComment", QColor("#898887")));
    _formats.insert("dsOthers"      , theme->format(_lexer, "dsOthers", QColor("#006E28")));
    _formats.insert("dsAlert"       , theme->format(_lexer, "dsAlert", QColor("#BF0303")));
    _formats.insert("dsFunction"    , theme->format(_lexer, "dsFunction", QColor("#644A9B")));
    _formats.insert("dsRegionMarker", theme->format(_lexer, "dsRegionMarker", QColor("#DEDEDE")));
    _formats.insert("dsError"       , theme->format(_lexer, "dsError", QColor("#BF0303")));
}

Highlighter::BlockData::BlockData():
    _originalObservableState(-1)
{}

Highlighter::BlockData::~BlockData()
{}

void  Highlighter::setDefaultContext(const QSharedPointer<Context> &defaultContext)
{
    _defaultContext = defaultContext;
    _persistentObservableStates.insert(_defaultContext->name(), Default);
    _indentationBasedFolding = defaultContext->definition()->isIndentationBasedFolding();
}

void Highlighter::setTabSettings(const TabSettings &ts)
{
    //_tabSettings = &ts;
}

void Highlighter::highlightBlock(const QString &text)
{
    if (!_defaultContext.isNull() && !_isBroken) {
        try {
            if (!currentBlockUserData())
                initializeBlockData();
            setupDataForBlock(text);

            handleContextChange(_currentContext->lineBeginContext(), _currentContext->definition());

            ProgressData progress;
            const int length = text.length();
            while (progress.offset() < length)
                iterateThroughRules(text, length, &progress, false, _currentContext->rules());

            handleContextChange(_currentContext->lineEndContext(), _currentContext->definition(), false);
            _contexts.clear();

            if (_indentationBasedFolding) {
                applyIndentationBasedFolding(text);
            } else {
                applyRegionBasedFolding();

                // In the case region depth has changed since the last time the state was set.
                setCurrentBlockState(computeState(extractObservableState(currentBlockState())));
            }
        } catch (const HighlighterException &) {
            _isBroken = true;
        }
    }

    //applyFormatToSpaces(text, _creatorFormats.value(VisualWhitespace));
}

void Highlighter::setupDataForBlock(const QString &text)
{
    if (extractObservableState(currentBlockState()) == WillContinue)
        analyseConsistencyOfWillContinueBlock(text);

    if (previousBlockState() == -1) {
        _regionDepth = 0;
        setupDefault();
    } else {
        _regionDepth = extractRegionDepth(previousBlockState());
        const int observablePreviousState = extractObservableState(previousBlockState());
        if (observablePreviousState == Default)
            setupDefault();
        else if (observablePreviousState == WillContinue)
            setupFromWillContinue();
        else if (observablePreviousState == Continued)
            setupFromContinued();
        else
            setupFromPersistent();

        blockData(currentBlockUserData())->_foldingRegions = blockData(currentBlock().previous().userData())->_foldingRegions;
    }

    assignCurrentContext();
}

void Highlighter::setupDefault()
{
    _contexts.push_back(_defaultContext);

    setCurrentBlockState(computeState(Default));
}

void Highlighter::setupFromWillContinue()
{
    BlockData *previousData = blockData(currentBlock().previous().userData());
    _contexts.push_back(previousData->_contextToContinue);

    BlockData *data = blockData(currentBlock().userData());
    data->_originalObservableState = previousData->_originalObservableState;

    if (currentBlockState() == -1 || extractObservableState(currentBlockState()) == Default)
        setCurrentBlockState(computeState(Continued));
}

void Highlighter::setupFromContinued()
{
    BlockData *previousData = blockData(currentBlock().previous().userData());

    Q_ASSERT(previousData->_originalObservableState != WillContinue &&
             previousData->_originalObservableState != Continued);

    if (previousData->_originalObservableState == Default ||
        previousData->_originalObservableState == -1) {
        _contexts.push_back(_defaultContext);
    } else {
        pushContextSequence(previousData->_originalObservableState);
    }

    setCurrentBlockState(computeState(previousData->_originalObservableState));
}

void Highlighter::setupFromPersistent()
{
    pushContextSequence(extractObservableState(previousBlockState()));

    setCurrentBlockState(previousBlockState());
}

void Highlighter::iterateThroughRules(const QString &text,
                                      const int length,
                                      ProgressData *progress,
                                      const bool childRule,
                                      const QList<QSharedPointer<Rule> > &rules)
{
    typedef QList<QSharedPointer<Rule> >::const_iterator RuleIterator;

    bool contextChanged = false;
    bool atLeastOneMatch = false;

    RuleIterator it = rules.begin();
    RuleIterator endIt = rules.end();
    while (it != endIt && progress->offset() < length) {
        int startOffset = progress->offset();
        const QSharedPointer<Rule> &rule = *it;
        if (rule->matchSucceed(text, length, progress)) {
            atLeastOneMatch = true;

            if (!_indentationBasedFolding || 1==1) {
                BlockData *data = blockData(currentBlockUserData());
                if (!rule->beginRegion().isEmpty()) {
                    data->_foldingRegions.push(rule->beginRegion());
                    ++_regionDepth;
                    data->_beginRegion = data->_beginRegion > 0 ? qMin(data->_beginRegion, _regionDepth) : _regionDepth;
                    data->_endRegion = -1;
                    if (progress->isOpeningBraceMatchAtFirstNonSpace())
                        ++blockData(currentBlockUserData())->_foldingIndentDelta;
                }
                if (!rule->endRegion().isEmpty()) {
                    QStack<QString> *currentRegions = &blockData(currentBlockUserData())->_foldingRegions;
                    if (!currentRegions->isEmpty() && rule->endRegion() == currentRegions->top()) {
                        currentRegions->pop();
                        data->_beginRegion = -1;
                        data->_endRegion = data->_endRegion > 0 ? qMin(data->_endRegion, _regionDepth) : _regionDepth;
                        --_regionDepth;
                        if (progress->isClosingBraceMatchAtNonEnd())
                            --blockData(currentBlockUserData())->_foldingIndentDelta;
                    }
                }
                progress->clearBracesMatches();
            }

            if (progress->isWillContinueLine()) {
                createWillContinueBlock();
                progress->setWillContinueLine(false);
            } else {
                if (rule->hasChildren())
                    iterateThroughRules(text, length, progress, true, rule->children());

                if (!rule->context().isEmpty() && contextChangeRequired(rule->context())) {
                    _currentCaptures = progress->captures();
                    changeContext(rule->context(), rule->definition());
                    contextChanged = true;
                }
            }

            // Format is not applied to child rules directly (but relative to the offset of their
            // parent) nor to look ahead rules.
            if (!childRule && !rule->isLookAhead()) {
                if (rule->itemData().isEmpty())
                    applyFormat(startOffset, progress->offset() - startOffset,
                                _currentContext->itemData(), _currentContext->definition());
                else
                    applyFormat(startOffset, progress->offset() - startOffset, rule->itemData(),
                                rule->definition());
            }

            // When there is a match of one child rule the others should be skipped. Otherwise
            // the highlighting would be incorret in a case like 9ULLLULLLUULLULLUL, for example.
            if (contextChanged || childRule) {
                break;
            } else {
                it = rules.begin();
                continue;
            }
        } else {
            if (!rule->beginRegion().isEmpty())
                blockData(currentBlockUserData())->_beginRegion = -1;
            if (!rule->endRegion().isEmpty())
                blockData(currentBlockUserData())->_endRegion = -1;
        }
        ++it;
    }

    if (!childRule && !atLeastOneMatch) {
        if (_currentContext->isFallthrough()) {
            handleContextChange(_currentContext->fallthroughContext(),
                                _currentContext->definition());
            iterateThroughRules(text, length, progress, false, _currentContext->rules());
        } else {
            applyFormat(progress->offset(), 1, _currentContext->itemData(),
                        _currentContext->definition());
            if (progress->isOnlySpacesSoFar() && !text.at(progress->offset()).isSpace())
                progress->setOnlySpacesSoFar(false);
            progress->incrementOffset();
        }
    }
}

bool Highlighter::contextChangeRequired(const QString &contextName) const
{
    if (contextName == kStay)
        return false;
    return true;
}

void Highlighter::changeContext(const QString &contextName,
                                const QSharedPointer<HighlightDefinition> &definition,
                                const bool assignCurrent)
{
    if (contextName.startsWith(kPop)) {
        QStringList list = contextName.split(kHash, QString::SkipEmptyParts);
        for (int i = 0; i < list.size(); ++i)
            _contexts.pop_back();

        if (extractObservableState(currentBlockState()) >= PersistentsStart) {
            // One or more contexts were popped during during a persistent state.
            const QString &currentSequence = currentContextSequence();
            if (_persistentObservableStates.contains(currentSequence))
                setCurrentBlockState(
                    computeState(_persistentObservableStates.value(currentSequence)));
            else
                setCurrentBlockState(
                    computeState(_leadingObservableStates.value(currentSequence)));
        }
    } else {
        const QSharedPointer<Context> &context = definition->context(contextName);

        if (context->isDynamic())
            pushDynamicContext(context);
        else
            _contexts.push_back(context);

        if (_contexts.back()->lineEndContext() == kStay ||
            extractObservableState(currentBlockState()) >= PersistentsStart) {
            const QString &currentSequence = currentContextSequence();
            mapLeadingSequence(currentSequence);
            if (_contexts.back()->lineEndContext() == kStay) {
                // A persistent context was pushed.
                mapPersistentSequence(currentSequence);
                setCurrentBlockState(
                    computeState(_persistentObservableStates.value(currentSequence)));
            }
        }
    }

    if (assignCurrent)
        assignCurrentContext();
}

void Highlighter::handleContextChange(const QString &contextName,
                                      const QSharedPointer<HighlightDefinition> &definition,
                                      const bool setCurrent)
{
    if (!contextName.isEmpty() && contextChangeRequired(contextName))
        changeContext(contextName, definition, setCurrent);
}

void Highlighter::applyFormat(int offset,
                              int count,
                              const QString &itemDataName,
                              const QSharedPointer<HighlightDefinition> &definition)
{
    if (count == 0)
        return;

    QSharedPointer<ItemData> itemData;
    try {
        itemData = definition->itemData(itemDataName);
    } catch (const HighlighterException &) {
        // There are some broken files. For instance, the Printf context in java.xml points to an
        // inexistent Printf item data. These cases are considered to have normal text style.
        return;
    }

    const QString formatName = _formats.contains(itemDataName) ? itemDataName : itemData->style();
    if (formatName != "dsNormal"){
        QTextCharFormat format = _formats.value(formatName, QTextCharFormat());
        /*if (itemData->isCustomized()) {
            if (itemData->color().isValid())
                format.setForeground(itemData->color());
            if (itemData->isItalicSpecified())
                format.setFontItalic(itemData->isItalic());
            if (itemData->isBoldSpecified())
                format.setFontWeight(toFontWeight(itemData->isBold()));
            if (itemData->isUnderlinedSpecified())
                format.setFontUnderline(itemData->isUnderlined());
            if (itemData->isStrikeOutSpecified())
                format.setFontStrikeOut(itemData->isStrikeOut());
        }*/
        setFormat(offset, count, format);
    }
}

void Highlighter::createWillContinueBlock()
{
    BlockData *data = blockData(currentBlockUserData());
    const int currentObservableState = extractObservableState(currentBlockState());
    if (currentObservableState == Continued) {
        BlockData *previousData = blockData(currentBlock().previous().userData());
        data->_originalObservableState = previousData->_originalObservableState;
    } else if (currentObservableState != WillContinue) {
        data->_originalObservableState = currentObservableState;
    }
    data->_contextToContinue = _currentContext;

    setCurrentBlockState(computeState(WillContinue));
}

void Highlighter::analyseConsistencyOfWillContinueBlock(const QString &text)
{
    if (currentBlock().next().isValid() && (
        text.length() == 0 || text.at(text.length() - 1) != kBackSlash) &&
        extractObservableState(currentBlock().next().userState()) != Continued) {
        currentBlock().next().setUserState(computeState(Continued));
    }

    if (text.length() == 0 || text.at(text.length() - 1) != kBackSlash) {
        BlockData *data = blockData(currentBlockUserData());
        data->_contextToContinue.clear();
        setCurrentBlockState(computeState(data->_originalObservableState));
    }
}

void Highlighter::mapPersistentSequence(const QString &contextSequence)
{
    if (!_persistentObservableStates.contains(contextSequence)) {
        int newState = _persistentObservableStatesCounter;
        _persistentObservableStates.insert(contextSequence, newState);
        _persistentContexts.insert(newState, _contexts);
        ++_persistentObservableStatesCounter;
    }
}

void Highlighter::mapLeadingSequence(const QString &contextSequence)
{
    if (!_leadingObservableStates.contains(contextSequence))
        _leadingObservableStates.insert(contextSequence,
                                         extractObservableState(currentBlockState()));
}

void Highlighter::pushContextSequence(int state)
{
    const QVector<QSharedPointer<Context> > &contexts = _persistentContexts.value(state);
    for (int i = 0; i < contexts.size(); ++i)
        _contexts.push_back(contexts.at(i));
}

QString Highlighter::currentContextSequence() const
{
    QString sequence;
    for (int i = 0; i < _contexts.size(); ++i)
        sequence.append(_contexts.at(i)->id());

    return sequence;
}

Highlighter::BlockData *Highlighter::initializeBlockData()
{
    BlockData *data = new BlockData;
    setCurrentBlockUserData(data);
    return data;
}

Highlighter::BlockData *Highlighter::blockData(QTextBlockUserData *userData)
{
    return static_cast<BlockData *>(userData);
}

void Highlighter::pushDynamicContext(const QSharedPointer<Context> &baseContext)
{
    // A dynamic context is created from another context which serves as its basis. Then,
    // its rules are updated according to the captures from the calling regular expression which
    // triggered the push of the dynamic context.
    QSharedPointer<Context> context(new Context(*baseContext));
    context->configureId(_dynamicContextsCounter);
    context->updateDynamicRules(_currentCaptures);
    _contexts.push_back(context);
    ++_dynamicContextsCounter;
}

void Highlighter::assignCurrentContext()
{
    if (_contexts.isEmpty()) {
        // This is not supposed to happen. However, there are broken files (for example, php.xml)
        // which will cause this behaviour. In such cases pushing the default context is enough to
        // keep highlighter working.
        _contexts.push_back(_defaultContext);
    }
    _currentContext = _contexts.back();
}

int Highlighter::extractRegionDepth(const int state)
{
    return state >> 12;
}

int Highlighter::extractObservableState(const int state)
{
    return state & 0xFFF;
}

int Highlighter::computeState(const int observableState) const
{
    return _regionDepth << 12 | observableState;
}

void Highlighter::applyRegionBasedFolding() const
{
    int folding = 0;
    BlockData *data = blockData(currentBlockUserData());
    BlockData *previousData = blockData(currentBlock().previous().userData());
    if (previousData) {
        folding = extractRegionDepth(previousBlockState());
        if (data->_foldingIndentDelta != 0) {
            folding += data->_foldingIndentDelta;
            if (data->_foldingIndentDelta > 0)
                data->setFoldingStartIncluded(true);
            else
                previousData->setFoldingEndIncluded(false);
            data->_foldingIndentDelta = 0;
        }
    }
    data->setFoldingEndIncluded(true);
    data->setFoldingIndent(folding);
}

void Highlighter::applyIndentationBasedFolding(const QString &text) const
{
    BlockData *data = blockData(currentBlockUserData());
    data->setFoldingEndIncluded(true);

    // If this line is empty, check its neighbours. They all might be part of the same block.
    if (text.trimmed().isEmpty()) {
        data->setFoldingIndent(0);
        const int previousIndent = neighbouringNonEmptyBlockIndent(currentBlock().previous(), true);
        if (previousIndent > 0) {
            const int nextIndent = neighbouringNonEmptyBlockIndent(currentBlock().next(), false);
            if (previousIndent == nextIndent)
                data->setFoldingIndent(previousIndent);
        }
    } else {
        data->setFoldingIndent(_tabSettings.indentationColumn(text));
    }
}

int Highlighter::neighbouringNonEmptyBlockIndent(QTextBlock block, const bool previous) const
{
    while (true) {
        if (!block.isValid())
            return 0;
        if (block.text().trimmed().isEmpty()) {
            if (previous)
                block = block.previous();
            else
                block = block.next();
        } else {
            return _tabSettings.indentationColumn(block.text());
        }
    }
}

}
