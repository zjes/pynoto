#include <QDebug>
#include <QTextBlock>
#include "BaseDocumentLayout.h"

namespace TextEditor {


BaseDocumentLayout::BaseDocumentLayout(QTextDocument *doc):
    QPlainTextDocumentLayout(doc)
{
}

BaseDocumentLayout::~BaseDocumentLayout()
{

}

void BaseDocumentLayout::emitDocumentSizeChanged()
{
    emit documentSizeChanged(documentSize());
}

QSizeF BaseDocumentLayout::documentSize() const
{
    QSizeF size = QPlainTextDocumentLayout::documentSize();
    QTextBlock block = document()->firstBlock();
    int height = 0;
    while(block.isValid()){
        if (block.isVisible())
            height += blockBoundingRect(block).height();
        block = block.next();
    }
    size.setHeight(height);
    return size;
}

TextBlockData::TextBlockData():
    _foldingIndent(0),
    _foldingStartIncluded(false),
    _foldingEndIncluded(false),
    _foldingIndentDelta(0),
    _beginRegion(-1),
    _endRegion(-1)
{
}

TextBlockData::~TextBlockData()
{
}

}
