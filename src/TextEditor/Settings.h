#ifndef _SETTINGS_H_
#define _SETTINGS_H_
#include <QColor>
#include <QFont>
#include <QSettings>

namespace Theme{
    class ITheme;
}

namespace TextEditor {
class Settings : public QSettings
{
    Q_OBJECT
    Q_PROPERTY(bool wordWrap READ wordWrap WRITE setWordWrap)
    Q_PROPERTY(bool hightlight READ hightlight WRITE setHightlight)
    Q_PROPERTY(QString fontFace READ fontFace WRITE setFontFace)
    Q_PROPERTY(int fontSize READ fontSize WRITE setFontSize)
    Q_PROPERTY(QString themeName READ themeName WRITE setThemeName)
    Q_PROPERTY(QFont::Style fontStyle READ fontStyle WRITE setFontStyle)
    Q_PROPERTY(int fontWeight READ fontWeight WRITE setFontWeight)
    Q_PROPERTY(bool useAntialias READ useAntialias WRITE setUseAntialias)
    Q_PROPERTY(bool showWhitespace READ showWhitespace WRITE setShowWhitespace)
    Q_PROPERTY(bool showIndentLine READ showIndentLine WRITE setShowIndentLine)
    Q_PROPERTY(bool showRightMargin READ showRightMargin WRITE setShowRightMargin)
public:
    bool wordWrap();
    void setWordWrap(bool wrap);

    bool hightlight();
    void setHightlight(bool show);

    QString fontFace();
    void setFontFace(const QString& face);

    int fontSize();
    void setFontSize(int size);

    QFont::Style fontStyle();
    void setFontStyle(QFont::Style style);

    int fontWeight();
    void setFontWeight(int weight);

    QString themeName();
    void setThemeName(const QString& name);

    bool useAntialias();
    void setUseAntialias(bool use);

    bool showWhitespace();
    void setShowWhitespace(bool show);

    bool showIndentLine();
    void setShowIndentLine(bool show);

    QColor identColor();
    void setIdentColor(const QColor& color);

    bool showRightMargin();
    void setShowRightMargin(bool show);

    Theme::ITheme* theme();
    Theme::ITheme* themeByName(const QString& name);
};
}
#endif
