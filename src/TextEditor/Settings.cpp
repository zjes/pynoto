#include <QDir>
#include <QApplication>
#include <QFont>
#include "Include/Aux.h"
#include "Include/IThemePlugin.h"
#include "Settings.h"

namespace TextEditor {

bool Settings::wordWrap()
{
    return value("texteditor/wordwrap", false).toBool();
}

void Settings::setWordWrap(bool wrap)
{
    setValue("texteditor/wordwrap", wrap);
}

bool Settings::hightlight()
{
    return value("texteditor/highlight", true).toBool();
}

void Settings::setHightlight(bool show)
{
    setValue("texteditor/highlight", show);
}

QString Settings::fontFace()
{
    return value("texteditor/fontface", QApplication::font().family()).toString();
}

void Settings::setFontFace(const QString& face)
{
    setValue("texteditor/fontface", face);
}

int Settings::fontSize()
{
    return value("texteditor/fontsize", QApplication::font().pixelSize()).toInt();
}

void Settings::setFontSize(int size)
{
    setValue("texteditor/fontsize", size);
}

QFont::Style Settings::fontStyle()
{
    return (QFont::Style)value("texteditor/fontstyle", QFont::StyleNormal).toInt();
}

void Settings::setFontStyle(QFont::Style style)
{
    setValue("texteditor/fontstyle", style);
}

int Settings::fontWeight()
{
    return value("texteditor/fontweight", QFont::Normal).toInt();
}

void Settings::setFontWeight(int weight)
{
    setValue("texteditor/fontweight", weight);
}

QString Settings::themeName()
{
    return value("texteditor/theme", "default").toString();
}

void Settings::setThemeName(const QString& name)
{
    setValue("texteditor/theme", name);
}

bool Settings::useAntialias()
{
    return value("texteditor/antialias", true).toBool();
}

void Settings::setUseAntialias(bool use)
{
    setValue("texteditor/antialias", use);
}


Theme::ITheme* Settings::theme()
{
    return themeByName(themeName());
}

Theme::ITheme* Settings::themeByName(const QString& name)
{
    return Aux::plugin<Theme::IThemePlugin*>("theme")->theme(name);
}

bool Settings::showWhitespace()
{
    return value("texteditor/showwhitespace", false).toBool();
}

void Settings::setShowWhitespace(bool show)
{
    return setValue("texteditor/showwhitespace", show);
}

bool Settings::showIndentLine()
{
    return value("texteditor/showidentline", true).toBool();
}

void Settings::setShowIndentLine(bool show)
{
    return setValue("texteditor/showidentline", show);
}

QColor Settings::identColor()
{
    return QColor(value("texteditor/indentColor", "#FF0000").toString());
}

void Settings::setIdentColor(const QColor& color)
{
    setValue("texteditor/indentColor", color.name());
}

bool Settings::showRightMargin()
{
    return value("texteditor/rightmargin", true).toBool();
}

void Settings::setShowRightMargin(bool show)
{
    setValue("texteditor/rightmargin", show);
}


}
