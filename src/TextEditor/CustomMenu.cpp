#include <QMenu>
#include "CustomMenu.h"
#include "TextEditorWidget.h"
#include "Include/IIconProvider.h"
#include "Include/Aux.h"

namespace TextEditor {

CustomMenu::CustomMenu(TextEditorWidget* parent):
    QObject(parent),
    _editor(parent)
{
}

CustomMenu::~CustomMenu()
{

}

void CustomMenu::createMenu(const QPoint& point)
{
    QMenu *menu = new QMenu(_editor);
    QAction *act = menu->addAction(Aux::icons()->icon("edit-copy"), tr("Copy"), _editor, SLOT(copy()));
    act->setEnabled(!_editor->selectedText().isEmpty());
    act = menu->addAction(Aux::icons()->icon("edit-cut"), tr("Cut"), _editor, SLOT(cut()));
    act->setEnabled(!_editor->selectedText().isEmpty());
    act = menu->addAction(Aux::icons()->icon("edit-paste"), tr("Paste"), _editor, SLOT(paste()));
    act->setEnabled(_editor->canPaste());
    act = menu->addAction(Aux::icons()->icon("edit-delete"), tr("Delete"), _editor, SLOT(del()));
    act->setEnabled(!_editor->selectedText().isEmpty());
    menu->addSeparator();
    act = menu->addAction(tr("Uppercase"), _editor, SLOT(uppercase()));
    act->setEnabled(!_editor->selectedText().isEmpty());
    act = menu->addAction(tr("Lowercase"), _editor, SLOT(lowercase()));
    act->setEnabled(!_editor->selectedText().isEmpty());
    act = menu->addAction(tr("Capitalize"), _editor, SLOT(capitalize()));
    act->setEnabled(!_editor->selectedText().isEmpty());
    act = menu->addAction(tr("Camel case"), _editor, SLOT(camelize()));
    act->setEnabled(!_editor->selectedText().isEmpty());
    act = menu->addAction(tr("Under score case"), _editor, SLOT(underlize()));
    act->setEnabled(!_editor->selectedText().isEmpty());
    menu->popup(_editor->mapToGlobal(point));
}

}
