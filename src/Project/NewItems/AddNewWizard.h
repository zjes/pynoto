#ifndef _ADDNEWWIZARD_H_
#define _ADDNEWWIZARD_H_

#include <QWizard>
#include <QDir>
#include "Node.h"

namespace Project {
class IProject;
class AddNewWizard: public QWizard
{
    Q_OBJECT
public:
    AddNewWizard(const QString& path, NodeTypes type, IProject * project, QWidget * parent);
public:
    enum {
        SelectTape,
        PythonClass,
        PythonFile,
        PythonModule,
        PythonQtClass,
        QtDesigner,
        QmlFile,
        JsFile,
        AnyFile,
        AnyDir
    };
    virtual int	nextId() const;
private:
    void saveFileContent(const QString& fileName, const QString& content);
private slots:
    void onAccept();
    void createClass();
    void createFile(const QString& ext);
    void createModule();
    void createQtClass();
    void createQtDesigner();
    void createDirectory();
private:
    QDir _path;
    NodeTypes _type;
    IProject * _project;
};

}

#endif
