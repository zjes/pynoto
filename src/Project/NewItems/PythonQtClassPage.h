#ifndef _PYTHONQTCLASSPAGE_H_
#define _PYTHONQTCLASSPAGE_H_

#include "CreateNewPage.h"

namespace Ui {
class PythonQtClassPage;
}

namespace Project {

class PythonQtClassPage : public CreateNewPage
{
    Q_OBJECT
public:
    explicit PythonQtClassPage(const QDir& path, IProject * project, QWidget *parent = 0);
    virtual ~PythonQtClassPage();
protected:
    virtual void initializePage();
private slots:
    void onClassNameChanged(const QString&);
private:
    Ui::PythonQtClassPage *_ui;
    QString _path;
};

}

#endif
