#include "PythonQtClassPage.h"
#include "ui_PythonQtClassPage.h"

namespace Project {

PythonQtClassPage::PythonQtClassPage(const QDir& path, IProject * project, QWidget *parent) :
    CreateNewPage(path, project, parent),
    _ui(new Ui::PythonQtClassPage)
{
    _ui->setupUi(this);
    setTitle(tr("Create python qt form class"));

    _ui->edtClassFile->setDefaultExtension("py");
    _ui->edtUiFile->setDefaultExtension("ui");
    _ui->edtUiGenFile->setDefaultExtension("py");

    _ui->edtClassFile->setPath(path.absolutePath());
    _ui->edtUiFile->setPath(path.absolutePath());
    _ui->edtUiGenFile->setPath(path.absolutePath());

    registerField("qfClassName*", _ui->edtClassName);
    registerField("qfClassKind*", _ui->cmbKind, "currentText", SIGNAL(editTextChanged(QString)));
    registerField("qfClassFile*", _ui->edtClassFile, "fileName", SIGNAL(fileNameChanged(QString)));
    registerField("qfUiFile*", _ui->edtUiFile, "fileName", SIGNAL(fileNameChanged(QString)));
    registerField("qfUiGenFile*", _ui->edtUiGenFile, "fileName", SIGNAL(fileNameChanged(QString)));

    connect(_ui->edtClassName, SIGNAL(textChanged(QString)), SLOT(onClassNameChanged(QString)));
}

PythonQtClassPage::~PythonQtClassPage()
{
    delete _ui;
}

void PythonQtClassPage::initializePage()
{
    QStringList items;
    items << "" << "QMainWindow" << "QDialog" << "QWidget" << "QWizard"
          << "QWizardPage" << "QStackedWidget" << "QToolBox"
          << "QTabWidget" << "QMdiArea" << "QScrollArea"
          << "QGroupBox" << "QFrame" << "QDockWidget";
    _ui->cmbKind->addItems(items);
}

void PythonQtClassPage::onClassNameChanged(const QString& text)
{
    _ui->edtClassFile->setText(text+".py");
    _ui->edtUiFile->setText(text+".ui");
    _ui->edtUiGenFile->setText("Ui_"+text+".py");
}

}
