#ifndef TEMPLATE_H
#define TEMPLATE_H

#include <QString>
#include <QMap>

class Block
{
public:
    Block(){}
    Block(const QString& content);
    virtual ~Block();
    virtual QString render(const QMap<QString, QString> & items);
    void setContent(const QString& content);
private:
    void parserInnerBlock(const QString& name);
private:
    QString _content;
    QList<Block*> _blocks;
};

class Condition: public Block
{
public:
    Condition(const QString& def, const QString& content);
    virtual ~Condition();

    virtual QString render(const QMap<QString, QString> & items);
private:
    enum Operand{
        Equal,
        NotEqual,
        More,
        Less,
        MoreEqual,
        LessEqual,
        Wrong
    };
    bool parseCondition(const QString& what, Operand op, const QString& value, const QMap<QString, QString> & items);
    Operand operand(const QString& op);
    QString _def;
};

class Template: public Block
{
public:
    Template(int pyVersion, const QString & name);
    virtual ~Template();
    static QStringList listTemplates(int pyVersion);
public:
    //QString render(const QMap<QString, QString> & items);
};

#endif // TEMPLATE_H
