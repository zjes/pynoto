#include "QtDesignerPage.h"
#include "ui_QtDesignerPage.h"

namespace Project {

QtDesignerPage::QtDesignerPage(const QDir& path, IProject * project, QWidget *parent) :
    CreateNewPage(path, project, parent),
    _ui(new Ui::QtDesignerPage)
{
    _ui->setupUi(this);
    setTitle(tr("Create qt designer form"));

    _ui->edtUiFile->setDefaultExtension("ui");
    _ui->edtUiGenFile->setDefaultExtension("py");

    _ui->edtUiFile->setPath(path.absolutePath());
    _ui->edtUiGenFile->setPath(path.absolutePath());

    registerField("uiClassName*", _ui->edtClassName);
    registerField("formKind*", _ui->cmbKind, "currentText", SIGNAL(editTextChanged(QString)));
    registerField("uiFile*", _ui->edtUiFile, "fileName", SIGNAL(fileNameChanged(QString)));
    registerField("uiGenFile*", _ui->edtUiGenFile, "fileName", SIGNAL(fileNameChanged(QString)));

    connect(_ui->edtClassName, SIGNAL(textChanged(QString)), SLOT(onClassNameChanged(QString)));
}

QtDesignerPage::~QtDesignerPage()
{
    delete _ui;
}

void QtDesignerPage::initializePage()
{
    QStringList items;
    items << "" << "QMainWindow" << "QDialog" << "QWidget" << "QWizard"
          << "QWizardPage" << "QStackedWidget" << "QToolBox"
          << "QTabWidget" << "QMdiArea" << "QScrollArea"
          << "QGroupBox" << "QFrame" << "QDockWidget";
    _ui->cmbKind->addItems(items);
}

void QtDesignerPage::onClassNameChanged(const QString& text)
{
    _ui->edtUiFile->setText(text+".ui");
    _ui->edtUiGenFile->setText("Ui_"+text+".py");
}

}
