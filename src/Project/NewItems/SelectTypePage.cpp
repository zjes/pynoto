#include "SelectTypePage.h"
#include "ui_SelectTypePage.h"
#include "AddNewWizard.h"
#include "Include/IProject.h"

namespace Project {

SelectTypePage::SelectTypePage(const QDir& path, IProject * project, NodeTypes type, QWidget * parent):
    CreateNewPage(path, project, parent),
    _ui(new Ui::SelectTypePage)
{
    _ui->setupUi(this);

    setTitle(tr("Choose item to create"));

    items.append(tr("Python class"));
    items.append(tr("Empty Python file"));
    items.append(tr("Python module"));
    items.append(tr("Python Qt Designer class form"));
    items.append(tr("Qt Designer form"));
    items.append(tr("Qml file"));
    items.append(tr("Javascript file"));
    items.append(tr("File"));
    items.append(tr("Directory"));

    _ui->items->addItems(items);
    connect(_ui->items, SIGNAL(currentRowChanged(int)), SLOT(onItemChanged(int)));

    if (_project->guiName() != "PyQt4" && project->guiName() != "PySide"){
        _ui->items->item(3)->setFlags(!Qt::ItemIsEditable & !Qt::ItemIsSelectable);
        _ui->items->item(4)->setFlags(!Qt::ItemIsEditable & !Qt::ItemIsSelectable);
        _ui->items->item(5)->setFlags(!Qt::ItemIsEditable & !Qt::ItemIsSelectable);
    }
    _ui->items->setCurrentRow(0);
}

SelectTypePage::~SelectTypePage()
{
    delete _ui;
}

void SelectTypePage::onItemChanged(int row)
{
    switch(row){
    case 0: _ui->descr->setText(tr("Creates custom python class type with default constructor.")); break;
    case 1: _ui->descr->setText(tr("Creates empty python file")); break;
    case 2: _ui->descr->setText(tr("Creates empty python module")); break;
    case 3: _ui->descr->setText(tr("Creates designer class form")); break;
    case 4: _ui->descr->setText(tr("Creates Qt Designer Form")); break;
    case 5: _ui->descr->setText(tr("Creates empty QML file")); break;
    case 6: _ui->descr->setText(tr("Creates javascript file")); break;
    case 7: _ui->descr->setText(tr("Creates any file")); break;
    case 8: _ui->descr->setText(tr("Creates directory")); break;
    }
}

int SelectTypePage::next() const
{
    switch(_ui->items->currentRow()){
    case 0:
        return AddNewWizard::PythonClass;
    case 1:
        return AddNewWizard::PythonFile;
    case 2:
        return AddNewWizard::PythonModule;
    case 3:
        return AddNewWizard::PythonQtClass;
    case 4:
        return AddNewWizard::QtDesigner;
    case 5:
        return AddNewWizard::QmlFile;
    case 6:
        return AddNewWizard::JsFile;
    case 7:
        return AddNewWizard::AnyFile;
    case 8:
        return AddNewWizard::AnyDir;
    }
    return -1;
}

}
