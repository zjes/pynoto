#include <QDebug>
#include <QFile>
#include <QDir>
#include <QStringBuilder>
#include <QRegExp>
#include <QStringList>
#include "Template.h"

Template::Template(int pyVersion, const QString & name):
    Block()
{
    QString path = QString("python%1/templates/%2.txt").arg(pyVersion).arg(name);
    QString content;
    QFile file(path);
    if (file.exists()){
        file.open(QIODevice::ReadOnly);
        content = file.readAll();
        file.close();
    }
    setContent(content);
}


Template::~Template()
{

}

QStringList Template::listTemplates(int pyVersion)
{
    QDir path(QString("python%1/templates/").arg(pyVersion));
    return path.entryList();
}

QString Block::render(const QMap<QString, QString> & items)
{
    foreach(QString key, items.keys()){
        _content = _content.replace("{"%key%"}", items[key]);
    }
    parserInnerBlock("if");
    int count = 0;
    foreach(Block *con, _blocks){
        _content.replace(QString("___cond%1____").arg(count++), con->render(items));
    }

    return _content;
}

void Block::parserInnerBlock(const QString& name)
{
    QString startTag = QString("\\{\\-?%1\\s+").arg(name);
    QString endTag = QString("\\{\\-?end%1\\}").arg(name);

    int pos = _content.indexOf(QRegExp(startTag));
    while(pos != -1){
        int end = _content.indexOf(QRegExp(endTag), pos + 1);
        if (end == -1)
            break;
        int innerIf = _content.indexOf(QRegExp(startTag), pos + 1);
        int count = innerIf != -1 && innerIf < end ? 1 : 0;
        while(count > 0){
            end = _content.indexOf(QRegExp(endTag), end + 1);
            innerIf = _content.indexOf(QRegExp(startTag), innerIf + 1);
            if (innerIf == -1 || innerIf > end)
                --count;
        }
        int startTagEnd = _content.indexOf("}", pos) + 1;
        if (_content[pos+1] == '-'){
            if (_content[startTagEnd] == '\n')
                ++startTagEnd;
            while(pos > 0){
                if (_content[pos-1] == ' ' || _content[pos-1] == '\t')
                    --pos;
                else
                    break;
            }
        }
        int endTagEnd = _content.indexOf("}", end+1) + 1;
        if (_content[end+1] == '-'){
            if (_content[endTagEnd] == '\n')
                ++endTagEnd;
            while(end > 0){
                if (_content[end-1] == ' ' || _content[end-1] == '\t')
                    --end;
                else
                    break;
            }
        }
        QString def = _content.mid(pos, startTagEnd - pos);
        QString innerContent = _content.mid(startTagEnd, end-startTagEnd);
        Condition* con = new Condition(def, innerContent);
        _content = _content.replace(pos, endTagEnd - pos, QString("___cond%1____").arg(_blocks.length()));
        _blocks.append(con);
        pos = _content.indexOf(QRegExp(startTag));
    }

}

Block::Block(const QString& content):
    _content(content)
{

}

Block::~Block()
{
    qDeleteAll(_blocks);
}

void Block::setContent(const QString& content)
{
    _content = content;
}

Condition::Condition(const QString& def, const QString& content):
    Block(content),
    _def(def)
{
}

Condition::~Condition()
{
}

QString Condition::render(const QMap<QString, QString> & items)
{
    bool result = false;
    QRegExp def("\\{\\-?if\\s+(\\S+)\\s*(==|!=|>|<|>=|<=)\\s*(\\S+)\\s*\\}");
    if (def.indexIn(_def) != -1){
        result = parseCondition(def.cap(1), operand(def.cap(2)), def.cap(3), items);
    }
    return result ? Block::render(items) : "";
}

bool Condition::parseCondition(const QString& what, Operand op, const QString& value, const QMap<QString, QString> & items)
{
    if (!items.contains(what))
        return false;
    QVariant compValue = items[what];
    QVariant toValue = value;
    if (compValue.type() == QVariant::String && value.startsWith("\"")){
        toValue = value.mid(1, value.length()-2);
    }
    toValue.convert(compValue.type());
    switch(op){
    case Equal:
        return compValue == toValue;
    case NotEqual:
        return compValue != toValue;
    default:
        break;
    }
    if (compValue.type() == QVariant::Int || compValue.type() == QVariant::Double){
        float numValue = compValue.toFloat();
        float numToValue = toValue.toFloat();
        switch(op){
        case More:
            return numValue > numToValue;
        case Less:
            return numValue < numToValue;
        case MoreEqual:
            return numValue >= numToValue;
        case LessEqual:
            return numValue <= numToValue;
        default:
            break;
        }
    }
    return false;
}

Condition::Operand Condition::operand(const QString& op)
{
    if (op == "==")
        return Equal;
    if (op == "!=")
        return NotEqual;
    if (op == ">")
        return More;
    if (op == "<")
        return Less;
    if (op == ">=")
        return MoreEqual;
    if (op == "<=")
        return LessEqual;

    return Wrong;
}
