#include <QDebug>
#include <QDir>
#include "AddNewWizard.h"
#include "SelectTypePage.h"
#include "PythonClassPage.h"
#include "Template.h"
#include "Include/IProject.h"
#include "PythonFilePage.h"
#include "PythonModulePage.h"
#include "PythonQtClassPage.h"
#include "QtDesignerPage.h"

namespace Project {

AddNewWizard::AddNewWizard(const QString& path, NodeTypes type, IProject * project, QWidget * parent):
    QWizard(parent),
    _path(QDir(path)),
    _type(type),
    _project(project)
{
    setWindowTitle(QString(tr("Create new item in \"%1\"")).arg(project->name()));
    setPage(SelectTape, new SelectTypePage(_path, project, _type, this));
    setPage(PythonClass, new PythonClassPage(_path, project, this));
    setPage(PythonFile, new PythonFilePage(tr("Create empty python file"), "py", _path, project, this));
    setPage(PythonModule, new PythonModulePage(_path, project, this));
    setPage(PythonQtClass, new PythonQtClassPage(_path, project, this));
    setPage(QtDesigner, new QtDesignerPage(_path, project, this));
    setPage(QmlFile, new PythonFilePage(tr("Create empty qml file"), "qml", _path, project, this));
    setPage(JsFile, new PythonFilePage(tr("Create empty javascript file"), "js", _path, project, this));
    setPage(AnyFile, new PythonFilePage(tr("Create empty file"), "", _path, project, this));
    setPage(AnyDir, new PythonFilePage(tr("Create empty directory"), "", _path, project, this, true));

    connect(this, SIGNAL(accepted()), SLOT(onAccept()));
}

int	AddNewWizard::nextId() const
{
    if (currentId() == SelectTape)
        return qobject_cast<SelectTypePage*>(page(SelectTape))->next();
    return -1;
}

void AddNewWizard::onAccept()
{
    switch(currentId()){
    case PythonClass:
        createClass(); break;
    case PythonFile:
        createFile("py"); break;
    case PythonModule:
        createModule(); break;
    case PythonQtClass:
        createQtClass(); break;
    case QtDesigner:
        createQtDesigner(); break;
    case QmlFile:
        createFile("qml"); break;
    case JsFile:
        createFile("js"); break;
    case AnyFile:
        createFile(""); break;
    case AnyDir:
        createDirectory(); break;
    }
}

void AddNewWizard::createClass()
{
    QString parentName = field("parentClassName").toString();
    if (parentName.isEmpty())
        parentName = "object";

    QMap<QString, QString> params;
    params["className"] = field("className").toString();
    params["fileName"] = field("fileName").toString();
    params["parentClassName"] = parentName;
    params["parentClassImport"] = field("parentClassImport").toString();

    QString content;
    if (parentName.startsWith("Q")){
        Template tmpl(_project->pythonMajorVersion(), "python-qt-class");
        content = tmpl.render(params);
    } else {
        Template tmpl(_project->pythonMajorVersion(), "python-class");
        content = tmpl.render(params);
    }
    if (!content.isEmpty()){
        saveFileContent(params["fileName"], content);
    }
}

void AddNewWizard::createFile(const QString& ext)
{
    saveFileContent(field("empty"+ext+"FileName").toString(), "");
}

void AddNewWizard::createModule()
{
    _path.mkdir(field("moduleName").toString());
    saveFileContent(field("moduleName").toString()+QDir::separator()+"__init__.py", "");
}

void AddNewWizard::createQtClass()
{
    QMap<QString, QString> params;
    params["className"] = field("qfClassName").toString();
    params["fileName"] = field("qfClassFile").toString();
    params["parentClassName"] = field("qfClassKind").toString();
    params["parentClassImport"] = "from "+_project->guiName()+".QtGui import "+params["parentClassName"];
    params["uiFile"] = field("qfUiFile").toString();
    params["uiGenFile"] = field("qfUiGenFile").toString();

    Template tmpl(_project->pythonMajorVersion(), "python-qt-class");
    saveFileContent(params["fileName"], tmpl.render(params));


    QString tmplName = "qtform-"+params["parentClassName"];
    QStringList templates = Template::listTemplates(_project->pythonMajorVersion());
    if (!templates.contains(tmplName))
        tmplName = "qtform-QWidget";

    Template ftmpl(_project->pythonMajorVersion(), tmplName);
    saveFileContent(params["uiFile"], ftmpl.render(params));
}

void AddNewWizard::createQtDesigner()
{
    QMap<QString, QString> params;
    params["className"] = field("uiClassName").toString();
    params["parentClassName"] = field("formKind").toString();
    params["uiFile"] = field("uiFile").toString();
    params["uiGenFile"] = field("uiGenFile").toString();

    QString tmplName = "qtform-"+params["parentClassName"];
    QStringList templates = Template::listTemplates(_project->pythonMajorVersion());
    if (!templates.contains(tmplName))
        tmplName = "qtform-QWidget";

    Template ftmpl(_project->pythonMajorVersion(), tmplName);
    saveFileContent(params["uiFile"], ftmpl.render(params));
}

void AddNewWizard::createDirectory()
{
    _path.mkdir(field("emptyDir").toString());
}

void AddNewWizard::saveFileContent(const QString& fileName, const QString& content)
{
    QFile file(_path.filePath(fileName));
    if (file.open(QIODevice::WriteOnly)){
        file.write(content.toUtf8());
        file.close();
        _project->openFile(_path.filePath(fileName));
    }
}

}
