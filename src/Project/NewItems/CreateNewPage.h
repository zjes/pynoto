#ifndef _CREATENEWPAGE_H_
#define _CREATENEWPAGE_H_

#include <QWizardPage>
#include <QDir>

namespace Project {

class IProject;

class CreateNewPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit CreateNewPage(const QDir& path, IProject * project, QWidget *parent = 0);
protected:
    QDir _path;
    IProject* _project;
};

}
#endif
