#include "PythonFilePage.h"
#include "ui_PythonFilePage.h"

namespace Project {
PythonFilePage::PythonFilePage(const QString& title, const QString& ext, const QDir& path, IProject* project, QWidget *parent, bool isDir) :
    CreateNewPage(path, project, parent),
    _ui(new Ui::PythonFilePage)
{
    _ui->setupUi(this);
    setTitle(title);

    _ui->edtFileName->setPath(path.absolutePath());
    _ui->edtFileName->setDefaultExtension(ext);

    registerField(!isDir ? "empty"+ext+"FileName*" : "emptyDir", _ui->edtFileName, "fileName", SIGNAL(fileNameChanged(QString)));
}

PythonFilePage::~PythonFilePage()
{
    delete _ui;
}

}
