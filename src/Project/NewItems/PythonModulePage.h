#ifndef _PYTHONMODULEPAGE_H_
#define _PYTHONMODULEPAGE_H_

#include "CreateNewPage.h"

namespace Ui {
class PythonModulePage;
}

namespace Project {

class PythonModulePage : public CreateNewPage
{
    Q_OBJECT
public:
    explicit PythonModulePage(const QDir& path, IProject * project, QWidget *parent = 0);
    virtual ~PythonModulePage();
protected:
    virtual void initializePage();
private:
    Ui::PythonModulePage *_ui;
};

}
#endif
