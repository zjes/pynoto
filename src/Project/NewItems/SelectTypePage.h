#ifndef _SELECTTYPEPAGE_H_
#define _SELECTTYPEPAGE_H_
#include "CreateNewPage.h"
#include "Node.h"


namespace Ui {
    class SelectTypePage;
}

namespace Project {

class SelectTypePage: public CreateNewPage
{
    Q_OBJECT
public:
    SelectTypePage(const QDir& path, IProject * project, NodeTypes type, QWidget * parent = 0);
    virtual ~SelectTypePage();
    int next() const;
private slots:
    void onItemChanged(int);
private:
    Ui::SelectTypePage * _ui;
    QList<QString> items;
};

}

#endif
