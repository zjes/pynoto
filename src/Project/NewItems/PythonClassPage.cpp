#include "PythonClassPage.h"
#include "ui_PythonClassPage.h"

namespace Project {

PythonClassPage::PythonClassPage(const QDir& path, IProject * project, QWidget *parent) :
    CreateNewPage(path, project, parent),
    _ui(new Ui::PythonClassPage)
{
    _ui->setupUi(this);

    setTitle(tr("Create python class"));
    connect(_ui->edtClassName, SIGNAL(textChanged(QString)), SLOT(onClassNameChanged(QString)));

    registerField("className*", _ui->edtClassName);
    registerField("fileName*", _ui->edtFileName, "fileName", SIGNAL(fileNameChanged(QString)));
    registerField("parentClassName", _ui->edtParent);
    registerField("parentClassImport", _ui->edtImport);

    _ui->edtClassName->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z0-9]{3,}"), this));
    _ui->edtFileName->setPath(path.absolutePath());
    _ui->edtFileName->setDefaultExtension("py");
}

PythonClassPage::~PythonClassPage()
{
    delete _ui;
}

void PythonClassPage::initializePage()
{
}

void PythonClassPage::onClassNameChanged(const QString& text)
{
    _ui->edtFileName->setText(!text.isEmpty() ? text+".py" : "");
}

}
