#include "PythonModulePage.h"
#include "ui_PythonModulePage.h"

namespace Project {

PythonModulePage::PythonModulePage(const QDir& path, IProject * project, QWidget *parent) :
    CreateNewPage(path, project, parent),
    _ui(new Ui::PythonModulePage)
{
    _ui->setupUi(this);
    _ui->edtFileName->setPath(path.absolutePath());

    setTitle(tr("Create python module"));
    registerField("moduleName*", _ui->edtFileName);
}

PythonModulePage::~PythonModulePage()
{
    delete _ui;
}

void PythonModulePage::initializePage()
{
}

}
