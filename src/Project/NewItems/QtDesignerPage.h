#ifndef _QTDESIGNERPAGE_H_
#define _QTDESIGNERPAGE_H_

#include "CreateNewPage.h"

namespace Ui {
class QtDesignerPage;
}

namespace Project {

class QtDesignerPage : public CreateNewPage
{
    Q_OBJECT
public:
    explicit QtDesignerPage(const QDir& path, IProject * project, QWidget *parent = 0);
    virtual ~QtDesignerPage();
protected:
    virtual void initializePage();
private slots:
    void onClassNameChanged(const QString& text);
private:
    Ui::QtDesignerPage * _ui;
};

}
#endif
