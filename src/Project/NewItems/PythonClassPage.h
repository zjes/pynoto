#ifndef _PYTHONCLASSPAGE_H_
#define _PYTHONCLASSPAGE_H_

#include "CreateNewPage.h"

namespace Ui {
class PythonClassPage;
}

namespace Project {

class PythonClassPage : public CreateNewPage
{
    Q_OBJECT
    
public:
    explicit PythonClassPage(const QDir& path, IProject * project, QWidget *parent = 0);
    virtual ~PythonClassPage();
private slots:
    void onClassNameChanged(const QString& text);
protected:
    virtual void initializePage();
private:
    Ui::PythonClassPage *_ui;
};

}
#endif // PYTHONCLASSPAGE_H
