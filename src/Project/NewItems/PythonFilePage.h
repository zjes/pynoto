#ifndef _PYTHONFILEPAGE_H_
#define _PYTHONFILEPAGE_H_

#include "CreateNewPage.h"

namespace Ui {
class PythonFilePage;
}

namespace Project {

class PythonFilePage : public CreateNewPage
{
    Q_OBJECT
public:
    explicit PythonFilePage(const QString& title, const QString& ext, const QDir& path, IProject* project, QWidget *parent = 0, bool isDir = false);
    virtual ~PythonFilePage();
private:
    Ui::PythonFilePage *_ui;
};

}
#endif
