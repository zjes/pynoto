#include <QDebug>
#include "CreateNewPage.h"
#include "Include/IProject.h"

namespace Project {

CreateNewPage::CreateNewPage(const QDir& path, IProject * project, QWidget *parent) :
    QWizardPage(parent),
    _path(path),
    _project(project)
{
    if (!project->path().isEmpty()){
        QDir prjPath(project->path());
        setSubTitle(QString(tr("In (%1)/%2")).arg(project->name()).arg(prjPath.relativeFilePath(_path.absolutePath())));
    } else {
        setSubTitle(QString(tr("In %1")).arg(_path.absolutePath()));
    }
}

}
