#include <QDebug>
#include <QDir>
#include "FindInProject.h"
#include "ui_FindInProject.h"
#include "ProjectFile.h"

namespace Project {

FindInProject::FindInProject(ProjectFile *project, Main::ISearchResultConsole * console, QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::FindInProject),
    _project(project),
    _console(console)
{
    _ui->setupUi(this);

    _ui->srchEdt->addItem("", 0);
    for(auto& srch: _project->searchHistory()){
        _ui->srchEdt->addItem(srch.search, srch.mask);
    }
    connect(_ui->srchEdt, (void (QComboBox::*)(int))&QComboBox::activated, [&](int index){
        int mask = _ui->srchEdt->itemData(index).toInt();
        _ui->isCaseSens->setChecked(mask & CaseSensetive);
        _ui->isWhole->setChecked(mask & WholeWords);
        _ui->isRegex->setChecked(mask & Regular);
    });
    connect(_ui->buttonBox, &QDialogButtonBox::rejected, this, &FindInProject::reject);
    connect(_ui->buttonBox, &QDialogButtonBox::accepted, this, &FindInProject::startSearch);
}

FindInProject::~FindInProject()
{
    delete _ui;
}

void FindInProject::startSearch()
{
    QString search = _ui->srchEdt->currentText();
    int mask = 0;
    mask |= (_ui->isCaseSens->isChecked() ? CaseSensetive: 0);
    mask |= (_ui->isWhole->isChecked() ? WholeWords: 0);
    mask |= (_ui->isRegex->isChecked() ? Regular: 0);
    _project->addSearchHistory(search, mask);

    _console->setResult(runSearch(search, mask, "*.py"));
}

QList<Main::FoundItems> FindInProject::runSearch(const QString& search, int mask, const QString& filter, const QString& path)
{
    QList<Main::FoundItems> list;
    QDir dir(path.isEmpty() ? _project->path() : path);
    for(QFileInfo& item : dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Dirs)){
        if (item.fileName() == "__pycache__")
            continue;
        list += runSearch(search, mask, filter, item.filePath());
    }

    dir.setNameFilters(filter.split(";"));
    QString find = search;
    if (mask & WholeWords)
        find = "\\b"+search+"\\b";

    QRegExp exp(find, mask & CaseSensetive ? Qt::CaseSensitive : Qt::CaseInsensitive);
    if (!(mask & (Regular | WholeWords)))
        exp.setPatternSyntax(QRegExp::FixedString);

    for(QFileInfo& item: dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files)){
        QFile file(item.filePath());
        if (file.open(QIODevice::ReadOnly)) {
            QString str;
            QTextStream in(&file);
            int line = 1;
            while (!in.atEnd()) {
                str = in.readLine();
                if (str.contains(exp)) {
                    list.append({
                        item.filePath(),
                        str,
                        line
                    });
                }
                ++line;
            }
        }
    }
    return list;
}

}
