#include <QSettings>
#include <QFileInfo>
#include <QDir>
#include <QTimer>
#include "ProjectFile.h"
#include "Include/Aux.h"
#include "Include/IPythonCodePlugin.h"
#include "Include/IPythonCode.h"
#include "Include/IPythonPlugin.h"

namespace Project {

ProjectFile::ProjectFile():
    _loaded(false),
    _settings(NULL),
    _pythonCode(NULL)
{
    _python = Aux::plugin<Python::IPythonPlugin*>("python");
}

ProjectFile::~ProjectFile()
{
    delete _settings;
    delete _pythonCode;
}

bool ProjectFile::load(const QString& fileName)
{
    if (_loaded){
        unload();
    }
    QString projectFile = fileName;
    QFileInfo info(projectFile);
    if (info.isDir()){
        QDir dir(projectFile);
        foreach(QString item, dir.entryList(QDir::Files)){
            if (item.split(".").last() == "pynotoprj")
                projectFile = projectFile+QDir::separator()+item;
        }
    }
    QFileInfo file(projectFile);

    _settings = new QSettings(file.absoluteFilePath(), QSettings::IniFormat);
    _loaded = true;

    emit loaded();
    if (QColor(_settings->value("common/color", "transparent").toString()).alpha() != 0){
        Aux::emitPreferencesChanged("panel-color", QColor(_settings->value("common/color", "transparent").toString()));
    }
    return _loaded;
}

void ProjectFile::unload()
{
    delete _settings;
    _settings = NULL;
    _loaded = false;
    if (_pythonCode){
        _pythonCode->close();
        delete _pythonCode;
        _pythonCode = NULL;
    }

    emit unloaded();
}

QString ProjectFile::path()
{
    if (!_settings)
        return "";
    return QFileInfo(_settings->fileName()).absolutePath();
}

void ProjectFile::setPath(const QString& p)
{
    if (_settings)
        delete _settings;
    _settings = new QSettings(p, QSettings::IniFormat);
}


QStringList ProjectFile::shownFiles()
{
    return _settings->value("common/showfiles", "*.py;*.ui;*.qrc;*.png;*.jpg;*.svg").toString().split(";");
}

QString ProjectFile::name()
{
    if (!_settings)
        return "";
    return _settings->value("common/name", "").toString();
}

void ProjectFile::setName(const QString& name)
{
    if (!_settings)
        return;
    _settings->setValue("common/name", name);
}

QString ProjectFile::interpretName()
{
    if (!_settings)
        return "";
    return _settings->value("python/interpret", "").toString();
}

QString ProjectFile::interpretExec()
{
    if (!_settings)
        return "";

    Python::IPythonPlugin::Version ver = _python->pyVersion(interpretName());
    if (!ver.exec.isEmpty())
        return ver.exec;
    return "";
}

QStringList ProjectFile::interpretPathes()
{
    if (!_settings)
        return QStringList();
    Python::IPythonPlugin::Version ver = _python->pyVersion(interpretName());
    if (ver.exec.isEmpty())
        return ver.pathes;
    return QStringList();
}

QString ProjectFile::pythonUic()
{
    if (!_settings)
        return "";
    Python::IPythonPlugin::Version ver = _python->pyVersion(interpretName());

    if (guiName() == "PySide")
        return ver.pySide.uic;

    return ver.pyQt4.uic;
}

QString ProjectFile::fileName()
{
    if (!_settings)
        return "";
    return _settings->fileName();
}

QStringList ProjectFile::openedDirs()
{
    if (!_settings)
        return QStringList();
    QStringList list;
    int size = _settings->beginReadArray("openedDirs");
    for (int i = 0; i < size; ++i) {
        _settings->setArrayIndex(i);
        list.append(_settings->value("dir").toString());
    }
    _settings->endArray();
    return list;
}

void ProjectFile::setOpenedDirs(const QStringList& dirs)
{
    if (!_settings)
        return;
    _settings->remove("openedDirs");
    _settings->beginWriteArray("openedDirs");
    for (int i = 0; i < dirs.length(); ++i) {
        _settings->setArrayIndex(i);
        _settings->setValue("dir", dirs[i]);
    }
    _settings->endArray();
    _settings->sync();
}


QStringList ProjectFile::openedFiles()
{
    if (!_settings)
        return QStringList();
    QStringList list;
    int size = _settings->beginReadArray("openedFiles");
    for (int i = 0; i < size; ++i) {
        _settings->setArrayIndex(i);
        list.append(_settings->value("file").toString());
    }
    _settings->endArray();
    return list;
}

void ProjectFile::openFile(const QString& fileName, int offset)
{
    if (!_settings){
        emit openFileOffset(fileName, offset);
        return;
    }
    QStringList files = openedFiles();
    if (!files.contains(fileName)){
        files.append(fileName);
        _settings->remove("openedFiles");
        _settings->beginWriteArray("openedFiles");
        for (int i = 0; i < files.length(); ++i) {
            _settings->setArrayIndex(i);
            _settings->setValue("file", files[i]);
        }
        _settings->endArray();
        _settings->sync();
    }
    emit openFileOffset(fileName, offset);
}

void ProjectFile::closeFile(const QString& fileName)
{
    if (!_settings){
        emit fileNeedToClose(fileName);
        return;
    }
    QStringList files = openedFiles();
    if (files.contains(fileName)){
        files.removeAll(fileName);
        _settings->remove("openedFiles");
        _settings->beginWriteArray("openedFiles");
        for (int i = 0; i < files.length(); ++i) {
            _settings->setArrayIndex(i);
            _settings->setValue("file", files[i]);
        }
        _settings->endArray();
        _settings->sync();
    }
    emit fileNeedToClose(fileName);
}

QString ProjectFile::mainScript()
{
    if (!_settings)
        return "";
    return _settings->value("common/mainscript", "").toString();
}

QString ProjectFile::fileDisplayName(const QString& fileName)
{
    if (!_settings)
        return fileName;
    QDir info(path());
    return info.relativeFilePath(fileName);
}

PythonCode::IPythonCode* ProjectFile::pythonCode()
{
    if (!_settings)
        return NULL;
    if (!_pythonCode && !interpretName().isEmpty()){
        _pythonCode = Aux::plugin<PythonCode::IPythonCodePlugin*>("pythoncode")->python();
        _pythonCode->init(interpretExec(), path(), mainScript(), _settings->fileName(), interpretPathes() + extraPathes());
    }
    if (_pythonCode)
        connect(_pythonCode, SIGNAL(systemError(QString)), SIGNAL(systemError(QString)));
    return _pythonCode;
}

QString ProjectFile::guiName()
{
    if (!_settings)
        return "";
    return _settings->value("python/guiname", "").toString();
}

void ProjectFile::setShownFiles(const QStringList& list)
{
    if (!_settings)
        return;
    QStringList newList;
    foreach(QString it, list){
        newList.append(it.trimmed());
    }
    newList.removeDuplicates();
    if (shownFiles() != newList){
        _settings->setValue("common/showfiles", newList.join(";"));
        emit shownFilesChanged();
    }
}

void ProjectFile::setMainScript(const QString& mainScript)
{
    if (!_settings)
        return;
    _settings->setValue("common/mainscript", mainScript);
}

void ProjectFile::setInterpretName(const QString& name)
{
    if (!_settings)
        return;
    if (interpretName() != name){
        _settings->setValue("python/interpret", name);
        if (_pythonCode){
            _pythonCode->init(interpretExec(), path(), mainScript(), _settings->fileName(), interpretPathes() + extraPathes());
            emit lintChanged();
        }
        emit interpretChanged();
    }
}

void ProjectFile::setGuiName(const QString& name)
{
    if (!_settings)
        return;
    _settings->setValue("python/guiname", name);
}

QMap<QString, QVariant> ProjectFile::codeCheck()
{
    QMap<QString, QVariant> check;
    check.insert("dummyVars"        , _settings->value("lint/dummyVar", "_|dummy"));
    check.insert("badFunctions"     , _settings->value("lint/badFunc", "map, filter, apply, input"));
    check.insert("goodNames"        , _settings->value("lint/goodNames", "i, j, k, ex, Run, _"));
    check.insert("badNames"         , _settings->value("lint/badNames", "foo, bar, baz"));
    check.insert("maxLineLength"    , _settings->value("lint/maxLineLength", 80));
    check.insert("maxModuleLenght"  , _settings->value("lint/maxModLines", 1000));


    check.insert("moduleStyle"      , _settings->value("lint/checkModule", "(([a-z_][a-z0-9_]*)|([A-Z][a-zA-Z0-9]+))$"));
    check.insert("constStyle"       , _settings->value("lint/checkConst", "(([A-Z_][A-Z0-9_]*)|(__.*__))$"));
    check.insert("classStyle"       , _settings->value("lint/checkClass", "[A-Z_][a-zA-Z0-9]+$"));
    check.insert("funcStyle"        , _settings->value("lint/checkFunc", "[a-z_][a-z0-9_]{2,30}$"));
    check.insert("methodStyle"      , _settings->value("lint/checkMethod", "[a-z_][a-z0-9_]{2,30}$"));
    check.insert("attrStyle"        , _settings->value("lint/checkAttr", "[a-z_][a-z0-9_]{2,30}$"));
    check.insert("argStyle"         , _settings->value("lint/checkArg", "[a-z_][a-z0-9_]{2,30}$"));
    check.insert("varStyle"         , _settings->value("lint/checkVar", "[a-z_][a-z0-9_]{2,30}$"));
    check.insert("inlVarStyle"      , _settings->value("lint/checkInlineVar", "[A-Za-z_][A-Za-z0-9_]*$"));


    check.insert("maxArgs"          , _settings->value("lint/maxArgs", 5));
    check.insert("ignoreNames"      , _settings->value("lint/ignArgNames", "_.*"));
    check.insert("maxLocals"        , _settings->value("lint/maxLocals", 15));
    check.insert("maxReturns"       , _settings->value("lint/maxReturns", 6));
    check.insert("maxBranchs"       , _settings->value("lint/maxBranchs", 12));
    check.insert("maxStats"         , _settings->value("lint/maxStats", 50));
    check.insert("maxParents"       , _settings->value("lint/maxParents", 7));
    check.insert("maxAttr"          , _settings->value("lint/maxAttrs", 7));
    check.insert("minPublic"        , _settings->value("lint/minPublic", 2));
    check.insert("maxPublic"        , _settings->value("lint/maxPublic", 20));

    check.insert("useStyle"         , _settings->value("lint/style", 0));

    return check;
}

#define SETINT(confname, mapname) \
    if (_settings->value(confname).toInt() != check[mapname].toInt()){ \
        changed |= true; \
        _settings->setValue(confname, check[mapname]); \
    }

#define SETTEXT(confname, mapname) \
    if (_settings->value(confname, "").toString() != check[mapname].toString()){ \
        changed |= true; \
        _settings->setValue(confname, check[mapname]); \
    }

void ProjectFile::setCodeCheck(const QMap<QString, QVariant>& check)
{
    bool changed = false;
    int rmChanged = _settings->value("lint/maxLineLength").toInt() != check["maxLineLength"];

    SETTEXT("lint/dummyVar"      , "dummyVars");
    SETTEXT("lint/badFunc"       , "badFunctions");
    SETTEXT("lint/goodNames"     , "goodNames");
    SETTEXT("lint/badNames"      , "badNames");
    SETINT ("lint/maxLineLength" , "maxLineLength");
    SETINT ("lint/maxModLines"   , "maxModuleLenght");

    SETTEXT("lint/checkModule"   , "moduleStyle");
    SETTEXT("lint/checkConst"    , "constStyle");
    SETTEXT("lint/checkClass"    , "classStyle");
    SETTEXT("lint/checkFunc"     , "funcStyle");
    SETTEXT("lint/checkMethod"   , "methodStyle");
    SETTEXT("lint/checkAttr"     , "attrStyle");
    SETTEXT("lint/checkArg"      , "argStyle");
    SETTEXT("lint/checkVar"      , "varStyle");
    SETTEXT("lint/checkInlineVar", "inlVarStyle");

    SETINT ("lint/maxArgs"       , "maxArgs");
    SETTEXT("lint/ignArgNames"   , "ignoreNames");
    SETINT ("lint/maxLocals"     , "maxLocals");
    SETINT ("lint/maxReturns"    , "maxReturns");
    SETINT ("lint/maxBranchs"    , "maxBranchs");
    SETINT ("lint/maxStats"      , "maxStats");
    SETINT ("lint/maxParents"    , "maxParents");
    SETINT ("lint/maxAttrs"      , "maxAttr");
    SETINT ("lint/minPublic"     , "minPublic");
    SETINT ("lint/maxPublic"     , "maxPublic");

    SETINT ("lint/style"         , "useStyle");

    _settings->sync();
    if (changed){
        _pythonCode->init(interpretExec(), path(), mainScript(), _settings->fileName(), interpretPathes() + extraPathes());
        emit lintChanged();
    }

    if (rmChanged){
        Aux::emitPreferencesChanged("rightMarginChanged");
    }
}

QColor ProjectFile::color()
{
    if (!_settings)
        return Qt::transparent;
    return QColor(_settings->value("common/color", "transparent").toString());
}

void ProjectFile::setColor(const QColor& color)
{
    if (!_settings)
        return;
    if (color.alpha() == 0)
        _settings->setValue("common/color", "transparent");
    else
        _settings->setValue("common/color", color.name());

    Aux::emitPreferencesChanged("panel-color", color);
}

QStringList ProjectFile::extraPathes()
{
    return _settings->value("common/pathes").toStringList();
}

void ProjectFile::setExtraPathes(const QStringList& pathes)
{
    bool changed = pathes != extraPathes();
    _settings->setValue("common/pathes", pathes);
    _settings->sync();;
    if (changed){
        _pythonCode->init(interpretExec(), path(), mainScript(), _settings->fileName(), interpretPathes() + extraPathes());
        emit lintChanged();
    }
}

void ProjectFile::openDir(const QString& path)
{
    QStringList dirs = openedDirs();
    if (!dirs.contains(path)){
        dirs.append(path);
        setOpenedDirs(dirs);
    }
}

void ProjectFile::closeDir(const QString& path)
{
    QStringList dirs = openedDirs();
    dirs.removeAll(path);
    setOpenedDirs(dirs);
}

int ProjectFile::pythonMajorVersion()
{
    //TODO Fix it
    return 3;
}

int ProjectFile::rightMarginWidth()
{
    if (!_settings)
        return 80;
    return _settings->value("lint/maxLineLength", 80).toInt();
}

QList<SearchHistory> ProjectFile::searchHistory()
{
    QList<SearchHistory> list;
    if (!_settings)
        return list;

    int size = _settings->beginReadArray("searchHistory");
    for (int i = 0; i < size; ++i) {
        _settings->setArrayIndex(i);
        list.append({
            _settings->value("str").toString(),
            _settings->value("mask").toInt()
        });
    }
    _settings->endArray();
    return list;
}

void ProjectFile::addSearchHistory(const QString& search, int mask)
{
    QList<SearchHistory> old = searchHistory();
    SearchHistory srch{search, mask};
    bool found = false;
    for(SearchHistory& oldSrch: old){
        if (oldSrch == srch){
            old.removeOne(oldSrch);
            old.prepend(srch);
            found = true;
            break;
        }
    }
    if (!found)
        old.prepend(srch);

    _settings->remove("searchHistory");
    _settings->beginWriteArray("searchHistory");
    for (int i = 0; i < old.length(); ++i) {
        _settings->setArrayIndex(i);
        _settings->setValue("str", old[i].search);
        _settings->setValue("mask", old[i].mask);
        if (i == 10)
            break;
    }
    _settings->endArray();
    _settings->sync();
}

}
