#ifndef _PATHESMODEL_H_
#define _PATHESMODEL_H_
#include <QStringList>
#include <QAbstractListModel>

namespace Project {

class PathesModel: public QAbstractListModel
{
    Q_OBJECT
public:
    PathesModel(QStringList* cPathes, QObject *parent = NULL);
    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
public slots:
    QModelIndex addCustomPath(const QString& path);
    QModelIndex deleteCustomPath(const QModelIndex& index);
private:
    QStringList * _cPathes;
};

}
#endif
