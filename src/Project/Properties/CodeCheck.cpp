#include "CodeCheck.h"
#include "ui_CodeCheck.h"
#include "ProjectFile.h"

namespace Project {

CodeCheck::CodeCheck(QWidget* parent):
    QWidget(parent),
    _ui(new Ui::CodeCheck),
    _project(NULL)
{
    _ui->setupUi(this);
}

CodeCheck::~CodeCheck()
{
    delete _ui;
}

void CodeCheck::setProject(ProjectFile* project)
{
    _project = project;
    initBasic();
}

void CodeCheck::initBasic()
{
    QMap<QString, QVariant> check = _project->codeCheck();
    _ui->dummyVars->setText(check["dummyVars"].toString());
    _ui->badFunctions->setText(check["badFunctions"].toString());
    _ui->goodNames->setText(check["goodNames"].toString());
    _ui->badNames->setText(check["badNames"].toString());
    _ui->maxLineLength->setValue(check["maxLineLength"].toInt());
    _ui->maxModuleLenght->setValue(check["maxModuleLenght"].toInt());

    _ui->moduleStyle->setText(check["moduleStyle"].toString());
    _ui->constStyle->setText(check["constStyle"].toString());
    _ui->classStyle->setText(check["classStyle"].toString());
    _ui->funcStyle->setText(check["funcStyle"].toString());
    _ui->methodStyle->setText(check["methodStyle"].toString());
    _ui->attrStyle->setText(check["attrStyle"].toString());
    _ui->argStyle->setText(check["argStyle"].toString());
    _ui->varStyle->setText(check["varStyle"].toString());
    _ui->inlVarStyle->setText(check["inlVarStyle"].toString());

    _ui->maxArgs->setValue(check["maxArgs"].toInt());
    _ui->ignoreNames->setText(check["ignoreNames"].toString());
    _ui->maxLocals->setValue(check["maxLocals"].toInt());
    _ui->maxReturns->setValue(check["maxReturns"].toInt());
    _ui->maxBranchs->setValue(check["maxBranchs"].toInt());
    _ui->maxStats->setValue(check["maxStats"].toInt());
    _ui->maxParents->setValue(check["maxParents"].toInt());
    _ui->maxAttr->setValue(check["maxAttr"].toInt());
    _ui->minPublic->setValue(check["minPublic"].toInt());
    _ui->maxPublic->setValue(check["maxPublic"].toInt());

    switch(check["useStyle"].toInt()){
    case 1: _ui->camelStyle->setChecked(true); _ui->styleGroup->setEnabled(false); break;
    case 2: _ui->customStyle->setChecked(true); _ui->styleGroup->setEnabled(true); break;
    default: _ui->underscoreStyle->setChecked(true); _ui->styleGroup->setEnabled(false); break;
    }

    connect(_ui->camelStyle, SIGNAL(toggled(bool)), SLOT(onCamelToggled(bool)));
    connect(_ui->underscoreStyle, SIGNAL(toggled(bool)), SLOT(onUnderscoreToggled(bool)));
    connect(_ui->customStyle, SIGNAL(toggled(bool)), SLOT(onCustomToggled(bool)));
}

void CodeCheck::onCamelToggled(bool toggled)
{
    if (!toggled)
        return;

    _ui->moduleStyle->setText("(([a-z_][a-z0-9_]*)|([A-Z][a-zA-Z0-9]+))$");
    _ui->constStyle->setText("(([A-Z_][A-Z0-9_]*)|(__.*__))$");
    _ui->classStyle->setText("[A-Z_][a-zA-Z0-9]+$");
    _ui->funcStyle->setText("[a-z_][a-zA-Z0-9_]{2,30}$");
    _ui->methodStyle->setText("[a-z_][a-zA-Z0-9_]{2,30}$");
    _ui->attrStyle->setText("[a-z_][a-zA-Z0-9_]{2,30}$");
    _ui->argStyle->setText("[a-z_][a-zA-Z0-9_]{2,30}$");
    _ui->varStyle->setText("[a-z_][a-zA-Z0-9_]{2,30}$");
    _ui->inlVarStyle->setText("[A-Za-z_][A-Za-z0-9_]*$");

    _ui->styleGroup->setEnabled(false);
}

void CodeCheck::onCustomToggled(bool toggled)
{
    if (!toggled)
        return;

    QMap<QString, QVariant> check = _project->codeCheck();
    _ui->moduleStyle->setText(check["moduleStyle"].toString());
    _ui->constStyle->setText(check["constStyle"].toString());
    _ui->classStyle->setText(check["classStyle"].toString());
    _ui->funcStyle->setText(check["funcStyle"].toString());
    _ui->methodStyle->setText(check["methodStyle"].toString());
    _ui->attrStyle->setText(check["attrStyle"].toString());
    _ui->argStyle->setText(check["argStyle"].toString());
    _ui->varStyle->setText(check["varStyle"].toString());
    _ui->inlVarStyle->setText(check["inlVarStyle"].toString());

    _ui->styleGroup->setEnabled(true);
}

void CodeCheck::onUnderscoreToggled(bool toggled)
{
    if (!toggled)
        return;

    _ui->moduleStyle->setText("(([a-z_][a-z0-9_]*)|([A-Z][a-zA-Z0-9]+))$");
    _ui->constStyle->setText("(([A-Z_][A-Z0-9_]*)|(__.*__))$");
    _ui->classStyle->setText("[A-Z_][a-zA-Z0-9]+$");
    _ui->funcStyle->setText("[a-z_][a-z0-9_]{2,30}$");
    _ui->methodStyle->setText("[a-z_][a-z0-9_]{2,30}$");
    _ui->attrStyle->setText("[a-z_][a-z0-9_]{2,30}$");
    _ui->argStyle->setText("[a-z_][a-z0-9_]{2,30}$");
    _ui->varStyle->setText("[a-z_][a-z0-9_]{2,30}$");
    _ui->inlVarStyle->setText("[A-Za-z_][A-Za-z0-9_]*$");

    _ui->styleGroup->setEnabled(false);
}

void CodeCheck::save()
{
    QMap<QString, QVariant> check = _project->codeCheck();

    check["dummyVars"]       = _ui->dummyVars->text();
    check["badFunctions"]    = _ui->badFunctions->text();
    check["goodNames"]       = _ui->goodNames->text();
    check["badNames"]        = _ui->badNames->text();
    check["maxLineLength"]   = _ui->maxLineLength->value();
    check["maxModuleLenght"] = _ui->maxModuleLenght->value();


    check["moduleStyle"]     = _ui->moduleStyle->text();
    check["constStyle"]      = _ui->constStyle->text();
    check["classStyle"]      = _ui->classStyle->text();
    check["funcStyle"]       = _ui->funcStyle->text();
    check["methodStyle"]     = _ui->methodStyle->text();
    check["attrStyle"]       = _ui->attrStyle->text();
    check["argStyle"]        = _ui->argStyle->text();
    check["varStyle"]        = _ui->varStyle->text();
    check["inlVarStyle"]     = _ui->inlVarStyle->text();

    check["maxArgs"]         = _ui->maxArgs->value();
    check["ignoreNames"]     = _ui->ignoreNames->text();
    check["maxLocals"]       = _ui->maxLocals->value();
    check["maxReturns"]      = _ui->maxReturns->value();
    check["maxBranchs"]      = _ui->maxBranchs->value();
    check["maxStats"]        = _ui->maxStats->value();
    check["maxParents"]      = _ui->maxParents->value();
    check["maxAttr"]         = _ui->maxAttr->value();
    check["minPublic"]       = _ui->minPublic->value();
    check["maxPublic"]       = _ui->maxPublic->value();

    if (_ui->camelStyle->isChecked())
        check["useStyle"] = 1;
    else if (_ui->underscoreStyle->isChecked())
        check["useStyle"] = 0;
    else
        check["useStyle"] = 2;

    _project->setCodeCheck(check);
}

}
