#include "ProjectProperties.h"
#include "ui_ProjectProperties.h"
#include "General.h"

namespace Project {

ProjectProperties::ProjectProperties(ProjectFile* project, QWidget *parent):
    QDialog(parent),
    ui(new Ui::ProjectProperties),
    _project(project)
{
    ui->setupUi(this);

    ui->project->setProject(project);
    ui->code->setProject(project);

    connect(ui->buttonBox, SIGNAL(accepted()), SLOT(onAccept()));
    connect(ui->buttonBox, SIGNAL(rejected()), SLOT(reject()));
}

ProjectProperties::~ProjectProperties()
{
    delete ui;
}

void ProjectProperties::onAccept()
{
    ui->project->save();
    ui->code->save();
    accept();
}

}
