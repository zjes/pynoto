#ifndef _GENERAL_H_
#define _GENERAL_H_
#include <QWidget>
#include <QModelIndex>

namespace Ui {
    class General;
}

namespace Project {
class ProjectFile;

class General: public QWidget
{
    Q_OBJECT
public:
    General(QWidget* parent = NULL);
    virtual ~General();
    bool save();
    void setProject(ProjectFile* project);
private slots:
    void setupPython();
    void refreshPyVersions();
    void refreshGuiVersions();
    void onPythonChanged(int);

    void onAddPath();
    void onDeletePath();
    void onPathActivated(const QModelIndex& index);
private:
    Ui::General *_ui;
    ProjectFile* _project;
    QStringList _pathes;
};

}

#endif
