#ifndef _PROJECTPROPERTIES_H_
#define _PROJECTPROPERTIES_H_
#include <QDialog>

namespace Ui {
    class ProjectProperties;
}

namespace Project {

class ProjectFile;
class ProjectProperties: public QDialog
{
    Q_OBJECT
public:
    ProjectProperties(ProjectFile* project, QWidget *parent);
    virtual ~ProjectProperties();
private slots:
    void onAccept();
private:
    Ui::ProjectProperties *ui;
    ProjectFile *_project;
};

}
#endif
