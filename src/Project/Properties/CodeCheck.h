#ifndef _CODECHECK_H_
#define _CODECHECK_H_
#include <QWidget>

namespace Ui { class CodeCheck; }

namespace Project {
class ProjectFile;

class CodeCheck: public QWidget
{
    Q_OBJECT
public:
    CodeCheck(QWidget* parent = NULL);
    virtual ~CodeCheck();

    void setProject(ProjectFile* project);
    void save();
private:
    void initBasic();
private slots:
    void onCamelToggled(bool);
    void onCustomToggled(bool);
    void onUnderscoreToggled(bool);
private:
    Ui::CodeCheck *_ui;
    ProjectFile * _project;
};

}

#endif
