#include <QDebug>
#include "PathesModel.h"

namespace Project {

PathesModel::PathesModel(QStringList * cPathes, QObject *parent):
    QAbstractListModel(parent),
    _cPathes(cPathes)
{

}

int PathesModel::rowCount(const QModelIndex & parent) const
{
    return _cPathes->length();
}

QVariant PathesModel::data(const QModelIndex & index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole){
        return _cPathes->at(index.row());
    }
    return QVariant();
}

QModelIndex PathesModel::addCustomPath(const QString& path)
{
    beginInsertRows(QModelIndex(), _cPathes->length() - 1, _cPathes->length() - 1);
    _cPathes->append(path);
    QModelIndex index = createIndex(_cPathes->length() - 1, 0);
    endInsertRows();
    return index;
}

QModelIndex PathesModel::deleteCustomPath(const QModelIndex& index)
{
    beginRemoveRows(QModelIndex(), index.row(), index.row());
    _cPathes->removeAt(index.row());
    QModelIndex ret = createIndex(index.row()-1 > 0 ? index.row()-1 : 0, 0);
    endRemoveRows();
    return ret;
}

}
