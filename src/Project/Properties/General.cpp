#include <QFileDialog>
#include "Include/Aux.h"
#include "Include/IPythonPlugin.h"
#include "General.h"
#include "PathesModel.h"
#include "ProjectFile.h"
#include "ui_General.h"

namespace Project {

General::General(QWidget* parent):
    QWidget(parent),
    _ui(new Ui::General),
    _project(NULL)
{
    _ui->setupUi(this);
    connect(_ui->btnSetupPython, SIGNAL(clicked()), SLOT(setupPython()));
}


General::~General()
{
    delete _ui;
}

void General::setProject(ProjectFile* project)
{
    _project = project;

    _ui->edtProjectName->setText(_project->name());
    _ui->mainScript->setText(_project->mainScript());
    _ui->mainScript->setDefaultPath(_project->path());
    _ui->edtFileToShow->setText(_project->shownFiles().join(";"));
    _ui->projectColor->setColor(_project->color());

    _pathes = _project->extraPathes();
    _ui->pathes->setModel(new PathesModel(&_pathes, this));

    refreshPyVersions();
    refreshGuiVersions();

    connect(_ui->cmbPyVersion, SIGNAL(currentIndexChanged(int)), SLOT(onPythonChanged(int)));
    connect(_ui->btnAddCustomPath, SIGNAL(clicked()), SLOT(onAddPath()));
    connect(_ui->btnDeleteCustomPath, SIGNAL(clicked()), SLOT(onDeletePath()));
    connect(_ui->pathes, SIGNAL(activated(QModelIndex)), SLOT(onPathActivated(QModelIndex)));
}

void General::refreshGuiVersions()
{
    _ui->cmdFrameworks->clear();
    Python::IPythonPlugin* py = Aux::plugin<Python::IPythonPlugin*>("python");
    QStringList fram = py->guiFrameworks(_ui->cmbPyVersion->currentText());
    if (fram.length()){
        _ui->cmdFrameworks->setEnabled(true);
        _ui->cmdFrameworks->addItem(tr("None"), "");
        foreach(QString f, fram){
            _ui->cmdFrameworks->addItem(f, f);
        }
        if (!_project->guiName().isEmpty()){
            _ui->cmdFrameworks->setCurrentIndex(_ui->cmdFrameworks->findText(_project->guiName()));
        }
    } else {
        _ui->cmdFrameworks->setEnabled(false);
    }
}

void General::onPythonChanged(int)
{
    refreshGuiVersions();
}

void General::setupPython()
{
    Python::IPythonPlugin* py = Aux::plugin<Python::IPythonPlugin*>("python");
    py->setupVersions(this);
    refreshPyVersions();
}

void General::refreshPyVersions()
{
    Python::IPythonPlugin* py = Aux::plugin<Python::IPythonPlugin*>("python");
    _ui->cmbPyVersion->clear();
    QList<Python::IPythonPlugin::Version> vers = py->listVersions();
    _ui->cmbPyVersion->addItem(tr("I don't know"));
    for(int i = 0; i < vers.length(); ++i){
        _ui->cmbPyVersion->addItem(vers[i].name, vers[i].name);
    }
    int index = _ui->cmbPyVersion->findText(_project->interpretName());
    if (index >= 0)
        _ui->cmbPyVersion->setCurrentIndex(index);
}

bool General::save()
{
    _project->setName(_ui->edtProjectName->text());
    _project->setMainScript(_ui->mainScript->text());
    _project->setShownFiles(_ui->edtFileToShow->text().split(";"));
    _project->setInterpretName(_ui->cmbPyVersion->itemData(_ui->cmbPyVersion->currentIndex()).toString());
    _project->setGuiName(_ui->cmdFrameworks->itemData(_ui->cmdFrameworks->currentIndex()).toString());
    _project->setColor(_ui->projectColor->color());
    _project->setExtraPathes(_pathes);
    return true;
}

void General::onAddPath()
{
    QFileDialog dlg(this);
    dlg.setFileMode(QFileDialog::DirectoryOnly);
    dlg.setWindowTitle(tr("Select extra path"));
    dlg.setDirectory(_project->path());
    if (dlg.exec() == QDialog::Accepted)
        qobject_cast<PathesModel*>(_ui->pathes->model())->addCustomPath(dlg.directory().absolutePath());
}

void General::onDeletePath()
{
    qobject_cast<PathesModel*>(_ui->pathes->model())->deleteCustomPath(_ui->pathes->currentIndex());
}

void General::onPathActivated(const QModelIndex& index)
{
    _ui->btnDeleteCustomPath->setEnabled(index.isValid());
}


}
