#ifndef _CREATEPROJECTDLG_H_
#define _CREATEPROJECTDLG_H_

#include <QDialog>

namespace Ui { class CreateProjectDlg; }
namespace Project {

class CreateProjectDlg: public QDialog
{
    Q_OBJECT
public:
    CreateProjectDlg(QWidget *parent);
    virtual ~CreateProjectDlg();

    QString projectPath();
    QString name();
private slots:
    void onNameChanged(const QString& text);
private:
    Ui::CreateProjectDlg *_ui;
};

}
#endif
