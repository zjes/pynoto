#ifndef _IPROJECTPLUGIN_H_
#define _IPROJECTPLUGIN_H_
#include "Include/IPlugin.h"
#include "Include/IProject.h"

class QAction;
namespace Main {
class IApplicationOutput;
class ISearchResultConsole;
}
namespace Project {

class IProjectPlugin: public IPlugin
{
    Q_OBJECT
public:
    virtual ~IProjectPlugin(){}

    virtual QWidget  * widget(QWidget * parent) = 0;
    virtual IProject * project() = 0;
signals:
    void preferecesChanged();
    void runned();
    void stoped();
    void output(const QString& msg);
    void error(const QString& msg);
    void fileDeleted(const QString& file);
public slots:
    virtual void editProperties(QWidget * parent) = 0;
    virtual void createNewProject(QWidget *parent) = 0;
    virtual void run() = 0;
    virtual void stop() = 0;
    virtual void fileActivated(const QString&) = 0;
    virtual void fileModificationChanged(const QString&, bool) = 0;
    virtual void createFile() = 0;
    virtual void findInProject(QWidget *parent, Main::ISearchResultConsole * console) = 0;
};

}

Q_DECLARE_INTERFACE(Project::IProjectPlugin, "Pynoto.IProjectPlugin")

#endif
