#ifndef _FINDINPROJECT_H_
#define _FINDINPROJECT_H_

#include <QDialog>
#include "Include/ISearchResultConsole.h"

namespace Ui {
class FindInProject;
}

namespace Project {
class ProjectFile;

class FindInProject : public QDialog
{
    Q_OBJECT
public:
    explicit FindInProject(ProjectFile *project, Main::ISearchResultConsole * console, QWidget *parent = 0);
    virtual ~FindInProject();
private slots:
    void startSearch();
private:
    QList<Main::FoundItems> runSearch(const QString& search, int mask, const QString& filter, const QString& path = "");
private:
    enum SearchMask
    {
        CaseSensetive = 1,
        WholeWords = 2,
        Regular = 4
    };

    Ui::FindInProject *_ui;
    ProjectFile *_project;
    Main::ISearchResultConsole *_console;
};

}
#endif
