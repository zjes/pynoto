#ifndef _PROJECTTREEMODEL_H_
#define _PROJECTTREEMODEL_H_

#include <QMutex>
#include <QAbstractItemModel>
#include <QStringList>
class QFileSystemWatcher;
class QSettings;
namespace IconProvider{
    class IIconProvider;
}
class QFileInfo;

namespace Project {

class Node;
class IProject;
class ProjectTreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit ProjectTreeModel(IProject *project, QObject *parent = 0);
    virtual ~ProjectTreeModel();

    QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex & index) const;
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    int columnCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    QList<QModelIndex> indexForPath(const QString & path);
    void setModify(const QString & path, bool m);
public slots:
    void onDirectoryChanged(const QString &, bool rec = false);
private slots:
    void onFileChanged(const QString &);
    void onNodeDestroy(QList<Node*> nodes);
    void onShowFilesChanged();
private:
    void readTree(const QString & parentPath = "", bool rec = true);
    Node *specNode(const QString & path, const QString & type);
    QList<QModelIndex> find(QModelIndex startIndex, Node * node);
    Node * createNode(const QFileInfo & info, const QString & spec = "");
    Node * createNode(const QString & info, const QString & spec = "");
private:
    QFileSystemWatcher *_watcher;
    QString _rootPath;
    Node * _root;
    IProject *_project;
    IconProvider::IIconProvider *_provider;
    QMutex _mutex;
};

}
#endif
