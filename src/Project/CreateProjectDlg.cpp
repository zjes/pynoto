#include <QDir>
#include "CreateProjectDlg.h"
#include "ui_CreateProjectDlg.h"

namespace Project {

CreateProjectDlg::CreateProjectDlg(QWidget *parent):
    QDialog(parent),
    _ui(new Ui::CreateProjectDlg)
{
    _ui->setupUi(this);
    _ui->projectPath->setText(QDir::homePath());
    _ui->projectPath->setDirOnly(true);
    _ui->projectName->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z0-9 \\-_]+"), this));

    _ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    connect(_ui->projectName, SIGNAL(textChanged(QString)), SLOT(onNameChanged(QString)));
}

CreateProjectDlg::~CreateProjectDlg()
{
    delete _ui;
}

void CreateProjectDlg::onNameChanged(const QString& text)
{
    int pos = 0;
    QString txt(text);
    _ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(
        _ui->projectName->validator()->validate(txt, pos) == QValidator::Acceptable
    );
}

QString CreateProjectDlg::projectPath()
{
    return QDir(_ui->projectPath->text()).filePath(_ui->projectName->text()+".pynotoprj");
}

QString CreateProjectDlg::name()
{
    return _ui->projectName->text().trimmed();
}

}
