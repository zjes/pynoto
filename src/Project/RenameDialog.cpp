#include <QDir>
#include <QDebug>
#include <QPushButton>
#include "RenameDialog.h"
#include "ui_RenameDialog.h"
#include "Include/IProject.h"

namespace Project {

RenameDialog::RenameDialog(const QString& path, IProject * project, QWidget *parent) :
    QDialog(parent),
    _ui(new Ui::RenameDialog),
    _path(QFileInfo(path)),
    _project(project)
{
    _ui->setupUi(this);
    _ui->newName->setDefaultExtension(_path.suffix());
    _ui->newName->setPath(_path.absolutePath());
    _ui->newName->setFileName(_path.fileName());
    connect(_ui->newName, SIGNAL(valid(bool)), _ui->buttonBox->button(QDialogButtonBox::Ok), SLOT(setEnabled(bool)));
    _ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    connect(_ui->buttonBox, SIGNAL(accepted()), SLOT(onAccept()));
    connect(_ui->buttonBox, SIGNAL(rejected()), SLOT(reject()));
}

RenameDialog::~RenameDialog()
{
    delete _ui;
}

void RenameDialog::onAccept()
{
    QString newPath = _path.absoluteDir().filePath(_ui->newName->fileName());
    _project->closeFile(_path.absoluteFilePath());
    QFile::rename(_path.absoluteFilePath(), newPath);
    _project->openFile(newPath);
}

}
