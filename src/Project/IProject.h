#ifndef _IPROJECT_H_
#define _IPROJECT_H_
#include <QObject>

namespace PythonCode { class IPythonCode; }

namespace Project {

class IProject: public QObject
{
    Q_OBJECT
public:
    virtual ~IProject(){}

    virtual bool load(const QString& fileName) = 0;
    virtual void unload() = 0;

    virtual QString path() = 0;
    virtual QString name() = 0;
    virtual QString fileName() = 0;
    virtual QStringList shownFiles() = 0;
    virtual QStringList openedDirs() = 0;
    virtual void setOpenedDirs(const QStringList&) = 0;
    virtual void openDir(const QString&) = 0;
    virtual void closeDir(const QString&) = 0;
    virtual QStringList openedFiles() = 0;
    virtual QString mainScript() = 0;
    virtual QString fileDisplayName(const QString& fileName) = 0;
    virtual PythonCode::IPythonCode* pythonCode() = 0;
    virtual QString interpretName() = 0;
    virtual QString interpretExec() = 0;
    virtual QStringList interpretPathes() = 0;
    virtual QString guiName() = 0;
    virtual QString pythonUic() = 0;
    virtual int pythonMajorVersion() = 0;
    virtual int rightMarginWidth() = 0;
signals:
    void interpretChanged();
    void shownFilesChanged();
    void lintChanged();
    void systemError(const QString& err);
public slots:
    virtual void openFile(const QString& fileName, int offset = 0) = 0;
    virtual void closeFile(const QString& fileName) = 0;
signals:
    void loaded();
    void unloaded();
    void openFileOffset(const QString& fileName, int offset);
    void fileNeedToClose(const QString& fileName);
};

}
#endif
