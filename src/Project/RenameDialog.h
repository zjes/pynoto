#ifndef _RENAMEDIALOG_H_
#define _RENAMEDIALOG_H_

#include <QFileInfo>
#include <QDialog>

namespace Ui {
class RenameDialog;
}

namespace Project {

class IProject;
class RenameDialog : public QDialog
{
    Q_OBJECT
public:
    explicit RenameDialog(const QString& path, IProject * project, QWidget *parent = 0);
    virtual ~RenameDialog();
private slots:
    void onAccept();
private:
    Ui::RenameDialog *_ui;
    QFileInfo _path;
    IProject * _project;
};

}

#endif
