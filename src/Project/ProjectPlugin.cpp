#include <QDebug>
#include <QtPlugin>
#include <QFileDialog>
#include <QMessageBox>
#include "ProjectPlugin.h"
#include "ProjectManagerWidget.h"
#include "Properties/ProjectProperties.h"
#include "CreateProjectDlg.h"
#include "ProjectRunner.h"
#include "FindInProject.h"

namespace Project {

ProjectPlugin::ProjectPlugin():
    _projectManager(NULL)
{

}

ProjectPlugin::~ProjectPlugin()
{
    delete _project;
    delete _runner;
}

bool ProjectPlugin::initialize()
{
    _project = new ProjectFile();
    _runner = new ProjectRunner(_project);
    connect(_runner, SIGNAL(stoped()), SIGNAL(stoped()));
    connect(_runner, SIGNAL(output(QString)), SIGNAL(output(QString)));
    connect(_runner, SIGNAL(error(QString)), SIGNAL(error(QString)));
    initialized = true;
    return false;
}

QWidget * ProjectPlugin::widget(QWidget * parent)
{
    if (!_projectManager){
        _projectManager = new ProjectManagerWidget(parent, _project);
        connect(_projectManager, SIGNAL(fileDeleted(QString)), this, SIGNAL(fileDeleted(QString)));
    }
    return _projectManager;
}

IProject * ProjectPlugin::project()
{
    return _project;
}

void ProjectPlugin::editProperties(QWidget * parent)
{
    ProjectProperties pprop(_project, parent);
    pprop.exec();
}

void ProjectPlugin::createNewProject(QWidget *parent)
{
    CreateProjectDlg dlg(parent);
    if (dlg.exec() == QDialog::Accepted){
        QString path = dlg.projectPath();
        if (QFile::exists(path)){
            if (QMessageBox::question(
                        parent,
                        tr("File exists"),
                        tr("Project file %1 already exists. Owerride?").arg(path),
                        QMessageBox::Yes | QMessageBox::No
                        ) == QMessageBox::No)
                return;
        }
        QFile file(path);
        if (file.open(QIODevice::WriteOnly | QIODevice::Truncate)){
            QTextStream st(&file);
            st << "[common]" << endl;
            st << "name=" << dlg.name() << endl;
            file.close();
        }
        _project->unload();
        _project->load(path);
        ProjectProperties pprop(_project, parent);
        pprop.exec();
    }
}

void ProjectPlugin::run()
{
    if (_runner->run())
        emit runned();
}

void ProjectPlugin::stop()
{
    if (_runner->stop())
        emit stoped();
}

void ProjectPlugin::fileActivated(const QString& file)
{
    if (_projectManager)
        _projectManager->activateFile(file);
}

void ProjectPlugin::fileModificationChanged(const QString& file, bool modified)
{
    if (_projectManager)
        _projectManager->modifyFile(file, modified);
}

void ProjectPlugin::createFile()
{
    if (_projectManager)
        _projectManager->createFile();
}

void ProjectPlugin::findInProject(QWidget *parent, Main::ISearchResultConsole * console)
{
    FindInProject find(_project, console, parent);
    find.exec();
}

}
