#ifndef NODE_H
#define NODE_H
#include <QList>
#include <QFileInfo>
#include <QStringList>
#include <QMetaType>

namespace Project {

enum NodeTypes{
    SimpleType,
    FileType,
    SpecType,
    DirectoryType,
    PyModuleType,
    DesignerType,
    ResourcesType
};

class Node: public QObject
{
    Q_OBJECT
friend class ProjectTreeModel;
public:
    Node();
    Node(const Node &node);
    Node(const QFileInfo & info, const QString & spec = "", NodeTypes type = SimpleType);
    Node(const QString & path, const QString & spec = "", NodeTypes type = SimpleType);
    virtual ~Node();

    void init();
    int calcPosition(Node *node);
    void addNode(Node* node);
    int row();
    QString path();
    QString parentPath();
    QString name();
    QString dirName();
    QString baseName();
    NodeTypes nodeType();
    QString extension();
    QFileInfo fileInfo();
    bool isModified();
    void setModified(bool modified);
    QString spec();

    void reparent(Node * newParent);
    void checkExistingChildren(const QStringList & extFilter);
    void changeType(NodeTypes newType);
signals:
    void nodeToDestroy(QList<Node*>);
private:
    QFileInfo    _info;
    Node*        _parent;
    QString      _spec;
    bool         _modified;
    QString      _path;
    NodeTypes    _type;
    QList<Node*> _children;
};


void removeNode(const QString & path);
Node * findNode(const QString & path);

}

Q_DECLARE_METATYPE(Project::Node*)

#endif // NODE_H
