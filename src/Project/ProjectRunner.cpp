#include <QMessageBox>
#include "ProjectRunner.h"
#include "ProjectFile.h"


namespace Project {

ProjectRunner::ProjectRunner(ProjectFile * project, QObject *parent) :
    QObject(parent),
    _project(project),
    _process(NULL)
{
}

bool ProjectRunner::run()
{
    if (_process){
        QMessageBox::warning(NULL, tr("Project run"), tr("Project already runned."));
        return false;
    }

    if (_project->interpretExec().isEmpty()){
        QMessageBox::warning(NULL, tr("Project run"), tr("Python iterpret doesn't configured for this project."));
        return false;
    }

    if (_project->mainScript().isEmpty()){
        QMessageBox::warning(NULL, tr("Project run"), tr("Python main script doesn't configured for this project."));
        return false;
    }

    _process = new QProcess(this);

    QStringList pathes = _project->extraPathes() + _project->interpretPathes();

    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("LANG", "en_US.UTF-8");
    env.insert("PYTHONPATH", pathes.join(":"));
    _process->setProcessEnvironment(env);

    connect(_process, SIGNAL(readyRead()), SLOT(onReadyRead()));
    connect(_process, SIGNAL(readyReadStandardError()), SLOT(onReadyReadError()));
    connect(_process, SIGNAL(finished(int)), SLOT(onRunFinished(int)));
    connect(_process, SIGNAL(error(QProcess::ProcessError)), SLOT(onRunError(QProcess::ProcessError)));

    _process->setWorkingDirectory(_project->path());
    _process->start(_project->interpretExec(), QStringList() << "-u" << _project->mainScript());

    return true;
}

bool ProjectRunner::stop()
{
    _process->terminate();
    return true;
}

void ProjectRunner::onRunError(QProcess::ProcessError err)
{
    switch(err){
    case QProcess::FailedToStart:
        emit error(tr("The process failed to start. Either the invoked program is missing, or you may have insufficient permissions to invoke the program."));
        break;
    case QProcess::Crashed:
        emit error(tr("The process crashed some time after starting successfully."));
        break;
    case QProcess::Timedout:
        emit error(tr("The last waitFor...() function timed out."));
        break;
    case QProcess::WriteError:
        emit error(tr("An error occurred when attempting to write to the process. For example, the process may not be running, or it may have closed its input channel."));
        break;
    case QProcess::ReadError:
        emit error(tr("An error occurred when attempting to read from the process. For example, the process may not be running."));
        break;
    default:
        emit error(tr("An error occurred when attempting to read from the process. For example, the process may not be running."));
        break;
    }
}

void ProjectRunner::onRunFinished(int status)
{
    emit output(tr("Project finished with status %1").arg(status));
    _process->deleteLater();
    _process = NULL;
    emit stoped();
}

void ProjectRunner::onReadyReadError()
{
    _process->setReadChannel(QProcess::StandardError);
    QByteArray text;
    while(!(text = _process->readLine()).isEmpty())
        emit error(text);
}

void ProjectRunner::onReadyRead()
{
    _process->setReadChannel(QProcess::StandardOutput);
    QByteArray text;
    while(!(text = _process->readLine()).isEmpty())
        emit output(text);
}

}
