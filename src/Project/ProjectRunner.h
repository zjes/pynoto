#ifndef _PROJECTRUNNER_H_
#define _PROJECTRUNNER_H_

#include <QObject>
#include <QProcess>

namespace Project {

class ProjectFile;

class ProjectRunner : public QObject
{
    Q_OBJECT
public:
    explicit ProjectRunner(ProjectFile * project, QObject *parent = 0);
signals:
    void stoped();
    void error(const QString& msg);
    void output(const QString& msg);
public slots:
    bool run();
    bool stop();
private slots:
    void onRunError(QProcess::ProcessError err);
    void onRunFinished(int status);
    void onReadyReadError();
    void onReadyRead();
private:
    ProjectFile * _project;
    QProcess * _process;

};

}

#endif
