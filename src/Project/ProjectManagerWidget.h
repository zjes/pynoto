#ifndef _PROJECTMANAGERWIDGET_H_
#define _PROJECTMANAGERWIDGET_H_
#include <QTreeView>

class QMenu;
namespace Project {

class IProject;
class ProjectManagerWidget: public QTreeView
{
    Q_OBJECT
public:
    ProjectManagerWidget(QWidget * parent, IProject* project);
    virtual ~ProjectManagerWidget();

    void activateFile(const QString& file);
    void modifyFile(const QString& file, bool modified);
    void createFile();
signals:
    void fileDeleted(const QString& file);
private slots:
    void onProjectLoaded();
    void onProjectUnloaded();

    void onItemActivated(const QModelIndex&);
    void onItemCollapsed(const QModelIndex&);
    void onItemExpanded(const QModelIndex&);

    void onMenuShow(const QPoint&);
    virtual QSize minimumSizeHint() const;
private slots:
    void openFile();
    void addNew();
    void deleteItem();
    void renameItem();
    void compileFile();
private:
    void restoreSession();
    bool removeDir(const QString & dirName);
private:
    IProject * _project;
    QMenu * _menu;
    QMap<QString, QAction*> _actions;
    QPoint _menuPoint;
};

}
#endif
