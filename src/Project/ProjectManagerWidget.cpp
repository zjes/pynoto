#include <QDebug>
#include <QFileInfo>
#include <QHeaderView>
#include <QMenu>
#include <QMessageBox>
#include <QFileDialog>
#include "ProjectManagerWidget.h"
#include "ProjectTreeModel.h"
#include "Include/IProject.h"
#include "Node.h"
#include "NewItems/AddNewWizard.h"
#include "RenameDialog.h"
#include "Include/IDesignerPlugin.h"
#include "Include/Aux.h"

namespace Project {

ProjectManagerWidget::ProjectManagerWidget(QWidget * parent, IProject* project):
    QTreeView(parent),
    _project(project),
    _menu(NULL)
{
    header()->hide();
    setFrameStyle(QFrame::NoFrame);
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(_project, SIGNAL(loaded()), SLOT(onProjectLoaded()));
    connect(_project, SIGNAL(unloaded()), SLOT(onProjectUnloaded()));

    connect(this, SIGNAL(activated(QModelIndex)), SLOT(onItemActivated(QModelIndex)));
    connect(this, SIGNAL(collapsed(QModelIndex)), SLOT(onItemCollapsed(QModelIndex)));
    connect(this, SIGNAL(expanded(QModelIndex)), SLOT(onItemExpanded(QModelIndex)));
    connect(this, SIGNAL(customContextMenuRequested(QPoint)), SLOT(onMenuShow(QPoint)));
}

ProjectManagerWidget::~ProjectManagerWidget()
{

}

void ProjectManagerWidget::onProjectLoaded()
{
    ProjectTreeModel * model = new ProjectTreeModel(_project, this);
    setModel(model);
    restoreSession();
}

void ProjectManagerWidget::onProjectUnloaded()
{
    QAbstractItemModel *mod = model();
    setModel(NULL);
    delete mod;
}

void ProjectManagerWidget::restoreSession()
{
    blockSignals(true);
    foreach(QString dir, _project->openedDirs())
        foreach(QModelIndex index, qobject_cast<ProjectTreeModel *>(model())->indexForPath(dir))
            expand(index);

    blockSignals(false);
    foreach(QString file, _project->openedFiles()){
        _project->openFile(file);
    }
}

void ProjectManagerWidget::onItemActivated(const QModelIndex & index)
{
    QString path = index.data(Qt::EditRole).toString();
    if (QFileInfo(path).isFile()){
        _project->openFile(path);
    }
}

void ProjectManagerWidget::onItemCollapsed(const QModelIndex& index)
{
    QString path = index.data(Qt::EditRole).toString();
    _project->closeDir(path);
}

void ProjectManagerWidget::onItemExpanded(const QModelIndex& index)
{
    QString path = index.data(Qt::EditRole).toString();
    _project->openDir(path);
}

void ProjectManagerWidget::onMenuShow(const QPoint& pt)
{
    _menuPoint = pt;
    if (!_menu){
        _menu = new QMenu(this);
        _actions["new"] = _menu->addAction(tr("Add new..."), this, SLOT(addNew()));
        _actions["delete"] = _menu->addAction(tr("Delete"), this, SLOT(deleteItem()));
        _actions["rename"] = _menu->addAction(tr("Rename..."), this, SLOT(renameItem()));
        _menu->addSeparator();
        _actions["open"] = _menu->addAction(tr("Open"), this, SLOT(openFile()));
        _actions["compile"] = _menu->addAction(tr("Compile"), this, SLOT(compileFile()));
    }
    QModelIndex index = indexAt(pt);
    if (index.isValid()){
        NodeTypes type = (NodeTypes)index.data(Qt::UserRole+1).toInt();
        QFileInfo info(index.data(Qt::EditRole).toString());
        _actions["new"]->setEnabled(type == DirectoryType || type == PyModuleType);
        _actions["delete"]->setEnabled(type != DesignerType);
        _actions["rename"]->setEnabled(type != DesignerType);
        _actions["open"]->setEnabled(type != DirectoryType && type != PyModuleType);
        _actions["compile"]->setEnabled(type == DesignerType || type == ResourcesType || info.suffix() == "ui");
    } else {
        _actions["new"]->setEnabled(true);
        _actions["delete"]->setEnabled(false);
        _actions["rename"]->setEnabled(false);
        _actions["open"]->setEnabled(false);
        _actions["compile"]->setEnabled(false);
    }
    _menu->popup(mapToGlobal(pt));
}

QSize ProjectManagerWidget::minimumSizeHint() const
{
    return QSize(250, 200);
}

void ProjectManagerWidget::openFile()
{
    QString path = currentIndex().data(Qt::EditRole).toString();
    if (QFileInfo(path).isFile()){
        _project->openFile(path);
    }
}

void ProjectManagerWidget::addNew()
{
    QModelIndex ind = indexAt(_menuPoint);
    QString path = _project->path();
    if (ind.isValid())
        path = ind.data(Qt::EditRole).toString();
    NodeTypes type = (NodeTypes)ind.data(Qt::UserRole+1).toInt();
    AddNewWizard wiz(path, type, _project, this);
    wiz.exec();
}

void ProjectManagerWidget::deleteItem()
{
    QModelIndex ind = indexAt(_menuPoint);
    if (!ind.isValid())
        return;

    QFileInfo path(ind.data(Qt::EditRole).toString());
    if (path.isDir()){
        if (QMessageBox::question(NULL,
                tr("Delete directory"),
                QString(tr("Do you really want delete this directory and content?\n\n%1")).arg(path.absoluteFilePath()),
                QMessageBox::Yes | QMessageBox::No
            ) == QMessageBox::No)
            return;
        removeDir(path.absoluteFilePath());
    } else {
        if (QMessageBox::question(NULL,
                tr("Delete file"),
                QString(tr("Do you really want delete file?\n\n%1")).arg(path.absoluteFilePath()),
                QMessageBox::Yes | QMessageBox::No
            ) == QMessageBox::No)
            return;
        _project->closeFile(path.absoluteFilePath());
        emit fileDeleted(path.absoluteFilePath());
        QFile::remove(path.absoluteFilePath());
    }
}

bool ProjectManagerWidget::removeDir(const QString & dirName)
{
    bool result = false;
    QDir dir(dirName);

    if (dir.exists(dirName)) {
        foreach(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)){
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            } else {
                _project->closeFile(info.absoluteFilePath());
                emit fileDeleted(info.absoluteFilePath());
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result) {
                return result;
            }
        }
        _project->closeDir(dirName);
        result = dir.rmdir(dirName);
    }
    return result;
}

void ProjectManagerWidget::activateFile(const QString& file)
{
    if (!model())
        return;
    QList<QModelIndex> ind = qobject_cast<ProjectTreeModel*>(model())->indexForPath(file);
    if (ind.length() > 0 && ind[0].isValid())
        setCurrentIndex(ind[0]);
}

void ProjectManagerWidget::modifyFile(const QString& file, bool modified)
{
    qobject_cast<ProjectTreeModel*>(model())->setModify(file, modified);
}

void ProjectManagerWidget::renameItem()
{
    QModelIndex ind = indexAt(_menuPoint);
    if (!ind.isValid())
        return;

    RenameDialog rem(ind.data(Qt::EditRole).toString(), _project, this);
    rem.exec();
}

void ProjectManagerWidget::createFile()
{
    QModelIndex ind = currentIndex();
    QString path = _project->path();
    NodeTypes type = SimpleType;
    if (path.isEmpty()){
        path = QFileDialog::getExistingDirectory(this, tr("Select target directory"));
    }
    if (ind.isValid()){
        path = ind.data(Qt::EditRole).toString();
        QFileInfo info(path);
        if (info.isFile()){
            path = info.absolutePath();
        }
        type = (NodeTypes)ind.data(Qt::UserRole+1).toInt();
    }
    AddNewWizard wiz(path, type, _project, this);
    wiz.exec();
}

void ProjectManagerWidget::compileFile()
{
    QModelIndex ind = indexAt(_menuPoint);
    if (!ind.isValid())
        return;

    QStringList files;
    QFileInfo path(ind.data(Qt::EditRole).toString());
    if (path.fileName() == "Special:designer"){
        foreach(QFileInfo name, path.absoluteDir().entryInfoList(QStringList() << "*.ui")){
            files << name.absoluteFilePath();
        }
    } else {
        files << path.absoluteFilePath();
    }

    Designer::IDesignerPlugin* plugin = Aux::plugin<Designer::IDesignerPlugin*>("designer");
    if (!plugin)
        qDebug() << "not designer";
    foreach(QString name, files){
        plugin->renderPyFile(_project, name);
    }

    qDebug() << files << path.fileName();
}

}
