#ifndef _PROJECTPLUGIN_H_
#define _PROJECTPLUGIN_H_
#include "IProjectPlugin.h"
#include "ProjectFile.h"

namespace Project {
class ProjectRunner;
class ProjectManagerWidget;

class ProjectPlugin: public IProjectPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IProjectPlugin")
    Q_INTERFACES(Project::IProjectPlugin)
public:
    ProjectPlugin();
    virtual ~ProjectPlugin();
    virtual bool initialize();

    virtual QWidget  * widget(QWidget * parent);
    virtual IProject * project();
public slots:
    virtual void editProperties(QWidget * parent);
    virtual void createNewProject(QWidget *parent);
    virtual void run();
    virtual void stop();
    virtual void fileActivated(const QString&);
    virtual void fileModificationChanged(const QString&, bool);
    virtual void createFile();
    virtual void findInProject(QWidget *parent, Main::ISearchResultConsole * console);
private:
    ProjectFile * _project;
    ProjectRunner * _runner;
    ProjectManagerWidget * _projectManager;
};

}

#endif
