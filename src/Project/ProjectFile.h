#ifndef _PROJECT_H_
#define _PROJECT_H_
#include <QMap>
#include <QColor>
#include "IProject.h"

class QSettings;
namespace Python { class IPythonPlugin; }
namespace Project {

struct SearchHistory
{
    QString search;
    int mask;

    bool operator==(const SearchHistory& other)
    {
        return search == other.search && mask == other.mask;
    }
};

class ProjectFile: public IProject
{
    Q_OBJECT
    Q_PROPERTY(QString path READ path WRITE setPath)
    Q_PROPERTY(QString name READ name WRITE setName)
public:
    ProjectFile();
    virtual ~ProjectFile();

    virtual bool load(const QString& fileName);
    virtual void unload();

    virtual QString path();
    void setPath(const QString& p);

    virtual QString name();
    void setName(const QString& name);

    virtual QString fileName();

    virtual QStringList shownFiles();
    void setShownFiles(const QStringList&);

    virtual QStringList openedDirs();
    virtual void setOpenedDirs(const QStringList& dirs);
    virtual void openDir(const QString&);
    virtual void closeDir(const QString&);

    virtual QStringList openedFiles();
    virtual QString mainScript();
    void setMainScript(const QString& mainScript);
    virtual QString fileDisplayName(const QString& fileName);
    virtual PythonCode::IPythonCode* pythonCode();
    virtual QString interpretName();
    virtual QString interpretExec();
    virtual QStringList interpretPathes();
    void setInterpretName(const QString& name);
    virtual QString guiName();
    void setGuiName(const QString& name);

    virtual QString pythonUic();
    virtual int pythonMajorVersion();

    QMap<QString, QVariant> codeCheck();
    void setCodeCheck(const QMap<QString, QVariant>& check);

    QColor color();
    void setColor(const QColor& color);

    QStringList extraPathes();
    void setExtraPathes(const QStringList& pathes);

    virtual int rightMarginWidth();
    QList<SearchHistory> searchHistory();
    void addSearchHistory(const QString& search, int mask);
public slots:
    virtual void openFile(const QString& fileName, int offset = 0);
    virtual void closeFile(const QString& fileName);
private:
    bool _loaded;
    QSettings * _settings;
    PythonCode::IPythonCode* _pythonCode;
    Python::IPythonPlugin * _python;
};

}
#endif
