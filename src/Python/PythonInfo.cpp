#include <QFileInfo>
#include <QDebug>
#include <QProcess>
#include <QDir>
#include "PythonInfo.h"

namespace Python {

typedef void (*PyInitFunc)();

PythonInfo::PythonInfo(const QString& pyExec)
{
    _pyPath = pyExec;
    QFileInfo info(pyExec);
    if (info.isSymLink())
        _pyPath = info.symLinkTarget();

    /*QString libPath = sharedLibraryPath();
    //python3 -c "import sysconfig; print(sysconfig.get_config_var('INSTSONAME'))"


    _lib.setFileName(libPath);
    _lib.load();
    qDebug() << _lib.errorString();
    PyInitFunc init = (PyInitFunc)_lib.resolve("Py_Initialize");
    if (init)
        init();*/
}

PythonInfo::~PythonInfo()
{
    /*PyInitFunc deinit = (PyInitFunc)_lib.resolve("Py_Finalize");
    if (deinit)
        deinit();
    _lib.unload();*/
}

QString PythonInfo::pyVersion()
{
    QProcess proc;
    QStringList args = QStringList() << _pyPath << "-u" << "-V";
    proc.setProcessChannelMode(QProcess::MergedChannels);
    proc.start(args.join(" "));
    if (!proc.waitForFinished()){
        qDebug() << "error:" << proc.errorString();
        return "";
    }
    return proc.readAll().trimmed();
}

QString PythonInfo::sharedLibraryPath()
{
    QProcess proc;
    QStringList args = QStringList() << _pyPath << "-u" << "-c" << "\"import sysconfig, os; print(sysconfig.get_config_var('LIBDIR')+os.sep+sysconfig.get_config_var('LIBRARY'))\"";
    proc.setProcessChannelMode(QProcess::MergedChannels);
    proc.start(args.join(" "));
    if (!proc.waitForFinished()){
        qDebug() << "error:" << proc.errorString();
        return "";
    }
    QFileInfo lib(proc.readAll().trimmed());
    QFileInfo slib(lib.absolutePath()+QDir::separator()+lib.completeBaseName()+".so");
    return slib.absoluteFilePath();
}

QStringList PythonInfo::standartPathes()
{
    QProcess proc;
    QStringList args = QStringList() << _pyPath << "-u" << "-c" << "\"import sys; print(sys.path)\"";
    proc.setProcessChannelMode(QProcess::MergedChannels);
    proc.start(args.join(" "));
    if (!proc.waitForFinished()){
        qDebug() << "error:" << proc.errorString();
        return QStringList();
    }

    QString rawPathes = proc.readAll().trimmed();
    rawPathes = rawPathes.mid(2, rawPathes.length()-4);
    QStringList ret;
    foreach(QString path, rawPathes.split("', '")){
        if (!path.trimmed().isEmpty())
            ret.append(path);
    }
    return ret;
}

QStringList PythonInfo::modules()
{
    QProcess proc;
    QStringList args = QStringList() << _pyPath << "-u" << "-c" << "\"import pkgutil; print([name for (load, name, ispkg) in pkgutil.iter_modules() if ispkg])\"";
    proc.setProcessChannelMode(QProcess::MergedChannels);
    proc.start(args.join(" "));
    if (!proc.waitForFinished()){
        qDebug() << "error:" << proc.errorString();
        return QStringList();
    }

    QString rawPathes = proc.readAll().trimmed();
    rawPathes = rawPathes.mid(2, rawPathes.length()-4);
    QStringList ret;
    foreach(QString path, rawPathes.split("', '")){
        if (!path.trimmed().isEmpty())
            ret.append(path);
    }
    return ret;
}

//

}
