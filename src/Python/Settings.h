#ifndef _PYSETTINGS_H_
#define _PYSETTINGS_H_
#include <QSettings>
#include "IPythonPlugin.h"

namespace Python {

class Settings: public QSettings
{
    Q_OBJECT
    Q_PROPERTY(QList<IPythonPlugin::Version> versions READ versions WRITE setVersions)
public:
    QList<IPythonPlugin::Version> versions();
    void setVersions(const QList<IPythonPlugin::Version>& vers);
};

}
#endif
