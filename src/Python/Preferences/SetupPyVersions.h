#ifndef _SETUPPYVERSIONS_H_
#define _SETUPPYVERSIONS_H_
#include <QWidget>
#include <QAbstractItemModel>
#include "Include/IPreferencesPage.h"

namespace Ui {
    class SetupPyVersions;
}

namespace Python {

class SetupPyVersions: public IPreferencesPage
{
    Q_OBJECT
public:
    SetupPyVersions(QWidget* parent = NULL);
    virtual ~SetupPyVersions();

    virtual QString title();
    virtual QIcon icon();
    virtual bool save();
    virtual int order() {return 2;}
private slots:
    void onAddClicked();
    void init();
    void onListRowChanged(const QModelIndex&);
    void onDeleteClicked();
    void onChangeShared();
    void onAddCustomPath();
    void onDeleteCustomPath();
    void onPathActivated(const QModelIndex&);
    void onPyQtChanged();
    void onPySideChanged();

    void changeName(const QString& text);
    void changeShared(const QString& text);

    void findPyQtTools();
    void findPySideTools();
private:
    Ui::SetupPyVersions * _ui;
};

}

#endif
