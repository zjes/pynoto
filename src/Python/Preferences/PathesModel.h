#ifndef _PATHESMODEL_H_
#define _PATHESMODEL_H_
#include <QStringList>
#include <QAbstractListModel>

namespace Python {

class PathesModel: public QAbstractListModel
{
    Q_OBJECT
public:
    PathesModel(const QStringList& stdPathes, QStringList* cPathes, QObject *parent = NULL);
    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    bool isCustom(const QModelIndex&);
public slots:
    QModelIndex addCustomPath(const QString& path);
    QModelIndex deleteCustomPath(const QModelIndex& index);
private:
    QStringList _stdPathes;
    QStringList * _cPathes;
};

}
#endif
