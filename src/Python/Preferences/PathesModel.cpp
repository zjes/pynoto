#include <QDebug>
#include <QColor>
#include "PathesModel.h"

namespace Python {

PathesModel::PathesModel(const QStringList& stdPathes, QStringList * cPathes, QObject *parent):
    QAbstractListModel(parent),
    _stdPathes(stdPathes),
    _cPathes(cPathes)
{

}

int PathesModel::rowCount(const QModelIndex & parent) const
{
    return _stdPathes.length()+_cPathes->length();
}

QVariant PathesModel::data(const QModelIndex & index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole){
        if (index.row() >= _stdPathes.length()){
            return _cPathes->at(index.row()-_stdPathes.length());
        }
        return _stdPathes[index.row()];
    }
    if (role == Qt::ForegroundRole){
        if (index.row() < _stdPathes.length())
            return QColor(Qt::gray);
    }
    return QVariant();
}

QModelIndex PathesModel::addCustomPath(const QString& path)
{
    beginInsertRows(QModelIndex(), _stdPathes.length() + _cPathes->length() - 1, _stdPathes.length() + _cPathes->length() - 1);
    _cPathes->append(path);
    QModelIndex index = createIndex(_stdPathes.length() + _cPathes->length() - 1, 0);
    endInsertRows();
    return index;
}

QModelIndex PathesModel::deleteCustomPath(const QModelIndex& index)
{
    beginRemoveRows(QModelIndex(), index.row(), index.row());
    _cPathes->removeAt(index.row() - _stdPathes.length());
    QModelIndex ret = createIndex(index.row()-1 > 0 ? index.row()-1 : 0, 0);
    endRemoveRows();
    return ret;
}

bool PathesModel::isCustom(const QModelIndex& index)
{
    return index.row() >= _stdPathes.length();
}

}
