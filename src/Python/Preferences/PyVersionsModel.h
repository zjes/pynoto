#ifndef _PYVERSIONSMODEL_H_
#define _PYVERSIONSMODEL_H_
#include <QAbstractTableModel>
#include "IPythonPlugin.h"

namespace Python {

class PathesModel;

class PyVersionsModel: public QAbstractTableModel
{
    Q_OBJECT
public:
    PyVersionsModel(const QList<IPythonPlugin::Version>& data, QObject * parent = NULL);
    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    void changeName(const QString& text, const QModelIndex& index);
    void changeShared(const QString& text, const QModelIndex& index);
    QModelIndex addVersion(const QString& exec, const QString& name, const QString& sharedPath);
    QModelIndex deleteItem(const QModelIndex& index);

    QList<IPythonPlugin::Version> versions();
    PathesModel* pathes(const QModelIndex&);

    void updatePyQt(const QModelIndex& index, const QString& uic, const QString& rcc, const QString& lupdate);
    void updatePySide(const QModelIndex& index, const QString& uic, const QString& rcc, const QString& lupdate);
private:
    QString solveUniqueName(const QString& name);
    QList<IPythonPlugin::Version> _data;
    QList<PathesModel *> _pathes;
};

}

#endif
