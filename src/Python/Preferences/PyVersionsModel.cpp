#include <QDebug>
#include "PyVersionsModel.h"
#include "PathesModel.h"
#include "PythonInfo.h"

namespace Python {

PyVersionsModel::PyVersionsModel(const QList<IPythonPlugin::Version>& data, QObject * parent):
    QAbstractTableModel(parent),
    _data(data)
{
    for(int i = 0; i < _data.length(); ++i){
        PythonInfo info(_data[i].exec);
        _pathes.append(new PathesModel(info.standartPathes(), &_data[i].pathes, this));
    }
}

int PyVersionsModel::rowCount(const QModelIndex & parent) const
{
    return _data.length();
}

int PyVersionsModel::columnCount(const QModelIndex & parent) const
{
    return 2;
}

QVariant PyVersionsModel::data(const QModelIndex & index, int role) const
{
    if (!index.isValid() || (index.row() < 0 || index.row() >= _data.length()))
        return QVariant();

    IPythonPlugin::Version it = _data[index.row()];
    if (role == Qt::DisplayRole){
        switch(index.column()){
        case 0: return it.name;
        case 1: return it.exec;
        }
    }
    switch(role){
    case Qt::DisplayRole:
        switch(index.column()){
        case 0: return it.name;
        case 1: return it.exec;
        }
        break;
    case Qt::UserRole+1:
        return it.name;
    case Qt::UserRole+2:
        return it.exec;
    case Qt::UserRole+3:
        return it.shared;
    case Qt::UserRole+4:
        return it.pyQt4.uic;
    case Qt::UserRole+5:
        return it.pyQt4.rcc;
    case Qt::UserRole+6:
        return it.pyQt4.lupdate;
    case Qt::UserRole+7:
        return it.pySide.uic;
    case Qt::UserRole+8:
        return it.pySide.rcc;
    case Qt::UserRole+9:
        return it.pySide.lupdate;
    }

    return QVariant();
}

QVariant PyVersionsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal)
        return QVariant();
    if (role != Qt::DisplayRole)
        return QVariant();

    switch(section){
    case 0: return tr("Version name");
    case 1: return tr("Path");
    }
    return QVariant();
}

void PyVersionsModel::changeName(const QString& text, const QModelIndex& index)
{
    _data[index.row()].name = text;
    emit dataChanged(createIndex(index.row(), 0), createIndex(index.row(), 1));
}

void PyVersionsModel::changeShared(const QString& text, const QModelIndex& index)
{
    _data[index.row()].shared = text;
    emit dataChanged(createIndex(index.row(), 0), createIndex(index.row(), 1));
}

QModelIndex PyVersionsModel::addVersion(const QString& exec, const QString& name, const QString& sharedPath)
{
    beginInsertRows(QModelIndex(), _data.length()-1, _data.length()-1);
    IPythonPlugin::Version ver(solveUniqueName(name), exec, sharedPath);
    _data.append(ver);
    PythonInfo info(exec);
    _pathes.append(new PathesModel(info.standartPathes(), &_data[_data.length()-1].pathes));
    QModelIndex index = createIndex(_data.length()-1, 0);
    endInsertRows();
    return index;
}

QString PyVersionsModel::solveUniqueName(const QString& name)
{
    bool found = false;
    foreach(IPythonPlugin::Version ver, _data){
        if (ver.name == name){
            found = true;
            break;
        }
    }
    if (!found)
        return name;

    QRegExp exp("(.*)?\\s\\((\\d+)\\)");
    if (!exp.exactMatch(name)){
        return solveUniqueName(name+" (1)");
    }
    int pos = exp.indexIn(name);
    if (pos >= 0){
        QString num = exp.capturedTexts()[2];
        QString newName = name;
        newName = newName.replace("("+num+")", "("+QString::number(num.toInt()+1)+")");
        return solveUniqueName(newName);
    }
    return name;
}

QModelIndex PyVersionsModel::deleteItem(const QModelIndex& index)
{
    beginRemoveRows(QModelIndex(), index.row(), index.row());
    _data.removeAt(index.row());
    _pathes.removeAt(index.row());
    QModelIndex ret = createIndex(index.row()-1 > 0 ? index.row()-1 : 0, 0);
    endRemoveRows();
    return ret;
}

QList<IPythonPlugin::Version> PyVersionsModel::versions()
{
    return _data;
}

PathesModel* PyVersionsModel::pathes(const QModelIndex& index)
{
    return _pathes[index.row()];
}

void PyVersionsModel::updatePyQt(const QModelIndex& index, const QString& uic, const QString& rcc, const QString& lupdate)
{
    _data[index.row()].pyQt4.uic = uic;
    _data[index.row()].pyQt4.rcc = rcc;
    _data[index.row()].pyQt4.lupdate = lupdate;
}

void PyVersionsModel::updatePySide(const QModelIndex& index, const QString& uic, const QString& rcc, const QString& lupdate)
{
    _data[index.row()].pySide.uic = uic;
    _data[index.row()].pySide.rcc = rcc;
    _data[index.row()].pySide.lupdate = lupdate;
}

}
