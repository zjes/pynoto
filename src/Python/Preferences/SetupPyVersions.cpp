#include <QDebug>
#include <QFileDialog>
#include <QHeaderView>
#include <QStringListModel>
#include "SetupPyVersions.h"
#include "PythonInfo.h"
#include "Settings.h"
#include "PythonInfo.h"
#include "PyVersionsModel.h"
#include "PathesModel.h"
#include "Core/Environment.h"
#include "ui_SetupPyVersions.h"

namespace Python {

SetupPyVersions::SetupPyVersions(QWidget* parent):
    IPreferencesPage(parent),
    _ui(new Ui::SetupPyVersions)
{
    _ui->setupUi(this);
    connect(_ui->lstPys, SIGNAL(activated(QModelIndex)), SLOT(onListRowChanged(QModelIndex)));
    connect(_ui->btnAdd, SIGNAL(clicked()), SLOT(onAddClicked()));
    connect(_ui->btnDelete, SIGNAL(clicked()), SLOT(onDeleteClicked()));
    connect(_ui->btnChangeShared, SIGNAL(clicked()), SLOT(onChangeShared()));
    connect(_ui->btnAddCustomPath, SIGNAL(clicked()), SLOT(onAddCustomPath()));
    connect(_ui->btnDeleteCustomPath, SIGNAL(clicked()), SLOT(onDeleteCustomPath()));
    connect(_ui->pathes, SIGNAL(activated(QModelIndex)), SLOT(onPathActivated(QModelIndex)));
    connect(_ui->btnPyQtFind, SIGNAL(clicked()), SLOT(findPyQtTools()));
    connect(_ui->btnPySideFind, SIGNAL(clicked()), SLOT(findPySideTools()));

    connect(_ui->edtPyUicPath, SIGNAL(editingFinished()), SLOT(onPyQtChanged()));
    connect(_ui->edtPyrccPath, SIGNAL(editingFinished()), SLOT(onPyQtChanged()));
    connect(_ui->edtPylupdatePath, SIGNAL(editingFinished()), SLOT(onPyQtChanged()));

    connect(_ui->edtPySideUicPath, SIGNAL(editingFinished()), SLOT(onPySideChanged()));
    connect(_ui->edtPySiderccPath, SIGNAL(editingFinished()), SLOT(onPySideChanged()));
    connect(_ui->edtPySidelupdatePath, SIGNAL(editingFinished()), SLOT(onPySideChanged()));

    _ui->lstPys->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    init();
}

SetupPyVersions::~SetupPyVersions()
{
    delete _ui;
}

QString SetupPyVersions::title()
{
    return tr("Python");
}

QIcon SetupPyVersions::icon()
{
    return QIcon();
}

bool SetupPyVersions::save()
{
    Settings set;
    set.setVersions(qobject_cast<PyVersionsModel*>(_ui->lstPys->model())->versions());
    return true;
}

void SetupPyVersions::onAddClicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Python executable"), "/usr/bin");
    if (!fileName.isEmpty()){
        PythonInfo info(fileName);
        QModelIndex index = qobject_cast<PyVersionsModel*>(_ui->lstPys->model())->addVersion(fileName, info.pyVersion(), info.sharedLibraryPath());
        _ui->lstPys->setCurrentIndex(index);
        onListRowChanged(index);
        _ui->lstPys->scrollTo(index);
        findPyQtTools();
        findPySideTools();
    }
}

void SetupPyVersions::init()
{
    Settings set;
    _ui->lstPys->setModel(new PyVersionsModel(set.versions(), this));
    connect(_ui->edtVersionName, SIGNAL(textChanged(QString)), SLOT(changeName(QString)));
    connect(_ui->edtSharedLib, SIGNAL(textChanged(QString)), SLOT(changeShared(QString)));
}

void SetupPyVersions::onListRowChanged(const QModelIndex& index)
{
    _ui->btnDelete->setEnabled(index.isValid());
    _ui->editGroup->setEnabled(index.isValid());
    _ui->btnAddCustomPath->setEnabled(index.isValid());
    if (index.isValid()){
        _ui->lblPyExec->setText(index.data(Qt::UserRole+2).toString());
        _ui->edtVersionName->setText(index.data(Qt::UserRole+1).toString());
        _ui->edtSharedLib->setText(index.data(Qt::UserRole+3).toString());
        _ui->pathes->setModel(qobject_cast<PyVersionsModel*>(_ui->lstPys->model())->pathes(index));

        PythonInfo info(index.data(Qt::UserRole+2).toString());
        QStringList modules = info.modules();
        _ui->modules->setModel(new QStringListModel(modules, this));
        _ui->tabWidget->setTabEnabled(2, modules.contains("PyQt4"));
        _ui->tabWidget->setTabEnabled(3, modules.contains("PySide"));

        _ui->edtPyUicPath->setText(index.data(Qt::UserRole+4).toString());
        _ui->edtPyrccPath->setText(index.data(Qt::UserRole+5).toString());
        _ui->edtPylupdatePath->setText(index.data(Qt::UserRole+6).toString());

        _ui->edtPySideUicPath->setText(index.data(Qt::UserRole+7).toString());
        _ui->edtPySiderccPath->setText(index.data(Qt::UserRole+8).toString());
        _ui->edtPySidelupdatePath->setText(index.data(Qt::UserRole+9).toString());
    } else {
        _ui->lblPyExec->setText("");
        _ui->edtVersionName->setText("");
        _ui->edtSharedLib->setText("");
        _ui->pathes->setModel(NULL);
        _ui->modules->setModel(NULL);

        _ui->edtPyUicPath->setText("");
        _ui->edtPyrccPath->setText("");
        _ui->edtPylupdatePath->setText("");

        _ui->edtPySideUicPath->setText("");
        _ui->edtPySiderccPath->setText("");
        _ui->edtPySidelupdatePath->setText("");
    }
}

void SetupPyVersions::onDeleteClicked()
{
    QModelIndex index = qobject_cast<PyVersionsModel*>(_ui->lstPys->model())->deleteItem(_ui->lstPys->currentIndex());
    _ui->lstPys->setCurrentIndex(index);
    onListRowChanged(index);
    _ui->lstPys->scrollTo(index);
}

void SetupPyVersions::changeName(const QString& text)
{
    qobject_cast<PyVersionsModel*>(_ui->lstPys->model())->changeName(text, _ui->lstPys->currentIndex());
}

void SetupPyVersions::changeShared(const QString& text)
{
    qobject_cast<PyVersionsModel*>(_ui->lstPys->model())->changeShared(text, _ui->lstPys->currentIndex());
}

void SetupPyVersions::onChangeShared()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Python shared library"), "/usr/lib");
    if (!fileName.isEmpty()){
        _ui->edtSharedLib->setText(fileName);
    }
}

void SetupPyVersions::onAddCustomPath()
{
    QString	path = QFileDialog::getExistingDirectory(this, tr("Select path to add"));
    if (!path.isEmpty()){
        QModelIndex index = qobject_cast<PathesModel*>(_ui->pathes->model())->addCustomPath(path);
        onPathActivated(index);
        _ui->pathes->scrollTo(index);
    }
}

void SetupPyVersions::onDeleteCustomPath()
{
    QModelIndex index = qobject_cast<PathesModel*>(_ui->pathes->model())->deleteCustomPath(_ui->pathes->currentIndex());
    onPathActivated(index);
    _ui->pathes->scrollTo(index);
}

void SetupPyVersions::onPathActivated(const QModelIndex&)
{
    _ui->btnDeleteCustomPath->setEnabled(qobject_cast<PathesModel*>(_ui->pathes->model())->isCustom(_ui->pathes->currentIndex()));
}

void SetupPyVersions::findPyQtTools()
{
    Core::Environment env(Core::Environment::systemEnvironment());
    QString uic = env.searchInPath("pyuic4");
    QString rcc = env.searchInPath("pyrcc4");
    QString lupdate = env.searchInPath("pylupdate4");
    _ui->edtPyUicPath->setText(uic);
    _ui->edtPyrccPath->setText(rcc);
    _ui->edtPylupdatePath->setText(lupdate);
    qobject_cast<PyVersionsModel*>(_ui->lstPys->model())->updatePyQt(_ui->lstPys->currentIndex(), uic, rcc, lupdate);
}

void SetupPyVersions::findPySideTools()
{
    Core::Environment env(Core::Environment::systemEnvironment());
    QString uic = env.searchInPath("pyside-uic");
    QString rcc = env.searchInPath("pyside-rcc");
    QString lupdate = env.searchInPath("pyside-lupdate");
    _ui->edtPySideUicPath->setText(uic);
    _ui->edtPySiderccPath->setText(rcc);
    _ui->edtPySidelupdatePath->setText(lupdate);
    qobject_cast<PyVersionsModel*>(_ui->lstPys->model())->updatePySide(_ui->lstPys->currentIndex(), uic, rcc, lupdate);
}

void SetupPyVersions::onPyQtChanged()
{
    qobject_cast<PyVersionsModel*>(_ui->lstPys->model())->updatePyQt(
        _ui->lstPys->currentIndex(),
        _ui->edtPyUicPath->text(),
        _ui->edtPyrccPath->text(),
        _ui->edtPylupdatePath->text()
    );
}

void SetupPyVersions::onPySideChanged()
{
    qobject_cast<PyVersionsModel*>(_ui->lstPys->model())->updatePySide(
        _ui->lstPys->currentIndex(),
        _ui->edtPySideUicPath->text(),
        _ui->edtPySiderccPath->text(),
        _ui->edtPySidelupdatePath->text()
    );
}

}
