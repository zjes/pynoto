#include <QDebug>
#include "Settings.h"

namespace Python
{


QList<IPythonPlugin::Version> Settings::versions()
{
    QList<IPythonPlugin::Version> ret;
    int size = beginReadArray("python");
    for(int i = 0; i < size; ++i){
        setArrayIndex(i);
        IPythonPlugin::Version ver(
            value("name").toString(),
            value("exec").toString(),
            value("shared").toString(),
            value("pathes").toStringList()
        );
        ver.pyQt4.uic = value("pyqt4/uic").toString();
        ver.pyQt4.rcc = value("pyqt4/rcc").toString();
        ver.pyQt4.lupdate = value("pyqt4/lupdate").toString();

        ver.pySide.uic = value("pyside/uic").toString();
        ver.pySide.rcc = value("pyside/rcc").toString();
        ver.pySide.lupdate = value("pyside/lupdate").toString();

        ret.append(ver);
    }
    endArray();
    return ret;
}

void Settings::setVersions(const QList<IPythonPlugin::Version>& vers)
{
    remove("python");
    beginWriteArray("python");
    for(int i = 0; i < vers.length(); ++i){
        setArrayIndex(i);
        setValue("exec", vers[i].exec);
        setValue("name", vers[i].name);
        setValue("shared", vers[i].shared);
        setValue("pathes", vers[i].pathes);
        setValue("pyqt4/uic", vers[i].pyQt4.uic);
        setValue("pyqt4/rcc", vers[i].pyQt4.rcc);
        setValue("pyqt4/lupdate", vers[i].pyQt4.lupdate);
        setValue("pyside/uic", vers[i].pySide.uic);
        setValue("pyside/rcc", vers[i].pySide.rcc);
        setValue("pyside/lupdate", vers[i].pySide.lupdate);
    }
    endArray();
    sync();
}

}
