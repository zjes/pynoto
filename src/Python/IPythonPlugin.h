#ifndef _IPYTHONPLUGIN_H_
#define _IPYTHONPLUGIN_H_
#include <QStringList>
#include "Include/IPlugin.h"

namespace Python {
class IPython;

class IPythonPlugin: public IPlugin
{
    Q_OBJECT
public:
    struct PyQt
    {
        QString uic;
        QString rcc;
        QString lupdate;
        PyQt():uic(""), rcc(""), lupdate("")
        {}
        PyQt(const QString& puic, const QString& prcc, const QString& plupdate):
            uic(puic), rcc(prcc), lupdate(plupdate)
        {}
    };

    struct Version
    {
        QString name;
        QString shared;
        QString exec;
        QStringList pathes;
        PyQt pyQt4;
        PyQt pySide;
        Version(const QString& pname, const QString& pexec, const QString& pshared, const QStringList& ppathes = QStringList()):
            name(pname),
            shared(pshared),
            exec(pexec),
            pathes(ppathes)
        {}
    };

    virtual ~IPythonPlugin(){}
    virtual void setupVersions(QWidget *parent) = 0;
    virtual QList<Version> listVersions() = 0;
    virtual QStringList guiFrameworks(const QString& pythonName) = 0;
    virtual Version pyVersion(const QString& name) = 0;
};

}

Q_DECLARE_INTERFACE(Python::IPythonPlugin, "Pynoto.IPythonPlugin")

#endif
