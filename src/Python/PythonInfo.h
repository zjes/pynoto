#ifndef _PYTHONINFO_H_
#define _PYTHONINFO_H_
#include <QLibrary>

namespace Python {

class PythonInfo
{
public:
    PythonInfo(const QString& pyExec);
    virtual ~PythonInfo();

    QString pyVersion();
    QString sharedLibraryPath();
    QStringList standartPathes();
    QStringList modules();
private:
    QLibrary _lib;
    QString _pyPath;
};

}

#endif
