#include <QtPlugin>
#include <QVBoxLayout>
#include <QDialog>
#include "PythonPlugin.h"
#include "PythonInfo.h"
#include "Preferences/SetupPyVersions.h"
#include "Settings.h"
#include "Include/PynotoApplication.h"

namespace Python {

PythonPlugin::~PythonPlugin()
{

}

bool PythonPlugin::initialize()
{
    initialized = true;
    return true;
}

void PythonPlugin::setupVersions(QWidget *parent)
{
    QDialog dlg(parent);
    dlg.setWindowTitle(tr("Setup python versions"));
    QVBoxLayout *lay = new QVBoxLayout;
    dlg.setLayout(lay);
    lay->addWidget(new SetupPyVersions);
    dlg.resize(500, 400);
    dlg.exec();
}

QList<IPythonPlugin::Version> PythonPlugin::listVersions()
{
    Settings set;
    return set.versions();
}

QStringList PythonPlugin::guiFrameworks(const QString& pythonName)
{
    QStringList frameworks;
    Settings set;
    QList<IPythonPlugin::Version> vers = set.versions();
    foreach(IPythonPlugin::Version ver, vers){
        if (ver.name == pythonName){
            PythonInfo info(ver.exec);
            QStringList modules = info.modules();
            if (modules.contains("PyQt4"))
                frameworks.append("PyQt4");
            if (modules.contains("PySide"))
                frameworks.append("PySide");
        }
    }
    return frameworks;
}

QList<IPreferencesPage*> PythonPlugin::preferences()
{
    return QList<IPreferencesPage*>() << new SetupPyVersions;
}

IPythonPlugin::Version PythonPlugin::pyVersion(const QString& name)
{
    Settings set;
    foreach(Version ver, set.versions()){
        if (ver.name == name)
            return ver;
    }
    Version ver("", "", "");
    return ver;
}

}
