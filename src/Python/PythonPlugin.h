#ifndef _PYTHONPLUGIN_H_
#define _PYTHONPLUGIN_H_
#include "IPythonPlugin.h"

namespace Python {

class PythonPlugin: public IPythonPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IPythonPlugin")
    Q_INTERFACES(Python::IPythonPlugin)
public:
    virtual ~PythonPlugin();
    virtual bool initialize();
    virtual void setupVersions(QWidget *parent);
    virtual QList<Version> listVersions();
    virtual QList<IPreferencesPage*> preferences();
    virtual QStringList guiFrameworks(const QString& pythonName);
    virtual Version pyVersion(const QString& name);
};

}

#endif
