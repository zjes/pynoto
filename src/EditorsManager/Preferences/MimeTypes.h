#ifndef _MIMETYPES_H_
#define _MIMETYPES_H_
#include <QAbstractTableModel>
#include "Include/IPreferencesPage.h"
#include "Settings.h"

namespace Ui {
    class MimeTypes;
}

namespace EditorsManager {


class MimeTypes: public IPreferencesPage
{
    Q_OBJECT
public:
    MimeTypes(QWidget * parent = NULL);
    virtual ~MimeTypes();

    virtual QString title();
    virtual QIcon icon();
    virtual bool save();
    virtual int order() {return 1;}
private:
    void init();
private slots:
    void onItemActivated(const QModelIndex& index);
    void changePattern(const QString& text);
    void changeEditor(int);
    void addNewItem();
    void deleteItem();
private:
    Ui::MimeTypes * _ui;
    QMap<QString, QString> _editors;
};

class MimeModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    MimeModel(const QList<FileType>& data, const QMap<QString, QString>& editors, QObject * parent = NULL);
    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QList<FileType> types();
public slots:
    void changePattern(const QString& text, const QModelIndex& index);
    void changeEditor(const QString& text, const QModelIndex& index);
    QModelIndex addItem();
    QModelIndex deleteItem(const QModelIndex& index);
private:
    QList<FileType> _data;
    QMap<QString, QString> _editors;
};

}
#endif
