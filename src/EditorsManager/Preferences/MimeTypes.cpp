#include <QSettings>
#include "MimeTypes.h"
#include "ui_MimeTypes.h"
#include "Settings.h"

namespace EditorsManager {

MimeTypes::MimeTypes(QWidget * parent):
    IPreferencesPage(parent),
    _ui(new Ui::MimeTypes)
{
    _ui->setupUi(this);
    _editors["python"] = tr("Python editor");
    _editors["designer"] = tr("Qt form designer");
    _editors["text"] = tr("Text editor");

    foreach(QString key, _editors.keys())
        _ui->cmbEditor->addItem(_editors[key], key);

    connect(_ui->table, SIGNAL(activated(QModelIndex)), SLOT(onItemActivated(QModelIndex)));
    connect(_ui->btnAdd, SIGNAL(clicked()), SLOT(addNewItem()));
    connect(_ui->btnDelete, SIGNAL(clicked()), SLOT(deleteItem()));
    _ui->editBox->setEnabled(false);
    _ui->table->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    init();
}

MimeTypes::~MimeTypes()
{
    delete _ui;
}

QString MimeTypes::title()
{
    return tr("File types");
}

QIcon MimeTypes::icon()
{
    return QIcon();
}

bool MimeTypes::save()
{
    Settings set;
    set.setFileTypes(qobject_cast<MimeModel*>(_ui->table->model())->types());
    return true;
}

void MimeTypes::init()
{
    Settings set;
    _ui->table->setModel(new MimeModel(set.fileTypes(), _editors, this));
    connect(_ui->edtPattern, SIGNAL(textChanged(QString)), SLOT(changePattern(QString)));
    connect(_ui->cmbEditor, SIGNAL(currentIndexChanged(int)), SLOT(changeEditor(int)));
}

void MimeTypes::onItemActivated(const QModelIndex& index)
{
    _ui->editBox->setEnabled(true);
    _ui->edtPattern->setText(index.data(Qt::UserRole).toString());
    _ui->cmbEditor->setCurrentIndex(_ui->cmbEditor->findData(index.data(Qt::UserRole+1), Qt::UserRole));
}

void MimeTypes::changePattern(const QString& text)
{
    qobject_cast<MimeModel*>(_ui->table->model())->changePattern(text, _ui->table->currentIndex());
}

void MimeTypes::changeEditor(int row)
{
    qobject_cast<MimeModel*>(_ui->table->model())->changeEditor(_ui->cmbEditor->itemData(row).toString(), _ui->table->currentIndex());
}

void MimeTypes::addNewItem()
{
    QModelIndex index = qobject_cast<MimeModel*>(_ui->table->model())->addItem();
    _ui->table->setCurrentIndex(index);
    onItemActivated(index);
}

void MimeTypes::deleteItem()
{
    QModelIndex index = qobject_cast<MimeModel*>(_ui->table->model())->deleteItem(_ui->table->currentIndex());
    if (index.isValid()){
        _ui->table->setCurrentIndex(index);
        onItemActivated(index);
    }
}

// -----------------------------------------------------------

MimeModel::MimeModel(const QList<FileType>& data, const QMap<QString, QString>& editors, QObject * parent):
    QAbstractTableModel(parent),
    _data(data),
    _editors(editors)
{
}

int MimeModel::rowCount(const QModelIndex & parent) const
{
    return _data.length();
}

int MimeModel::columnCount(const QModelIndex & parent) const
{
    return 2;
}

QVariant MimeModel::data(const QModelIndex & index, int role) const
{
    if (!index.isValid() || (index.row() < 0 || index.row() >= _data.length()))
        return QVariant();

    FileType it = _data[index.row()];
    if (role == Qt::DisplayRole){
        if (index.column() == 0)
            return _editors[it.editor];
        if (index.column() == 1)
            return it.pattern;
    }
    if (role == Qt::UserRole+1){
        return it.editor;
    }
    if (role == Qt::UserRole){
        return it.pattern;
    }
    return QVariant();
}

QVariant MimeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal)
        return QVariant();
    if (role != Qt::DisplayRole)
        return QVariant();

    switch(section){
    case 0:
        return tr("Editor");
    case 1:
        return tr("Pattern");
    default:
        return "";
    }
}

void MimeModel::changePattern(const QString& text, const QModelIndex& index)
{
    _data[index.row()].pattern = text;
    emit dataChanged(createIndex(index.row(), 0), createIndex(index.row(), 1));
}

void MimeModel::changeEditor(const QString& text, const QModelIndex& index)
{
    _data[index.row()].editor = text;
    emit dataChanged(createIndex(index.row(), 0), createIndex(index.row(), 1));
}

QModelIndex MimeModel::addItem()
{
    beginInsertRows(QModelIndex(), _data.length()-1, _data.length()-1);
    _data.append(FileType("", "text"));
    QModelIndex index = createIndex(_data.length()-1, 0);
    endInsertRows();
    return index;
}

QModelIndex MimeModel::deleteItem(const QModelIndex& index)
{
    beginRemoveRows(QModelIndex(), index.row(), index.row());
    _data.removeAt(index.row());
    QModelIndex ret = createIndex(index.row()-1 > 0 ? index.row()-1 : 0, 0);
    endRemoveRows();
    return ret;
}

QList<FileType> MimeModel::types()
{
    return _data;
}

}
