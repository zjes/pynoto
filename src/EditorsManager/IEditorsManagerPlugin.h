#ifndef _IEDITORSMANAGERPLUGIN_H_
#define _IEDITORSMANAGERPLUGIN_H_
#include "Include/IPlugin.h"

class QAction;
namespace EditorsManager {

class IEditorsManagerPlugin: public IPlugin
{
    Q_OBJECT
public:
    virtual ~IEditorsManagerPlugin(){}
    virtual QWidget* createManager(QWidget* parent) = 0;
    virtual QWidget* widget() = 0;
    virtual QWidget* sampleEditor(const QString& lexer) = 0;
    virtual void setAction(QAction* action, const char* slot) = 0;
    virtual QWidget * openedDocumentsWidget(QWidget* parent) = 0;
    virtual bool close() = 0;
    virtual QString currentFileName() = 0;
signals:
    void undoAvailable(bool);
    void redoAvailable(bool);
    void pasteAvailable(bool);
    void copyAvailable(bool);
    void findAvailable(bool);
    void replaceAvailable(bool);
    void modificationChanged(bool);
    void editorClosed(const QString& fileName);
    void editorActivated(const QString& fileName);
    void modificationChanged(const QString& fileName, bool modify);
    void fileAvailable(bool);
public slots:
    virtual void openFileOffset(const QString& fileName, int offset = 0) = 0;
    virtual void resetEditor() = 0;
    virtual void jumpCurrentFileLine(int line) = 0;
    virtual void jumpFileLine(const QString& fileName, int line) = 0;
    virtual void checkIssues() = 0;
    virtual void forceCloseFile(const QString& fileName) = 0;
    virtual void closeFile(const QString& fileName) = 0;
};

}

Q_DECLARE_INTERFACE(EditorsManager::IEditorsManagerPlugin, "Pynoto.IEditorsManagerPlugin")

#endif
