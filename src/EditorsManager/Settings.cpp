#include <QVariant>
#include "Settings.h"

namespace EditorsManager {

QList<FileType> Settings::fileTypes()
{
    QList<FileType> data;
    int size = beginReadArray("editors/filetypes");
    for (int i = 0; i < size; ++i){
        setArrayIndex(i);
        data.append(FileType(value("pattern").toString(), value("editor").toString()));
    }
    endArray();
    if (!data.length()){
        data.append(FileType("*.py;*.pyw;*.py3;*pyw3", "python"));
        data.append(FileType("*.ui", "designer"));
    }
    return data;
}

void Settings::setFileTypes(const QList<FileType>& types)
{
    remove("editors/filetypes");
    beginWriteArray("editors/filetypes", types.length());
    int count = 0;
    foreach(FileType t, types){
        setArrayIndex(count++);
        setValue("pattern", t.pattern);
        setValue("editor", t.editor);
    }
    endArray();
}

}


