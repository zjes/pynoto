#ifndef _EMSETTINGS_H_
#define _EMSETTINGS_H_

#include <QSettings>

namespace EditorsManager {

struct FileType
{
    QString pattern;
    QString editor;
    FileType(): pattern(""), editor(""){}
    FileType(const QString & pat, const QString& ed):
        pattern(pat),
        editor(ed)
    {}
};

class Settings: public QSettings
{
    Q_OBJECT
    Q_PROPERTY(QList<FileType> fileTypes READ fileTypes WRITE setFileTypes)
public:
    QList<FileType> fileTypes();
    void setFileTypes(const QList<FileType>& types);
};
}

#endif
