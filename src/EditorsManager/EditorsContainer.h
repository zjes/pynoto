#ifndef _EDITORSCONTAINER_H_
#define _EDITORSCONTAINER_H_
#include <QWidget>
#include <QMap>
#include <QTimer>
#include <QStackedWidget>

namespace Editors {
    class IEditor;
}
namespace Project {
    class IProject;
}
class QFileSystemWatcher;
class QAbstractTableModel;
namespace EditorsManager {

class FindDialog;
class HistoryNavigate;

class EditorsContainer: public QWidget
{
    Q_OBJECT
public:
    EditorsContainer(QWidget* parent = NULL);

    enum EditorType {
        Text = 0,
        Python,
        XML,
        QRC,
        QML,
        UI
    };

    Editors::IEditor * createEditor(const QString& fileName, EditorType type, bool sample = false);
    QString selectedText();
    Editors::IEditor * currenEditor();
    bool isEditorOpened(const QString& fileName);
    bool close();
    void markDeleted(const QString& fileName);
signals:
    void undoAvailable(bool);
    void redoAvailable(bool);
    void pasteAvailable(bool);
    void copyAvailable(bool);
    void findAvailable(bool);
    void replaceAvailable(bool);
    void modificationChanged(bool);

    void editorActivated(Editors::IEditor*);
    void editorActivated(const QString&);
    void editorModificationChanged(Editors::IEditor*, bool);
    void editorModificationChanged(const QString&, bool);
    void editorOpened(Editors::IEditor*);
    void editorClosed(const QString&);
    void errorListChanged(QAbstractTableModel*);
    void fileAvailable(bool);
public slots:
    void openFileOffset(const QString& fileName, int offset = 0);
    //File menu
    void closeEditor();
    void save();
    void saveAll();
    void saveAs();
    void open();
    //Edit menu
    void undo();
    void redo();
    void copy();
    void cut();
    void paste();
    void del();
    void selectAll();
    void upperCase();
    void lowerCase();
    void capitalize();
    void camelize();
    void underlize();
    void showFind();
    //Find
    void findNext(bool inc = true);
    void findPrevious(bool inc = true);
    void replace();
    void replaceFind();
    void replaceFindPrevious();
    void replaceAll();
    void startIncrementalFind();


    void activateEditor(Editors::IEditor*);
    void activateEditor(const QString& fileName);
    void closeEditor(const QString& fileName);
    void closeEditor(const QStringList& fileNames);
    void closeEditor(const QString& fileName, bool ask);
    void resetEditor();
    void jumpLine(int line);
    void switchSourceUi();
private:
    bool isTextFile(const QString& fileName);
    bool _closeEditor(const QString& fileName, bool activate = true, bool ask = true);
    EditorType typeByFileName(const QString& fileName);
    EditorType editorTypeByName(const QString& name);
private slots:
    void historyNavigate();
    void delNavigator(const QString& fileName);
    void closeCurrentEditor();
    void onFileChanged(const QString&);
    void _onFileChanged();
    void onModificationChanged(Editors::IEditor*, bool);
private:
    Editors::IEditor *_currentEditor;
    QStackedWidget * _place;
    QMap<QString, Editors::IEditor*> _openedEditors;
    FindDialog * _find;
    HistoryNavigate * _historyNavi;
    QStringList _stack;
    QFileSystemWatcher *_watcher;
    QStringList _changedFiles;
    QTimer _changedTimer;
    QStringList _toSaveFile;
    Project::IProject *_project;
};

}

#endif
