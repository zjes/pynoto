#include "FocusLineEdit.h"

FocusLineEdit::FocusLineEdit(QWidget* parent):
    QLineEdit(parent)
{

}

void FocusLineEdit::focusInEvent(QFocusEvent *)
{
    emit focused(true);
}

void FocusLineEdit::focusOutEvent(QFocusEvent *)
{
    emit focused(false);
}
