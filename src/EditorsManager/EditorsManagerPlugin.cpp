#include <QDebug>
#include <QtPlugin>
#include <QAction>
#include "Include/IEditor.h"
#include "EditorsManagerPlugin.h"
#include "EditorsContainer.h"
#include "OpenedDocuments.h"
#include "Preferences/MimeTypes.h"

namespace EditorsManager {

QWidget* EditorsManagerPlugin::createManager(QWidget* parent)
{
    _widget = new EditorsContainer(parent);
    connect(_widget, SIGNAL(copyAvailable(bool)),       this, SIGNAL(copyAvailable(bool)));
    connect(_widget, SIGNAL(redoAvailable(bool)),       this, SIGNAL(redoAvailable(bool)));
    connect(_widget, SIGNAL(pasteAvailable(bool)),      this, SIGNAL(pasteAvailable(bool)));
    connect(_widget, SIGNAL(undoAvailable(bool)),       this, SIGNAL(undoAvailable(bool)));
    connect(_widget, SIGNAL(modificationChanged(bool)), this, SIGNAL(modificationChanged(bool)));
    connect(_widget, SIGNAL(findAvailable(bool)),       this, SIGNAL(findAvailable(bool)));
    connect(_widget, SIGNAL(replaceAvailable(bool)),    this, SIGNAL(replaceAvailable(bool)));
    connect(_widget, SIGNAL(errorListChanged(QAbstractTableModel*)), this, SIGNAL(errorListChanged(QAbstractTableModel*)));
    connect(_widget, SIGNAL(editorClosed(QString)),     this, SIGNAL(editorClosed(QString)));
    connect(_widget, SIGNAL(editorActivated(QString)),  this, SIGNAL(editorActivated(QString)));
    connect(_widget, SIGNAL(editorModificationChanged(QString,bool)), this, SIGNAL(modificationChanged(QString,bool)));
    connect(_widget, SIGNAL(fileAvailable(bool)),       this, SIGNAL(fileAvailable(bool)));
    return _widget;
}

bool EditorsManagerPlugin::initialize()
{
    initialized = true;
    return true;
}

void EditorsManagerPlugin::openFileOffset(const QString& fileName, int offset)
{
    _widget->openFileOffset(fileName, offset);
}

QWidget* EditorsManagerPlugin::widget()
{
    return _widget;
}

void EditorsManagerPlugin::setAction(QAction* action, const char* slot)
{
    _widget->addAction(action);
    connect(action, SIGNAL(triggered()), _widget, slot);
}

QWidget * EditorsManagerPlugin::openedDocumentsWidget(QWidget* parent)
{
    _openedDocuments = new OpenedDocuments(_widget, parent);
    return _openedDocuments;
}

void EditorsManagerPlugin::undo(){_widget->undo();}
void EditorsManagerPlugin::redo(){_widget->redo();}
void EditorsManagerPlugin::copy(){_widget->copy();}
void EditorsManagerPlugin::cut() {_widget->cut();}
void EditorsManagerPlugin::resetEditor(){_widget->resetEditor();}

bool EditorsManagerPlugin::close()
{
    return _widget->close();
}

void EditorsManagerPlugin::jumpCurrentFileLine(int line)
{
    _widget->jumpLine(line);
}

void EditorsManagerPlugin::jumpFileLine(const QString& fileName, int line)
{
    _widget->openFileOffset(fileName);
    _widget->jumpLine(line);
}

QString EditorsManagerPlugin::currentFileName()
{
    return _widget->currenEditor() ? _widget->currenEditor()->fileName() : "";
}

void EditorsManagerPlugin::checkIssues()
{
    if (_widget->currenEditor())
        _widget->currenEditor()->checkIssues();
}

QList<IPreferencesPage*> EditorsManagerPlugin::preferences()
{
    return (QList<IPreferencesPage*>() << new MimeTypes);
}

QWidget* EditorsManagerPlugin::sampleEditor(const QString& lexer)
{
    Editors::IEditor *editor;
    if (lexer == "text")
        editor = _widget->createEditor("", EditorsContainer::Text, true);
    if (lexer == "python")
        editor = _widget->createEditor("", EditorsContainer::Python, true);
    if (editor)
        editor->load();
    return editor;
}

void EditorsManagerPlugin::forceCloseFile(const QString& fileName)
{
    _widget->markDeleted(fileName);
    _widget->closeEditor(fileName, false);
}

void EditorsManagerPlugin::closeFile(const QString& fileName)
{
    _widget->closeEditor(fileName, true);
}

}
