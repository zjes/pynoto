#include <QDebug>
#include <QFileInfo>
#include <QDir>
#include <QMenu>
#include "OpenedDocuments.h"
#include "EditorsContainer.h"
#include "Include/IEditor.h"

namespace EditorsManager {

OpenedDocuments::OpenedDocuments(EditorsContainer * editors, QWidget* parent):
    QListWidget(parent),
    _editors(editors)
{
    setFrameStyle(QFrame::NoFrame);
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(_editors, SIGNAL(editorModificationChanged(Editors::IEditor*, bool)), SLOT(onEditorModified(Editors::IEditor*, bool)));
    connect(_editors, SIGNAL(editorActivated(Editors::IEditor*)), SLOT(onEditorActivated(Editors::IEditor*)));
    connect(_editors, SIGNAL(editorOpened(Editors::IEditor*)), SLOT(onEditorOpened(Editors::IEditor*)));
    connect(_editors, SIGNAL(editorClosed(QString)), SLOT(onEditorClosed(QString)));

    connect(this, SIGNAL(currentRowChanged(int)), SLOT(onCurrentChange(int)));
    connect(this, SIGNAL(customContextMenuRequested(QPoint)), SLOT(onCustomMenu(QPoint)));
}

OpenedDocuments::~OpenedDocuments()
{

}

void OpenedDocuments::onEditorModified(Editors::IEditor* ed, bool modified)
{
    for(int ind = 0; ind < count(); ind++){
        if (item(ind)->data(Qt::UserRole+1).toString() == ed->fileName()){
            item(ind)->setText(item(ind)->data(Qt::UserRole+2).toString()+(modified ? " *" : ""));
            break;
        }
    }
}

void OpenedDocuments::onEditorActivated(Editors::IEditor* ed)
{
    for(int ind = 0; ind < count(); ind++){
        if (item(ind)->data(Qt::UserRole+1).toString() == ed->fileName()){
            setCurrentRow(ind);
            break;
        }
    }
}

void OpenedDocuments::onEditorOpened(Editors::IEditor* ed)
{
    QFileInfo info(ed->fileName());
    QString name = info.fileName();
    if (name == "__init__.py"){
        QFileInfo dir(info.path());
        name += " ("+dir.fileName()+")";
    }
    QListWidgetItem *item = new QListWidgetItem(name);
    item->setData(Qt::UserRole+1, ed->fileName());
    item->setData(Qt::UserRole+2, name);
    addItem(item);
}

void OpenedDocuments::onCurrentChange(int row)
{
    if (row >= 0)
        _editors->activateEditor(item(row)->data(Qt::UserRole+1).toString());
}

void OpenedDocuments::onCustomMenu(QPoint point)
{
    QListWidgetItem *item = itemAt(point);
    QMenu *menu = new QMenu(this);
    if (item){
        menu->addAction(tr("Close \"%1\"").arg(item->text()), this, SLOT(onCloseSelectedEditor()));
        menu->addAction(tr("Close all except \"%1\"").arg(item->text()), this, SLOT(onCloseAllExceptSelectedEditor()));
    }
    if (count()){
        menu->addAction(tr("Close all"), this, SLOT(onCloseAllEditors()));
    }
    if (menu->actions().count()){
        menu->popup(mapToGlobal(point));
    }
}

void OpenedDocuments::onCloseSelectedEditor()
{
    _editors->closeEditor(currentItem()->data(Qt::UserRole+1).toString());
}

void OpenedDocuments::onCloseAllExceptSelectedEditor()
{
    QStringList pathes;
    for(int i = 0; i < count(); ++i){
        if (i == currentRow())
            continue;
        pathes.append(item(i)->data(Qt::UserRole+1).toString());
    }
    _editors->closeEditor(pathes);
}

void OpenedDocuments::onCloseAllEditors()
{
    QStringList pathes;
    for(int i = 0; i < count(); ++i){
        pathes.append(item(i)->data(Qt::UserRole+1).toString());
    }
    _editors->closeEditor(pathes);
}

void OpenedDocuments::onEditorClosed(const QString& fileName)
{
    for(int i = 0; i < count(); ++i){
        if (item(i)->data(Qt::UserRole+1).toString() == fileName){
            QListWidgetItem* it = takeItem(i);
            delete it;
            break;
        }
    }
}

}
