#ifndef _OPENEDDOCUMENTS_H_
#define _OPENEDDOCUMENTS_H_
#include <QListWidget>

namespace Editors {
    class IEditor;
}
namespace EditorsManager {
class EditorsContainer;

class OpenedDocuments: public QListWidget
{
    Q_OBJECT
public:
    OpenedDocuments(EditorsContainer * editors, QWidget* parent);
    virtual ~OpenedDocuments();
private slots:
    void onEditorModified(Editors::IEditor*, bool);
    void onEditorActivated(Editors::IEditor*);
    void onEditorOpened(Editors::IEditor*);
    void onCurrentChange(int);
    void onCustomMenu(QPoint);
    void onCloseSelectedEditor();
    void onCloseAllExceptSelectedEditor();
    void onCloseAllEditors();
    void onEditorClosed(const QString&);
private:
    EditorsContainer * _editors;
};

}

#endif
