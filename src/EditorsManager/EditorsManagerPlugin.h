#ifndef _EDITORSMANAGERPLUGIN_H_
#define _EDITORSMANAGERPLUGIN_H_
#include "IEditorsManagerPlugin.h"

class QAbstractTableModel;
namespace EditorsManager {

class EditorsContainer;
class OpenedDocuments;

class EditorsManagerPlugin : public IEditorsManagerPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IEditorsManagerPlugin")
    Q_INTERFACES(EditorsManager::IEditorsManagerPlugin)
public:
    virtual ~EditorsManagerPlugin(){}
    virtual QWidget* createManager(QWidget* parent);
    virtual QWidget* widget();
    virtual void setAction(QAction* action, const char* slot);
    virtual bool initialize();
    virtual QWidget * openedDocumentsWidget(QWidget* parent);
    virtual bool close();
    virtual QString currentFileName();
    virtual QList<IPreferencesPage*> preferences();
    virtual QWidget* sampleEditor(const QString& lexer);
public slots:
    virtual void openFileOffset(const QString& fileName, int offset = 0);
    virtual void undo();
    virtual void redo();
    virtual void copy();
    virtual void cut();
    virtual void resetEditor();
    virtual void jumpCurrentFileLine(int line);
    virtual void jumpFileLine(const QString& fileName, int line);
    virtual void checkIssues();
    virtual void forceCloseFile(const QString& fileName);
    virtual void closeFile(const QString& fileName);
signals:
    void errorListChanged(QAbstractTableModel*);
private:
    EditorsContainer* _widget;
    OpenedDocuments * _openedDocuments;
};

}

#endif
