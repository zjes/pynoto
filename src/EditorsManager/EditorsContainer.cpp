#include <QVBoxLayout>
#include <QAction>
#include <QFileSystemWatcher>
#include <QMessageBox>
#include <QTimer>
#include <QRegExp>
#include <QFileDialog>
#include "EditorsContainer.h"
#include "Include/IEditor.h"
#include "Include/ITextEditor.h"
#include "Include/IEditorPlugin.h"
#include "Include/IDesignerPlugin.h"
#include "Include/Aux.h"
#include "Include/IProjectPlugin.h"
#include "Include/IProject.h"
#include "FindDialog.h"
#include "HistoryNavigate.h"
#include "Settings.h"
#include "Editors/FileProperty.h"

namespace EditorsManager {

static const char bigEndianByteOrderMarkC[] = "\xFE\xFF";
static const char littleEndianByteOrderMarkC[] = "\xFF\xFE";

EditorsContainer::EditorsContainer(QWidget* parent):
    QWidget(parent),
    _currentEditor(NULL),
    _historyNavi(NULL)
{
    QVBoxLayout *box = new QVBoxLayout(this);
    setLayout(box);

    _watcher = new QFileSystemWatcher(this);
    connect(_watcher, SIGNAL(fileChanged(QString)), SLOT(onFileChanged(QString)));

    _project = Aux::plugin<Project::IProjectPlugin*>("project")->project();

    _place = new QStackedWidget(this);

    _find = new FindDialog(this);
    connect(_find, SIGNAL(findAvailable(bool)), SIGNAL(findAvailable(bool)));
    connect(_find, SIGNAL(replaceAvailable(bool)), SIGNAL(replaceAvailable(bool)));
    connect(_find, SIGNAL(findNext(bool)), SLOT(findNext(bool)));
    connect(_find, SIGNAL(findPrevious(bool)), SLOT(findPrevious(bool)));
    connect(_find, SIGNAL(replace()), SLOT(replace()));
    connect(_find, SIGNAL(replaceFind()), SLOT(replaceFind()));
    connect(_find, SIGNAL(replaceFindPrevious()), SLOT(replaceFindPrevious()));
    connect(_find, SIGNAL(replaceAll()), SLOT(replaceAll()));
    connect(_find, SIGNAL(startIncrementalFind()), SLOT(startIncrementalFind()));

    layout()->addWidget(_place);
    layout()->addWidget(_find);
    layout()->setContentsMargins(0, 0, 0, 0);
    _find->hide();

    QAction *a = new QAction(this);
    a->setShortcut(Qt::ControlModifier + Qt::Key_Tab);
    connect(a, SIGNAL(triggered()), SLOT(historyNavigate()));
    _place->addAction(a);

    _changedTimer.setSingleShot(true);
    _changedTimer.setInterval(500);
    _changedTimer.stop();
    connect(&_changedTimer, SIGNAL(timeout()), SLOT(_onFileChanged()));
}

Editors::IEditor * EditorsContainer::createEditor(const QString& fileName, EditorType type, bool sample)
{
    Editors::IEditorPlugin * plugin = 0;
    switch(type){
    case Text:
        plugin = Aux::plugin<Editors::IEditorPlugin*>("texteditor");
        break;
    case Python:
        plugin = Aux::plugin<Editors::IEditorPlugin*>("pythoneditor");
        break;
    case UI:
        plugin = Aux::plugin<Designer::IDesignerPlugin*>("designer");
        break;
    default:
        break;
    }
    if (plugin)
        return plugin->createEditor(fileName, this, sample);
    return 0;
}

void EditorsContainer::openFileOffset(const QString& fileName, int offset)
{
    if (!_openedEditors.contains(fileName)){
        if (offset){
            Editors::FileProperty prop(fileName);
            prop.setCursorPos(offset);
        }
        Editors::IEditor * edt = NULL;
        if (isTextFile(fileName)) {
            EditorType etype = typeByFileName(fileName);
            edt = createEditor(fileName, etype);
        }

        if (!edt)
            return;

        _watcher->addPath(fileName);
        _place->addWidget(edt);
        _stack.prepend(fileName);
        edt->load();
        connect(edt, SIGNAL(editorModificationChanged(Editors::IEditor*,bool)), SLOT(onModificationChanged(Editors::IEditor*,bool)));
        connect(edt, SIGNAL(jumpToFileOffset(QString,int)), this, SLOT(openFileOffset(QString,int)));
        _openedEditors[fileName] = edt;
        emit editorOpened(edt);
    }
    activateEditor(_openedEditors[fileName]);
    emit fileAvailable(true);
    if (offset){
        Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_openedEditors[fileName]);
        if (ed)
            ed->jumpToOffset(offset);
    }
}

EditorsContainer::EditorType EditorsContainer::typeByFileName(const QString& fileName)
{
    QRegExp exp;
    exp.setPatternSyntax(QRegExp::Wildcard);
    exp.setCaseSensitivity(Qt::CaseInsensitive);
    Settings set;
    foreach(FileType t, set.fileTypes()){
        QStringList pattern = t.pattern.split(";");
        EditorType etype = editorTypeByName(t.editor);
        foreach(QString pat, pattern){
            exp.setPattern(pat.trimmed());
            if (exp.exactMatch(fileName)){
                return etype;
            }
        }
    }
    return Text;
}

EditorsContainer::EditorType EditorsContainer::editorTypeByName(const QString& name)
{
    if (name == "python")
        return Python;
    if (name == "designer")
        return UI;
    if (name == "resources")
        return QRC;
    if (name == "qml")
        return QML;
    return Text;
}

void EditorsContainer::activateEditor(Editors::IEditor* edt)
{
    if (_currentEditor){
        disconnect(_currentEditor, SIGNAL(copyAvailable(bool)));
        disconnect(_currentEditor, SIGNAL(pasteAvailable(bool)));
        disconnect(_currentEditor, SIGNAL(redoAvailable(bool)));
        disconnect(_currentEditor, SIGNAL(undoAvailable(bool)));
        disconnect(_currentEditor, SIGNAL(modificationChanged(bool)));
        disconnect(_currentEditor, SIGNAL(closeEditor()), this, SLOT(closeCurrentEditor()));
        disconnect(_currentEditor, SIGNAL(errorListChanged(QAbstractTableModel*)), this, SIGNAL(errorListChanged(QAbstractTableModel*)));
    }
    _currentEditor = edt;

    connect(_currentEditor, SIGNAL(copyAvailable(bool)), this, SIGNAL(copyAvailable(bool)));
    connect(_currentEditor, SIGNAL(pasteAvailable(bool)), this, SIGNAL(pasteAvailable(bool)));
    connect(_currentEditor, SIGNAL(redoAvailable(bool)), this, SIGNAL(redoAvailable(bool)));
    connect(_currentEditor, SIGNAL(undoAvailable(bool)), this, SIGNAL(undoAvailable(bool)));
    connect(_currentEditor, SIGNAL(modificationChanged(bool)), this, SIGNAL(modificationChanged(bool)));
    connect(_currentEditor, SIGNAL(closeEditor()), this, SLOT(closeCurrentEditor()));
    connect(_currentEditor, SIGNAL(errorListChanged(QAbstractTableModel*)), this, SIGNAL(errorListChanged(QAbstractTableModel*)));

    _currentEditor->updateStatus();

    _stack.removeOne(_currentEditor->fileName());
    _stack.prepend(_currentEditor->fileName());

    _place->setCurrentWidget(_currentEditor);
    _currentEditor->setFocus();
    emit editorActivated(_currentEditor);
    emit editorActivated(_currentEditor->fileName());
    emit errorListChanged(_currentEditor->errorListModel());
}

void EditorsContainer::activateEditor(const QString& fileName)
{
    activateEditor(_openedEditors[fileName]);
}

bool EditorsContainer::isTextFile(const QString& fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
        return false;
    QByteArray data = file.readAll();
    file.close();

    const int size = data.size();
    for (int i = 0; i < size; i++) {
        const char c = data.at(i);
        if (c >= 0x01 && c < 0x09) // Sure-fire binary
            return false;
        if (c == 0)             // Check for UTF16
            return data.startsWith(bigEndianByteOrderMarkC) || data.startsWith(littleEndianByteOrderMarkC);
    }
    return true;
}

QString EditorsContainer::selectedText()
{
    return _currentEditor ? _currentEditor->selectedText() : "";
}

void EditorsContainer::undo()
{ if (_currentEditor) _currentEditor->undo(); }

void EditorsContainer::redo()
{ if (_currentEditor) _currentEditor->redo(); }

void EditorsContainer::copy()
{ if (_currentEditor) _currentEditor->copy(); }

void EditorsContainer::cut()
{ if (_currentEditor) _currentEditor->cut(); }

void EditorsContainer::closeEditor()
{ closeCurrentEditor(); }

void EditorsContainer::save()
{
    if (_currentEditor){
        _toSaveFile.append(_currentEditor->fileName());
        _currentEditor->save();
    }
}

void EditorsContainer::saveAll()
{
    foreach(Editors::IEditor *ed, _openedEditors.values()){
        _toSaveFile.append(ed->fileName());
        ed->save();
    }
}

void EditorsContainer::saveAs()
{
    QString currentName = _currentEditor->fileName();
    QFileDialog dlg(this);
    QFileInfo info(currentName);
    dlg.setDirectory(info.absolutePath());
    dlg.setDefaultSuffix(info.suffix());
    dlg.setConfirmOverwrite(true);
    dlg.setAcceptMode(QFileDialog::AcceptSave);
    dlg.selectFile(info.absoluteFilePath());

    if (dlg.exec() == QDialog::Accepted){
        QString name = dlg.selectedFiles()[0];
        if (QFile::exists(name))
            QFile::remove(name);
        if (QFile::copy(currentName, name)){
            closeCurrentEditor();
            markDeleted(currentName);
            QFile::remove(currentName);
            openFileOffset(name);
        }
    }
}

void EditorsContainer::open()
{
    QString name = QFileDialog::getOpenFileName(this, tr("Open file"));
    if (!name.isEmpty())
        openFileOffset(name);
}

void EditorsContainer::paste()
{ if (_currentEditor) _currentEditor->paste(); }

void EditorsContainer::del()
{ if (_currentEditor) _currentEditor->del(); }

void EditorsContainer::selectAll()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->selectAll();
}

void EditorsContainer::upperCase()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->upperCase();
}

void EditorsContainer::lowerCase()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->lowerCase();
}

void EditorsContainer::capitalize()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->capitalize();
}

void EditorsContainer::camelize()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->camelize();
}

void EditorsContainer::underlize()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->underlize();
}

void EditorsContainer::showFind()
{
    _find->show();
}

void EditorsContainer::historyNavigate()
{
    if (_historyNavi) {
        _historyNavi->next();
    } else {
        _historyNavi = new HistoryNavigate(_stack, _place);
        connect(_historyNavi, SIGNAL(closed(QString)), SLOT(delNavigator(QString)));
    }
}

void EditorsContainer::delNavigator(const QString& fileName)
{
    activateEditor(_openedEditors[fileName]);
    _historyNavi->deleteLater();
    _historyNavi = NULL;
}

void EditorsContainer::closeCurrentEditor()
{
    if (_currentEditor)
        closeEditor(_currentEditor->fileName());
}

Editors::IEditor * EditorsContainer::currenEditor()
{
    return _currentEditor;
}

void EditorsContainer::closeEditor(const QString& fileName)
{
    if (isEditorOpened(fileName))
        _closeEditor(fileName);
}

void EditorsContainer::closeEditor(const QString& fileName, bool ask)
{
    if (isEditorOpened(fileName))
        _closeEditor(fileName, true, ask);
}

void EditorsContainer::closeEditor(const QStringList& fileNames)
{
    foreach(QString name, fileNames)
        closeEditor(name);
}

bool EditorsContainer::isEditorOpened(const QString& fileName)
{
    return _openedEditors.contains(fileName);
}

bool EditorsContainer::_closeEditor(const QString& fileName, bool activate, bool ask)
{
    if (ask && _openedEditors[fileName]->isModified()){
        if (QMessageBox::question(this, tr("File is modified"), tr("File is modified")+"\n"+fileName+tr("\nDo you want to close this anyway?"), QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
            return false;
    }
    for(int i = 0; i < _place->count(); i++){
        if (qobject_cast<Editors::IEditor*>(_place->widget(i))->fileName() == fileName){
            _place->removeWidget(_place->widget(i));
            Editors::IEditor *ed = _openedEditors.take(fileName);
            ed->saveState();
            ed->deleteLater();
            _stack.removeOne(fileName);
            _watcher->removePath(fileName);
            if (activate){
                _project->closeFile(fileName);
            }
            emit editorClosed(fileName);
            if (activate && _stack.length() > 0){
                activateEditor(_stack.first());
            }
            emit fileAvailable(_stack.length() > 0);
            break;
        }
    }
    if (_stack.length() == 0){
        _currentEditor = NULL;
        emit copyAvailable(false);
        emit pasteAvailable(false);
        emit redoAvailable(false);
        emit undoAvailable(false);
        emit modificationChanged(false);
    }
    return true;
}

void EditorsContainer::onFileChanged(const QString& file)
{
    if (_changedFiles.contains(file))
        return;

    _changedFiles.append(file);
    _changedTimer.start();
}

void EditorsContainer::_onFileChanged()
{
    foreach(QString file, _changedFiles){
        if (_toSaveFile.contains(file)){
            _toSaveFile.removeAll(file);
            _changedFiles.removeAll(file);
            continue;
        }
    }

    if (!_changedFiles.length())
        return;

    int btn = QMessageBox::question(this, tr("File was changed"), tr("Files\n%1\nWas changed externaly, Reload it?").arg(_changedFiles.join("\n")), QMessageBox::Yes, QMessageBox::No);
    if (btn == QMessageBox::Yes){
        foreach(QString file, _changedFiles){
            _openedEditors[file]->reload();
        }
    }
    _changedFiles.clear();
}

void EditorsContainer::findNext(bool inc)
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed){
        if (inc)
            ed->startIncrementalFind();
        ed->find(_find->findText(), true);
    }
}

void EditorsContainer::findPrevious(bool inc)
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed){
        if (inc)
            ed->startIncrementalFind();
        ed->find(_find->findText(), false);
    }
}

void EditorsContainer::replace()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->replace(_find->findText(), _find->replaceText(), false, true);
}

void EditorsContainer::replaceFind()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->replace(_find->findText(), _find->replaceText(), true, true);
}

void EditorsContainer::replaceFindPrevious()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->replace(_find->findText(), _find->replaceText(), true, false);
}

void EditorsContainer::replaceAll()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->replaceAll(_find->findText(), _find->replaceText());
}

void EditorsContainer::startIncrementalFind()
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->startIncrementalFind();
}

void EditorsContainer::resetEditor()
{
    _find->hide();
    _place->setFocus();
    if (_place->currentWidget())
        _place->currentWidget()->setFocus();
    if (_currentEditor){
        _currentEditor->reset();
        _currentEditor->setFocus();
    }
}

bool EditorsContainer::close()
{
    QStringList modified;
    foreach(Editors::IEditor* ed, _openedEditors.values()){
        if (ed->isModified())
            modified.append(ed->fileName());
    }
    if (modified.length()){
        if (QMessageBox::question(this, tr("File is modified"), tr("File is modified")+"\n"+modified.join("\n")+tr("\nDo you want to close this anyway?"), QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
            return false;
    }

    foreach(QString fileName, _openedEditors.keys()){
        if (!_closeEditor(fileName, false, false))
            return false;
    }
    return true;
}

void EditorsContainer::jumpLine(int line)
{
    Editors::ITextEditor* ed = qobject_cast<Editors::ITextEditor*>(_currentEditor);
    if (ed)
        ed->jumpToLine(line);
}

void EditorsContainer::onModificationChanged(Editors::IEditor* ed, bool mod)
{
    emit editorModificationChanged(ed->fileName(), mod);
    emit editorModificationChanged(ed, mod);
}

void EditorsContainer::markDeleted(const QString& fileName)
{
    _watcher->removePath(fileName);
}

void EditorsContainer::switchSourceUi()
{
    QFileInfo info(_currentEditor->fileName());
    if (info.suffix() == "ui"){
        QString pyName = info.absoluteDir().filePath(info.baseName()+".py");
        if (QFile::exists(pyName))
            openFileOffset(pyName);
    } else if (info.suffix() == "py"){
        QString uiName = info.absoluteDir().filePath(info.baseName()+".ui");
        if (QFile::exists(uiName))
            openFileOffset(uiName);
    }
}

}
