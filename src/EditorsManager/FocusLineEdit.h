#ifndef _FOCUSLINEEDIT_H_
#define _FOCUSLINEEDIT_H_
#include <QLineEdit>

class FocusLineEdit: public QLineEdit
{
    Q_OBJECT
public:
    FocusLineEdit(QWidget* parent = NULL);
signals:
    void focused(bool);
protected:
    void focusInEvent(QFocusEvent *);
    void focusOutEvent(QFocusEvent *);

};

#endif
