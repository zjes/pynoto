#include "FindDialog.h"
#include "Include/Aux.h"
#include "EditorsContainer.h"
#include "ui_FindDialog.h"

namespace EditorsManager {

FindDialog::FindDialog(EditorsContainer* cont) :
    QWidget(cont),
    _ui(new Ui::FindDialog),
    _container(cont)
{
    _ui->setupUi(this);
    _ui->closeBtn->setIcon(Aux::icons()->icon("window-close"));
    connect(_ui->closeBtn, SIGNAL(clicked()), SLOT(hide()));
    emit findAvailable(false);

    connect(_ui->findEdt, SIGNAL(textChanged(QString)), SLOT(onTextChanged(QString)));
    connect(_ui->findEdt, SIGNAL(focused(bool)), SLOT(onFindFocus(bool)));
    connect(_ui->findEdt, SIGNAL(returnPressed()), SLOT(onFindNext()));
    connect(_ui->findNextBtn, SIGNAL(clicked()), SLOT(onFindNext()));
    connect(_ui->findPrevBtn, SIGNAL(clicked()), SLOT(onFindPrevious()));
    connect(_ui->replaceBtn, SIGNAL(clicked()), SIGNAL(replace()));
    connect(_ui->replaceFindBtn, SIGNAL(clicked()), SIGNAL(replaceFind()));
    connect(_ui->replaceAllBtn, SIGNAL(clicked()), SIGNAL(replaceAll()));

    connect(_ui->replaceEdt, SIGNAL(textChanged(QString)), SLOT(onReplaceChanged(QString)));
    connect(_ui->replaceEdt, SIGNAL(returnPressed()), SIGNAL(replaceFind()));
}

FindDialog::~FindDialog()
{
    delete _ui;
}

void FindDialog::showEvent(QShowEvent *)
{
    _ui->findEdt->setFocus(Qt::TabFocusReason);
    QString selText = _container->selectedText();
    if (!selText.isEmpty() && !selText.contains("\n")){
        _ui->findEdt->setText(selText);
        _ui->findEdt->selectAll();
    }
    _ui->replaceAllBtn->setEnabled(!_ui->replaceEdt->text().isEmpty());
    _ui->replaceBtn->setEnabled(!_ui->replaceEdt->text().isEmpty());
    _ui->replaceFindBtn->setEnabled(!_ui->replaceEdt->text().isEmpty());
}

void FindDialog::activate()
{
    _ui->findEdt->setFocus(Qt::TabFocusReason);
    _ui->findEdt->selectAll();
}

void FindDialog::onTextChanged(const QString& text)
{
    emit findAvailable(!text.isEmpty());
    emit findNext(false);
}

QString FindDialog::findText()
{
    return _ui->findEdt->text();
}

QString FindDialog::replaceText()
{
    return _ui->replaceEdt->text();
}

void FindDialog::onReplaceChanged(const QString& text)
{
    _ui->replaceAllBtn->setEnabled(!text.isEmpty());
    _ui->replaceBtn->setEnabled(!text.isEmpty());
    _ui->replaceFindBtn->setEnabled(!text.isEmpty());
    emit replaceAvailable(!text.isEmpty() && !_ui->findEdt->text().isEmpty());
}

void FindDialog::onFindFocus(bool focus)
{
    if (focus)
        emit startIncrementalFind();
}

void FindDialog::onFindNext()
{
    emit startIncrementalFind();
    emit findNext();
}

void FindDialog::onFindPrevious()
{
    emit startIncrementalFind();
    emit findPrevious();
}

}
