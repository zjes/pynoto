#include <QDebug>
#include <QEvent>
#include <QKeyEvent>
#include <QFileInfo>
#include "HistoryNavigate.h"

namespace EditorsManager {

HistoryNavigate::HistoryNavigate(const QStringList & history, QWidget *parent):
    QListWidget(parent)
{
    setWindowFlags(Qt::Widget | Qt::Popup);
    setAttribute(Qt::WA_DeleteOnClose);
    _current = 1;

    foreach(QString file, history){
        QFileInfo inf = QFileInfo(file);
        QString name = inf.fileName();
        if (inf.baseName() == "__init__")
            name = name+" ("+QFileInfo(inf.absolutePath()).fileName()+")";
        QListWidgetItem *it = new QListWidgetItem(name, this);
        it->setData(Qt::UserRole, file);
    }
    setCurrentRow(_current);
    show();
}

HistoryNavigate::~HistoryNavigate()
{
}

void HistoryNavigate::next()
{
    _current++;
    if (_current >= count())
        _current = 0;

    setCurrentRow(_current);
}

void HistoryNavigate::previous()
{

}

bool HistoryNavigate::event(QEvent *event)
{
    if (event->type() == QEvent::Show){
        QPoint p = parentWidget()->mapToGlobal(parentWidget()->rect().topLeft());
        move(p.x() + (parentWidget()->geometry().width()-width())/2, p.y() + (parentWidget()->geometry().height()-height())/2);
        setFocus();
        return false;
    }

    if (event->type() == QEvent::KeyRelease && ((QKeyEvent*)event)->key() == Qt::Key_Control){
        close();
        return false;
    }

    if (event->type() == QEvent::KeyPress && ((QKeyEvent*)event)->key() == Qt::Key_Tab && ((QKeyEvent*)event)->modifiers() & Qt::ControlModifier){
        next();
        return false;
    }

    if (event->type() == QEvent::Close){
        if (currentItem())
            emit closed(currentItem()->data(Qt::UserRole).toString());
    }

    return QListWidget::event(event);
}

}
