#ifndef FINDDIALOG_H
#define FINDDIALOG_H

#include <QWidget>

namespace Ui {
    class FindDialog;
}

namespace EditorsManager {

class EditorsContainer;
class FindDialog : public QWidget
{
    Q_OBJECT

public:
    explicit FindDialog(EditorsContainer* cont);
    virtual ~FindDialog();

    QString findText();
    QString replaceText();
signals:
    void findAvailable(bool);
    void replaceAvailable(bool);
    void findNext(bool inc = true);
    void findPrevious(bool inc = true);
    void replace();
    void replaceFind();
    void replaceFindPrevious();
    void replaceAll();
    void startIncrementalFind();
public slots:
    void activate();
protected slots:
    void onTextChanged(const QString& text);
    void onReplaceChanged(const QString& text);
    void onFindFocus(bool);
    void onFindNext();
    void onFindPrevious();
protected:
    void showEvent(QShowEvent *);
private:
    Ui::FindDialog* _ui;
    EditorsContainer* _container;
};

}
#endif // FINDDIALOG_H
