#ifndef _PYTHON3ROPEPLUGIN_H_
#define _PYTHON3ROPEPLUGIN_H_
#include "IPythonCodePlugin.h"

namespace PythonCode {

class PythonCodePlugin: public IPythonCodePlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IPythonCodePlugin")
    Q_INTERFACES(PythonCode::IPythonCodePlugin)
public:
    virtual ~PythonCodePlugin();
    virtual bool initialize();
    virtual PythonCode::IPythonCode * python();
};

}

#endif
