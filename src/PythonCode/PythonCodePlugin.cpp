#include <QtPlugin>
#include "PythonCodePlugin.h"
#include "PythonCode.h"

namespace PythonCode {

PythonCodePlugin::~PythonCodePlugin()
{

}

bool PythonCodePlugin::initialize()
{
    initialized = true;
    return true;
}

PythonCode::IPythonCode * PythonCodePlugin::python()
{
    return new PythonCodeImp();
}

}
