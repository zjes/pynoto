#ifndef _IPYTHON3ROPEPLUGIN_H_
#define _IPYTHON3ROPEPLUGIN_H_
#include "Include/IPlugin.h"

namespace PythonCode {

class IPythonCode;

class IPythonCodePlugin: public IPlugin
{
    Q_OBJECT
public:
    virtual ~IPythonCodePlugin(){}
    virtual PythonCode::IPythonCode * python() = 0;
};

}

Q_DECLARE_INTERFACE(PythonCode::IPythonCodePlugin, "Pynoto.IPythonCodePlugin")

#endif
