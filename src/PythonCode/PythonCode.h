#ifndef _PYTHON3ROPE_H_
#define _PYTHON3ROPE_H_
#include <QProcess>
#include <QTcpSocket>
#include <QEventLoop>
#include "Include/IPythonCode.h"

class QDomNodeList;

namespace PythonCode {

class PythonCodeImp : public IPythonCode
{
    Q_OBJECT
public:
    PythonCodeImp();
    virtual ~PythonCodeImp();

    virtual void init(const QString& interpret, const QString& projectPath, const QString& mainScript, const QString& projectFile, const QStringList& extraPathes);
    virtual void codeComplit(const QString& fileName, const QString& cnt, int pos);
    virtual void errorList(const QString& fileName, const QString & content);
    virtual void findDeclaration(const QString& fileName, const QString& cnt, int pos);
    virtual void close();
private:
    void sendSyncRequest(const QString& req);
    void sendAsyncRequest(const QString& req);
    QString createRequest(const QString& name, const QVariantList& params);
    QString createRequest(const QString& name, const QString& inst, const QVariantList& params);
    void waitForResponce(int cmdNum);
    void parseAndEmitSignal(const QString& name, const QDomNodeList& items);
    void parseAndEmitErrorList(const QDomNodeList& items);
    void parseAndEmitCodeComplit(const QDomNodeList& items);
    void parseAndEmitDeclaration(const QDomNodeList& items);
    IPythonCode::CompletitionType getType(const QString & named);
private slots:
    void onRunError(QProcess::ProcessError);
    void onError();
    void onRead();
    void onSocReadyRead();
    void onCmdRead(int);
signals:
    void cmdRead(int cmd);
private:
    QProcess *_proc;
    QTcpSocket *_soc;
    int _cmdNum;
    QEventLoop _loop;
};

}

#endif
