#ifndef _IPYTHON_H_
#define _IPYTHON_H_
#include <QObject>

namespace PythonCode {

class IPythonCode: public QObject
{
    Q_OBJECT
public:
    enum Severity {
        CodeStyle = 1,
        Warning,
        Error,
        Suggestion
    };

    enum CompletitionType{
        Function = 1,
        Module,
        Class,
        Instance
    };
    struct Completition{
        CompletitionType type;
        QString name;
        QString scope;
    };

    struct ErrorItem {
        int line;
        Severity severity;
        QString message;
    };

    virtual ~IPythonCode(){}
public:
    virtual void init(const QString& interpret, const QString& projectPath, const QString& mainScript, const QString& projectFile, const QStringList& extraPathes) = 0;
    virtual void codeComplit(const QString& fileName, const QString& cnt, int pos) = 0;
    virtual void errorList(const QString& fileName, const QString & content) = 0;
    virtual void close() = 0;
    virtual void findDeclaration(const QString& fileName, const QString& cnt, int pos) = 0;
signals:
    void systemError(const QString& logString);
    void errors(const QString& fileName, const QList<ErrorItem>& items);
    void complitition(const QString& fileName, int pos, const QString& word, const QList<Completition>& items);
    void declaration(const QString& fileName, int offset, const QString& word, const QString& url, int wbegin, int wend);
};

}

#endif
