#ifndef _IICONPROVIDER_H_
#define _IICONPROVIDER_H_
#include <QFileInfo>
#include "Include/IPlugin.h"

namespace IconProvider {

class IIconProvider: public IPlugin
{
    Q_OBJECT
public:
    virtual ~IIconProvider(){}

    enum IconRole
    {
        Folder,
        ResourceGroup,
        FormGroup,
        File,
        PyModule
    };

    virtual QIcon icon(IconRole role) = 0;
    virtual QIcon icon(const QString & iconName) = 0;
    virtual QIcon icon(const QFileInfo & info) = 0;
    virtual QPixmap pixmap(const QString & imageName) = 0;
};

}

Q_DECLARE_INTERFACE(IconProvider::IIconProvider, "Pynoto.IIconProvider")

#endif
