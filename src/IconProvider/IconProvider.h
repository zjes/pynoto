#ifndef _ICONPROVIDER_H_
#define _ICONPROVIDER_H_

#include <QObject>
#include <QMap>
#include <QIcon>
#include <QFileInfo>
#include "IIconProvider.h"

namespace IconProvider {

class IconProviderImpl : public IIconProvider
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IIconProvider")
    Q_INTERFACES(IconProvider::IIconProvider)
public:
    IconProviderImpl();

    virtual bool initialize();
    virtual bool readSettings();

    virtual QIcon icon(IconRole role);
    virtual QIcon icon(const QString & iconName);
    virtual QIcon icon(const QFileInfo & info);
    virtual QPixmap pixmap(const QString & imageName);
private:
    void createIcon(QIcon & dest, const QIcon & src, QSize size, const QImage & srcPixmap, QIcon::Mode mode, QIcon::State state);
    QIcon createCompositeIcon(const QIcon proto, const QImage & img);
    QMap<QString, QIcon> _cache;
};

}


#endif
