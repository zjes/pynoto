#ifndef _IEDITORS_H_
#define _IEDITORS_H_
#include <QWidget>

namespace IconProvider{
    class IIconProvider;
}
class QAbstractTableModel;
namespace Editors {

class IEditor: public QWidget
{
    Q_OBJECT
public:
    IEditor(QWidget* parent);
    virtual ~IEditor(){}

    virtual bool isModified() = 0;
    virtual void updateStatus() = 0;
    virtual QString fileName() = 0;
    virtual QString selectedText() = 0;
    virtual void saveState() = 0;
    virtual QAbstractTableModel* errorListModel() = 0;
signals:
    void undoAvailable(bool);
    void redoAvailable(bool);
    void pasteAvailable(bool);
    void copyAvailable(bool);
    void modificationChanged(bool);
    void closeEditor();
    void errorListChanged(QAbstractTableModel*);
    void editorModificationChanged(Editors::IEditor*,bool);
    void jumpToFileOffset(const QString& file, int offset);
public slots:
    virtual bool save() = 0;
    virtual bool load() = 0;
    virtual bool reload() = 0;

    virtual void undo() = 0;
    virtual void redo() = 0;
    virtual void copy() = 0;
    virtual void cut() = 0;
    virtual void paste() = 0;
    virtual void del() = 0;
    virtual void reset() = 0;
    virtual void checkIssues() = 0;
};
}
Q_DECLARE_INTERFACE(Editors::IEditor, "Pynoto.IEditor")

#endif
