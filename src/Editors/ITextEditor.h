#include "IEditor.h"

namespace Editors {

class ITextEditor : public IEditor
{
    Q_OBJECT
    Q_INTERFACES(Editors::IEditor)
public:
    ITextEditor(QWidget* parent);
    virtual ~ITextEditor(){}

    virtual void selectAll() = 0;
    virtual void upperCase() = 0;
    virtual void lowerCase() = 0;
    virtual void capitalize() = 0;
    virtual void camelize() = 0;
    virtual void underlize() = 0;

    virtual void find(const QString& text, bool forward) = 0;
    virtual void replace(const QString& text, const QString& repl, bool findNext, bool forward) = 0;
    virtual void replaceAll(const QString& text, const QString& repl) = 0;
    virtual void startIncrementalFind() = 0;
    virtual void jumpToLine(int line) = 0;
    virtual void jumpToOffset(int offset) = 0;

    virtual void setThemeName(const QString& name) = 0;
};

}
Q_DECLARE_INTERFACE(Editors::ITextEditor, "Pynoto.ITextEditor")
