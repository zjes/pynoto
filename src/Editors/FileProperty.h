#ifndef _FILEPROPERTY_H_
#define _FILEPROPERTY_H_
#include <QObject>
#include <QSettings>

namespace Editors {

class FileProperty: public QObject
{
    Q_OBJECT
    Q_PROPERTY(int verticalScroll READ verticalScroll WRITE setVerticalScroll)
    Q_PROPERTY(int cursorPos READ cursorPos WRITE setCursorPos)
    Q_PROPERTY(QList<int> closedFolds READ closedFolds WRITE setClosedFolds)
public:
    FileProperty(const QString& fileName);
    virtual ~FileProperty();
public:
    int verticalScroll();
    void setVerticalScroll(int value);
    int cursorPos();
    void setCursorPos(int pos);
    QList<int> closedFolds();
    void setClosedFolds(const QList<int>& folds);
    void save();
private:
    QSettings *_set;
};

}

#endif
