#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QCryptographicHash>
#include "FileProperty.h"

namespace Editors {

FileProperty::FileProperty(const QString& fileName)
{
    QSettings pSet;
    QFileInfo pinfo(pSet.fileName());
    QDir path = pinfo.absoluteDir();
    if (!path.cd("files")){
        path.mkdir("files");
        path.cd("files");
    }
    QFileInfo info(fileName);
    _set = new QSettings(path.filePath(QCryptographicHash::hash(info.absolutePath().toUtf8(), QCryptographicHash::Md5).toHex()+"."+info.fileName()), QSettings::IniFormat);
}

FileProperty::~FileProperty()
{
    delete _set;
}

int FileProperty::verticalScroll()
{
    return _set->value("main/vscroll", 0).toInt();
}

void FileProperty::setVerticalScroll(int value)
{
    _set->setValue("main/vscroll", value);
}


int FileProperty::cursorPos()
{
    return _set->value("main/cursor", 0).toInt();
}

void FileProperty::setCursorPos(int pos)
{
    _set->setValue("main/cursor", pos);
}

void FileProperty::save()
{
    _set->sync();
}

QList<int> FileProperty::closedFolds()
{
    QList<int> lst;
    foreach(QString line, _set->value("main/closedfolds", "").toString().split(";"))
        if (!line.isEmpty())
            lst << line.toInt();
    return lst;
}

void FileProperty::setClosedFolds(const QList<int>& folds)
{
    QStringList lst;
    foreach(int line, folds)
        lst << QString::number(line);
    _set->setValue("main/closedfolds", lst.join(";"));
}

}
