#ifndef _IEDITORPLUGIN_H_
#define _IEDITORPLUGIN_H_
#include "Include/IPlugin.h"

namespace Editors {

class IEditor;

class IEditorPlugin : public IPlugin
{
    Q_OBJECT
public:
    virtual ~IEditorPlugin(){}
    virtual Editors::IEditor * createEditor(const QString& fileName, QWidget* parent, bool sample = false) = 0;
};

}

Q_DECLARE_INTERFACE(Editors::IEditorPlugin, "Pynoto.IEditorPlugin")
#endif
