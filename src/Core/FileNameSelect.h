#ifndef _FILENAMESELECT_H_
#define _FILENAMESELECT_H_
#include <QWidget>
#include <QLineEdit>
#include <QPushButton>

class FileNameSelect: public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(QString caption READ caption WRITE setCaption)
public:
    FileNameSelect(QWidget* parent = NULL);
    virtual ~FileNameSelect();
signals:
    void textChanged();
    void editingFinished();
public slots:
    void setText(const QString& text);
    void setCaption(const QString& text);
    void setDirOnly(bool dirOnly);
    void setDefaultPath(const QString& path);
public:
    QString text();
    QString caption();
private slots:
    void onTextChanged(const QString&);
    void onSelectClicked();
private:
    QLineEdit * _txt;
    QPushButton * _btn;
    QString _caption;
    bool _dirOnly;
    QString _defaultPath;
};

#endif
