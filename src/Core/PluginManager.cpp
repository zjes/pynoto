#include <QDebug>
#include <QCoreApplication>
#include <QLibraryInfo>
#include <QDir>
#include <QPluginLoader>
#include "PluginManager.h"
#include "Include/IPlugin.h"
#include "PynotoApplication.h"

namespace Core {

PluginManager::PluginManager()
{
}

PluginManager::~PluginManager()
{
}

void PluginManager::initPlugins()
{
    QDir pluginsDir(QApplication::applicationDirPath());
    pluginsDir.cd("plugins");

    QStringList pathes;
    pathes << QLibraryInfo::location(QLibraryInfo::LibrariesPath)+"/pynoto/plugins"
           << pluginsDir.path();

    _pluginFiles.clear();
    foreach(QString path, pathes){
        QCoreApplication::addLibraryPath(path);

        QDir pl(path);
        foreach(QFileInfo file, pl.entryInfoList(QStringList()))
        {
            if (!QLibrary::isLibrary(file.absoluteFilePath()))
                continue;
            if (_pluginFiles.contains(file.baseName()))
                continue;
            QString pluginName = file.baseName();
            if (pluginName.startsWith("lib"))
                pluginName = pluginName.right(pluginName.length() - 3);

            QPluginLoader loader(file.absoluteFilePath(), this);
            IPlugin* plugin = qobject_cast<IPlugin*>(loader.instance());
            if (plugin){
                connect(QApplication::instance(), SIGNAL(aboutToQuit()), plugin, SLOT(shutdown()));
                _pluginFiles[pluginName] = plugin;
            } else {
                qDebug() << loader.errorString();
            }
        }
    }
}

IPlugin* PluginManager::plugin(const QString& name)
{
    if (!_pluginFiles.contains(name))
        return NULL;

    if (!_pluginFiles[name]->isInitialized())
        _pluginFiles[name]->initialize();

    return _pluginFiles[name];
}

QList<IPlugin*> PluginManager::plugins()
{
    return _pluginFiles.values();
}


}

