#include <QStringListModel>
#include <QApplication>
#include <QDir>
#include <QCompleter>
#include <QDebug>
#include "FileNameInput.h"


FileNameInput::FileNameInput(QWidget *parent) :
    QLineEdit(parent),
    _path("")
{
    setUnique(true);
    connect(this, SIGNAL(textChanged(QString)), SLOT(onTextChanged(QString)));
    connect(this, SIGNAL(textChanged(QString)), SIGNAL(fileNameChanged(QString)));
}

void FileNameInput::setPath(const QString& path)
{
    _path = path;
    QDir dir(path);
    QStringListModel * model = new QStringListModel(dir.entryList(), this);
    QCompleter *comp = new QCompleter;
    comp->setModel(model);
    setCompleter(comp);
}

void FileNameInput::onTextChanged(const QString& /*text*/)
{
    if (_unique && completer()){
        QPalette pal = palette();
        if (qobject_cast<QStringListModel*>(completer()->model())->stringList().contains(fileName())){
            pal.setColor(QPalette::Text, Qt::red);
            emit valid(false);
        } else {
            pal.setColor(QPalette::Text, QApplication::palette().color(QPalette::Text));
            emit valid(true);
        }
        setPalette(pal);
    }
}

void FileNameInput::setUnique(bool uniq)
{
    _unique = uniq;
    setValidator(uniq ? new FileExistsValidator(this) : 0);
}

QString	FileNameInput::fileName() const
{
    QString txt = text();
    if (_defExt.isEmpty())
        return txt;

    if (txt.isEmpty())
        return txt;

    QFileInfo fi(txt);
    if (fi.suffix().isEmpty()){
        if (txt.endsWith("."))
            return txt+_defExt;
        return txt+"."+_defExt;
    }
    return txt;
}

void FileNameInput::setFileName(const QString& fileName)
{
    setText(fileName);
}

void FileNameInput::setDefaultExtension(const QString& extension)
{
    _defExt = extension;
}


FileExistsValidator::FileExistsValidator(QWidget * parent):
    QValidator(parent)
{

}

QValidator::State FileExistsValidator::validate(QString & /*input*/, int & /*pos*/) const
{
    FileNameInput * finput = qobject_cast<FileNameInput*>(parent());
    if (finput && finput->completer()){
        if (qobject_cast<QStringListModel*>(finput->completer()->model())->stringList().contains(finput->fileName()))
            return QValidator::Intermediate;
    }
    return QValidator::Acceptable;
}

