#include <QDebug>
#include "IPlugin.h"

IPlugin::IPlugin():
    QObject(),
    initialized(false)
{
}

IPlugin::~IPlugin()
{
}

void IPlugin::shutdown()
{
    deleteLater();
}

bool IPlugin::isInitialized()
{
    return initialized;
}

QList<IPreferencesPage*> IPlugin::preferences()
{
    return QList<IPreferencesPage*>();
}

bool IPlugin::readSettings()
{
    return false;
}

bool IPlugin::saveSettings()
{
    return false;
}
