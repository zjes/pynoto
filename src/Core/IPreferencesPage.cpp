#include "Include/Aux.h"
#include "IPreferencesPage.h"


IPreferencesPage::IPreferencesPage(QWidget * parent):
    QWidget(parent)
{
    connect(this, SIGNAL(changed(QString)), Aux::app(), SIGNAL(preferencesChanged(QString)));
}

int IPreferencesPage::order()
{
    return 0;
}

