#ifndef _IPREFERENCESPAGE_H_
#define _IPREFERENCESPAGE_H_
#include <QWidget>

class IPreferencesPage: public QWidget
{
    Q_OBJECT
public:
    IPreferencesPage(QWidget * parent);
    virtual ~IPreferencesPage(){}
public:
    virtual QString title() = 0;
    virtual QIcon icon() = 0;
    virtual bool save() = 0;
    virtual int order();
signals:
    void changed();
    void changed(const QString& what);
};


#endif
