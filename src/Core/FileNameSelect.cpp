#include <QDebug>
#include <QHBoxLayout>
#include <QFileSystemModel>
#include <QCompleter>
#include <QFileDialog>
#include "FileNameSelect.h"

FileNameSelect::FileNameSelect(QWidget* parent):
    QWidget(parent),
    _caption(tr("Select file")),
    _dirOnly(false),
    _defaultPath(QDir::homePath())
{
    setLayout(new QHBoxLayout);
    _txt = new QLineEdit(this);
    _btn = new QPushButton(tr("..."), this);
    layout()->setContentsMargins(0, 0, 0, 0);
    layout()->addWidget(_txt);
    layout()->addWidget(_btn);

    connect(_txt, SIGNAL(textChanged(QString)), SLOT(onTextChanged(QString)));
    connect(_txt, SIGNAL(editingFinished()), SIGNAL(editingFinished()));
    QFileSystemModel *model = new QFileSystemModel(this);
    model->setRootPath("/");
    QCompleter *comp = new QCompleter(this);
    comp->setModel(model);
    _txt->setCompleter(comp);

    connect(_btn, SIGNAL(clicked()), SLOT(onSelectClicked()));
}

FileNameSelect::~FileNameSelect()
{

}

void FileNameSelect::setText(const QString& text)
{
    _txt->setText(text);
    qobject_cast<QFileSystemModel *>(_txt->completer()->model())->setRootPath(text);
    emit textChanged();
}

QString FileNameSelect::text()
{
    return _txt->text();
}

void FileNameSelect::setCaption(const QString& caption)
{
    _caption = caption;
}

QString FileNameSelect::caption()
{
    return _caption;
}

void FileNameSelect::onTextChanged(const QString&)
{
    emit textChanged();
}

void FileNameSelect::onSelectClicked()
{
    QFileInfo info(_txt->text());
    if (!_dirOnly){
        QString file = QFileDialog::getOpenFileName(this, _caption, _txt->text().isEmpty() ? _defaultPath : info.absoluteDir().path());
        if (!file.isEmpty())
            _txt->setText(file);
    } else {
        QFileDialog dlg(this);
        dlg.setFileMode(QFileDialog::DirectoryOnly);
        dlg.setWindowTitle(_caption);
        dlg.setDirectory(_txt->text().isEmpty() ? _defaultPath : info.absoluteFilePath());
        if (dlg.exec() == QDialog::Accepted)
            _txt->setText(dlg.directory().absolutePath());
    }
}

void FileNameSelect::setDirOnly(bool dirOnly)
{
    _dirOnly = dirOnly;
    _caption = _dirOnly ? tr("Select directory") : tr("Select file");
}

void FileNameSelect::setDefaultPath(const QString& path)
{
    _defaultPath = path;
    qobject_cast<QFileSystemModel *>(_txt->completer()->model())->setRootPath(path);
}
