#ifndef _PYNOTOAPPLICATION_H_
#define _PYNOTOAPPLICATION_H_

#include <QApplication>
#include "Include/PluginManager.h"
#include "Include/IIconProvider.h"

class PynotoApplication: public QApplication
{
    Q_OBJECT
public:
    PynotoApplication(int &argc, char **argv, int = ApplicationFlags);
    virtual ~PynotoApplication();
    void emitPreferencesChanged(const QString& name);
    void emitPreferencesChanged(const QString& name, const QVariant& value);
signals:
    void preferencesChanged(const QString& what);
    void preferencesChanged(const QString& what, const QVariant& value);
public:
    Core::PluginManager *pluginManager;
    IconProvider::IIconProvider *iconProvider;
};

#endif
