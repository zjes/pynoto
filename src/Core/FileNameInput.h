#ifndef FILENAMEINPUT_H
#define FILENAMEINPUT_H

#include <QLineEdit>
#include <QValidator>

class FileExistsValidator: public QValidator
{
public:
    explicit FileExistsValidator(QWidget * parent = 0);
    virtual QValidator::State validate(QString & input, int & pos) const;
};

class FileNameInput : public QLineEdit
{
    Q_OBJECT
    Q_PROPERTY(QString fileName READ fileName WRITE setFileName NOTIFY fileNameChanged USER true)
public:
    explicit FileNameInput(QWidget *parent = 0);
signals:
    void fileNameChanged(const QString&);
    void valid(bool);
public:
    void setPath(const QString& path);
    void setUnique(bool uniq);
    void setDefaultExtension(const QString& extension);
    QString	fileName() const;
    void setFileName(const QString& fileName);
private slots:
    void onTextChanged(const QString& text);
private:
    QString _path;
    bool _unique;
    QString _defExt;
};

#endif // FILENAMEINPUT_H
