#include "Include/PynotoApplication.h"

namespace Aux {

IconProvider::IIconProvider* icons()
{
    return static_cast<PynotoApplication*>(QApplication::instance())->iconProvider;
}

Core::PluginManager * manager()
{
    return static_cast<PynotoApplication*>(QApplication::instance())->pluginManager;
}

PynotoApplication* app()
{
    return static_cast<PynotoApplication*>(QApplication::instance());
}

void emitPreferencesChanged(const QString& name)
{
    app()->emitPreferencesChanged(name);
}

void emitPreferencesChanged(const QString& name, const QVariant& value)
{
    app()->emitPreferencesChanged(name, value);
}

}


