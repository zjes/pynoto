#include "PynotoApplication.h"

PynotoApplication::PynotoApplication(int &argc, char **argv, int flag):
    QApplication(argc, argv, flag)
{
    pluginManager = new Core::PluginManager();
    pluginManager->initPlugins();

    iconProvider = pluginManager->plugin<IconProvider::IIconProvider*>("iconprovider");
}

PynotoApplication::~PynotoApplication()
{
    delete pluginManager;
}

void PynotoApplication::emitPreferencesChanged(const QString& name)
{
    emit preferencesChanged(name);
}

void PynotoApplication::emitPreferencesChanged(const QString& name, const QVariant& value)
{
    emit preferencesChanged(name, value);
}

