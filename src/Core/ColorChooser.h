#ifndef _COLORCHOOSER_H_
#define _COLORCHOOSER_H_
#include <QToolButton>

class ColorChooser: public QToolButton
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor)
public:
    ColorChooser(QWidget* parent);
    virtual ~ColorChooser();

    QColor color();
    void setColor(const QColor& color);
    void setDefaultColor(const QColor& color);
public slots:
    void colorReset();
    void showDialog();
signals:
    void changed(const QColor& color);
protected:
    virtual void paintEvent(QPaintEvent *);
    virtual QSize sizeHint() const;
private:
    bool isLight();
private:
    QColor _color;
    QColor _defaultColor;
};

#endif
