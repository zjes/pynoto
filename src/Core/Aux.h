#ifndef _AUX_H_
#define _AUX_H_

#include "PynotoApplication.h"

namespace Aux {

template<class T>
T plugin(const QString& name)
{
    return static_cast<PynotoApplication*>(QApplication::instance())->pluginManager->plugin<T>(name);
}

IconProvider::IIconProvider* icons();
Core::PluginManager * manager();
PynotoApplication* app();
void emitPreferencesChanged(const QString& name);
void emitPreferencesChanged(const QString& name, const QVariant& value);

}

#endif
