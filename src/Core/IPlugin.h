#ifndef _IPLUGIN_H_
#define _IPLUGIN_H_
#include <QObject>
#include <QSettings>

class IPreferencesPage;

class Q_DECL_EXPORT IPlugin: public QObject
{
    Q_OBJECT
public:
    IPlugin();
    virtual ~IPlugin();
    virtual bool initialize() = 0;
    virtual bool isInitialized();
    virtual QList<IPreferencesPage*> preferences();

    virtual bool readSettings();
    virtual bool saveSettings();
public slots:
    void shutdown();
protected:
    bool initialized;
};

#endif
