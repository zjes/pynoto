#ifndef _PLUGINMANAGER_H_
#define _PLUGINMANAGER_H_
#include <QMap>
#include <QDebug>
#include <QFileInfo>
#include <QSharedPointer>

class IPlugin;

namespace Core {

class Q_DECL_EXPORT PluginManager : public QObject
{
    Q_OBJECT
public:
    PluginManager();

    virtual ~PluginManager();

    void initPlugins();
    IPlugin* plugin(const QString& name);

    template <class T>
    T plugin(const QString& name)
    {
        return qobject_cast<T>(plugin(name));
    }
    QList<IPlugin*> plugins();
private:
    QMap<QString, IPlugin*> _pluginFiles;
};

}
#endif
