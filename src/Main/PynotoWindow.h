#ifndef _PYNOTOWINDOW_H_
#define _PYNOTOWINDOW_H_

#include <QMainWindow>
#include <QSplitter>
#include <QLabel>

namespace Ui {
    class PynotoWindow;
}

class QToolButton;
class QButtonGroup;

namespace IconProvider{ class IIconProvider; }
namespace Project { class IProjectPlugin; }
namespace EditorsManager { class IEditorsManagerPlugin; }

namespace Main {

class ConsoleWidget;

class PynotoWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit PynotoWindow(QWidget *parent = 0);
    virtual ~PynotoWindow();
protected:
    void loadPlugins();
    void setAction(QAction *action, const char *slot, const QString & iconName = "");
    void setAction(QWidget * reciver, QAction *action, const char *slot, const QString & iconName = "");
    virtual void closeEvent(QCloseEvent *);
private:
    void updateProjectMenu(bool);
    QToolButton * createStatusBtn(const QString& text, const QString& icon, int consoleNum);
private slots:
    void onProjectLoaded();
    void onProjectUnloaded();

    void restoreSession();
    void openProject();
    void closeProject();
    void showPreferences();
    void runProject();
    void configureProject();
    void findInProject();
    void newProject();
    void resetEditor();
    void consoleChanged(int);
private:
    Ui::PynotoWindow *ui;
    IconProvider::IIconProvider * _iconProvider;
    Project::IProjectPlugin * _project;
    EditorsManager::IEditorsManagerPlugin *_editors;
    ConsoleWidget * _console;
    QSplitter * _mSplit, * _pSplit, * _split;
    QLabel *_title;
    QButtonGroup *_statusGroup;
};

}
#endif
