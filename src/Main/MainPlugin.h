#ifndef _COREPLUGIN_H_
#define _COREPLUGIN_H_

#include "Include/IMainPlugin.h"

namespace Main
{

class PynotoWindow;

class MainPlugin: public IMainPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IMainPlugin")
    Q_INTERFACES(Main::IMainPlugin)
public:
    MainPlugin();
    virtual ~MainPlugin();
    virtual bool initialize();
    virtual QList<IPreferencesPage*> preferences();
private:
    PynotoWindow * _mainWnd;
};

}
#endif
