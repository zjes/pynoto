#ifndef _ICORE_H_
#define _ICORE_H_

#include "Include/IPlugin.h"

namespace Main {

class IMainPlugin: public IPlugin
{
    Q_OBJECT
public:
    virtual ~IMainPlugin(){}
};

}
Q_DECLARE_INTERFACE(Main::IMainPlugin, "Pynoto.IMainPlugin")
#endif
