#include <QtPlugin>
#include <QApplication>
#include "MainPlugin.h"
#include "PynotoWindow.h"
#include "Preferences/GeneralPrefs.h"
//#include "Preferences.h"

namespace Main {

MainPlugin::MainPlugin():
    IMainPlugin(),
    _mainWnd(NULL)
{

}

MainPlugin::~MainPlugin()
{
    delete _mainWnd;
}

bool MainPlugin::initialize()
{
    _mainWnd = new PynotoWindow();
    _mainWnd->show();
    initialized = true;
    return true;
}


QList<IPreferencesPage*> MainPlugin::preferences()
{
    return QList<IPreferencesPage*>() << new GeneralPrefs;
}
}

