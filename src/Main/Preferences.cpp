#include <QApplication>
#include <QIcon>
#include "Preferences.h"


namespace Main {

Preferences::~Preferences()
{

}

QString Preferences::title()
{
    return QApplication::tr("Environment", "Preferences");
}

QIcon Preferences::icon()
{
    return QIcon();
}

QWidget* Preferences::widget(QWidget * parent)
{
    return NULL;
}

bool Preferences::apply()
{
    return false;
}

}
