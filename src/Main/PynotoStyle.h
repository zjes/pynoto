#ifndef _PYNOTOSTYLE_H_
#define _PYNOTOSTYLE_H_
#include <QProxyStyle>

class QStyleOptionToolButton;
class PynotoStyle: public QProxyStyle
{
    Q_OBJECT
public:
    PynotoStyle(QStyle *baseStyle = 0);
    virtual void polish(QWidget *widget);
    virtual void drawPrimitive(PrimitiveElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const;
    virtual void drawComplexControl(ComplexControl control, const QStyleOptionComplex *option, QPainter *painter, const QWidget *widget) const;
    virtual void drawControl(ControlElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const;
    virtual int pixelMetric(PixelMetric metric, const QStyleOption *option, const QWidget *widget) const;
    virtual QSize sizeFromContents(ContentsType type, const QStyleOption *option, const QSize &size, const QWidget *widget) const;
private:
    bool isPanel(const QWidget* widget) const;
    QPalette panelPalette(const QPalette &oldPalette, bool lightColored = false);
    void drawButtonSeparator(QPainter *painter, const QRect &rect, bool reverse, bool lr = false) const;
    void drawFancyToolButton(const QStyleOptionToolButton* option, QPainter* painter, const QWidget* widget) const;
    void createPanelImage();
    bool isLight();
private slots:
    void onPreferencesChanged(const QString& name, const QVariant& value);
    void resetStyle();
private:
    QPixmap _panel;
    QPixmap _pbtn, _pbtnHover, _pbtnPressed;
    QIcon _arrowDown;
    QColor _hover;
    QColor _panelColor;
};

#endif
