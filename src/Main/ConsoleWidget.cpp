#include <QDebug>
#include <QTableView>

#include "Consoles/IssuesConsole.h"
#include "Consoles/ApplicationConsole.h"
#include "Consoles/LogConsole.h"
#include "Consoles/SearchResultConsole.h"
#include "Include/Aux.h"
#include "Include/IIconProvider.h"
#include "ConsoleWidget.h"
#include "ui_ConsoleWidget.h"
#include "Consoles/IApplicationOutput.h"

namespace Main {

ConsoleWidget::ConsoleWidget(QWidget *parent):
    QWidget(parent),
    _ui(new Ui::ConsoleWidget),
    _issues(new IssuesConsole(this)),
    _application(new ApplicationConsole(this)),
    _log(new LogConsole(this)),
    _searchResult(new SearchResultConsole(this)),
    _mode(-1)
{
    _ui->setupUi(this);
    _ui->stack->setFrameStyle(QFrame::NoFrame);
    _ui->modes->addItem(tr("Issues"));
    _ui->modes->addItem(tr("Application output"));
    _ui->modes->addItem(tr("Search result"));
    _ui->modes->addItem(tr("System log"));

    _ui->hideBtn->setIcon(Aux::icons()->icon("hide"));
    connect(_ui->hideBtn, SIGNAL(clicked()), SLOT(hide()));
    connect(_ui->modes, SIGNAL(currentIndexChanged(int)), SLOT(switchTo(int)));

    _ui->stack->addWidget(_issues->control());
    _ui->stack->addWidget(_application->control());
    _ui->stack->addWidget(_searchResult->control());
    _ui->stack->addWidget(_log->control());
    setVisible(false);
}

ConsoleWidget::~ConsoleWidget()
{
    delete _ui;
}

IssuesConsole * ConsoleWidget::issues() const
{
    return _issues;
}

IApplicationOutput * ConsoleWidget::application() const
{
    return _application;
}

ISearchResultConsole * ConsoleWidget::searchResult() const
{
    return _searchResult;
}

void ConsoleWidget::setup(EditorsManager::IEditorsManagerPlugin* editors, QButtonGroup * btns)
{
    _buttonGroup = btns;
    connect(editors, SIGNAL(errorListChanged(QAbstractTableModel*)), SLOT(errorListChanged(QAbstractTableModel*)));
    connect(_issues, SIGNAL(refresh()), editors, SLOT(checkIssues()));
    connect(_issues, SIGNAL(activated(int)), editors, SLOT(jumpCurrentFileLine(int)));
    connect(_searchResult, SIGNAL(activated()), SLOT(searchActivate()));
}


void ConsoleWidget::errorListChanged(QAbstractTableModel* model)
{
    _buttonGroup->button(0)->setText(model && model->rowCount() ? tr("Issues (%1)").arg(model->rowCount()) : tr("Issues"));
    _issues->setModel(model);
    //switchTo(0);
}

void ConsoleWidget::searchActivate()
{
    switchTo(2);
}

void ConsoleWidget::switchTo(int mode)
{
    if (mode != _mode){
        _ui->modes->blockSignals(true);
        _ui->modes->setCurrentIndex(mode);
        _ui->modes->blockSignals(false);
        _ui->stack->setCurrentIndex(mode);

        QLayoutItem * item;
        do {
            item = _ui->headerPlace->layout()->takeAt(0);
            if (item){
                _ui->headerPlace->layout()->removeWidget(item->widget());
                item->widget()->hide();
                delete item;
            }
        } while (item);

        if (mode == 0){
            _ui->headerPlace->layout()->addWidget(_issues->header());
            _issues->header()->show();
        } else if (mode == 1) {
            _ui->headerPlace->layout()->addWidget(_application->header());
            _application->header()->show();
        } else if (mode == 2) {
            _ui->headerPlace->layout()->addWidget(_searchResult->header());
            _searchResult->header()->show();
        } else if (mode == 3) {
            _ui->headerPlace->layout()->addWidget(_log->header());
            _log->header()->show();
        }

        _mode = mode;
        emit consoleChanged(mode);
    }
    setVisible(true);
}

}

