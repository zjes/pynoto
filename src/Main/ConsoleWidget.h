#ifndef _CONSOLEWIDGET_H_
#define _CONSOLEWIDGET_H_

#include <QStackedWidget>
#include "Include/IEditorsManagerPlugin.h"

class SearchResult;
class QAbstractTableModel;
class QButtonGroup;

namespace Ui {
    class ConsoleWidget;
}

namespace Main {

class IssuesConsole;
class ApplicationConsole;
class IApplicationOutput;
class LogConsole;
class SearchResultConsole;
class ISearchResultConsole;

class ConsoleWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ConsoleWidget(QWidget *parent = 0);
    virtual ~ConsoleWidget();

    IssuesConsole * issues() const;
    IApplicationOutput * application() const;
    ISearchResultConsole * searchResult() const;

    void setup(EditorsManager::IEditorsManagerPlugin* editors, QButtonGroup * btns);
public slots:
    void errorListChanged(QAbstractTableModel*);
    void switchTo(int mode);
    void searchActivate();
signals:
    void consoleChanged(int);
private:
    Ui::ConsoleWidget *_ui;
    IssuesConsole * _issues;
    ApplicationConsole * _application;
    LogConsole * _log;
    SearchResultConsole *_searchResult;

    QButtonGroup * _buttonGroup;
    int _mode;
};

}

#endif
