#ifndef _GENERALPREFS_H_
#define _GENERALPREFS_H_
#include "Include/IPreferencesPage.h"

namespace Ui {
    class GeneralPrefs;
}

namespace Main {

class GeneralPrefs: public IPreferencesPage {
    Q_OBJECT
public:
    GeneralPrefs(QWidget *parent = NULL);
    virtual ~GeneralPrefs();

    virtual QString title();
    virtual QIcon icon();
    virtual bool save();
private:
    void init();
private slots:
    void onProjectChanged(bool);
private:
    Ui::GeneralPrefs *_ui;
};

}

#endif
