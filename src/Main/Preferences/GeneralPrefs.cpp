#include "Settings.h"
#include "GeneralPrefs.h"
#include "ui_GeneralPrefs.h"

namespace Main {

GeneralPrefs::GeneralPrefs(QWidget *parent):
    IPreferencesPage(parent),
    _ui(new Ui::GeneralPrefs)
{
    _ui->setupUi(this);
    init();
    connect(_ui->chkRestoreProject, SIGNAL(toggled(bool)), SLOT(onProjectChanged(bool)));
}

GeneralPrefs::~GeneralPrefs()
{
    delete _ui;
}

QString GeneralPrefs::title()
{
    return tr("General");
}

QIcon GeneralPrefs::icon()
{
    return QIcon();
}

void GeneralPrefs::onProjectChanged(bool toggled)
{
    _ui->chkRestoreFiles->setEnabled(toggled);
    if (!toggled)
        _ui->chkRestoreFiles->setChecked(false);
}

void GeneralPrefs::init()
{
    Settings set;
    _ui->defColor->setColor(set.color());
    _ui->chkRestoreFiles->setChecked(set.restoreFiles());
    _ui->chkRestoreProject->setChecked(set.restoreProject());
}

bool GeneralPrefs::save()
{
    Settings set;
    connect(&set, SIGNAL(colorChanged()), SIGNAL(changed()));
    set.setColor(_ui->defColor->color());
    set.setRestoreFiles(_ui->chkRestoreFiles->isChecked());
    set.setRestoreProject(_ui->chkRestoreProject->isChecked());

    return true;
}

}
