#ifndef _SETTINGS_H_
#define _SETTINGS_H_
#include <QSettings>
#include <QColor>
#include <QStringList>

namespace Main {

class Settings: public QSettings
{
    Q_OBJECT
    Q_PROPERTY(QStringList recentProjects READ recentProjects WRITE setRecentProjects)
    Q_PROPERTY(QString lastProject READ lastProject WRITE setLastProject)
    Q_PROPERTY(QByteArray editorSplitSize READ editorSplitSize WRITE setEditorSplitSize)
    Q_PROPERTY(QByteArray leftSplitSize READ leftSplitSize WRITE setLeftSplitSize)
    Q_PROPERTY(QByteArray workSplitSize READ workSplitSize WRITE setWorkSplitSize)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(bool restoreProject READ restoreProject WRITE setRestoreProject)
    Q_PROPERTY(bool restoreFiles READ restoreFiles WRITE setRestoreFiles)
signals:
    void colorChanged();
public:
    QStringList recentProjects();
    void setRecentProjects(const QStringList& projects);

    QString lastProject();
    void setLastProject(const QString& project);

    QByteArray editorSplitSize();
    void setEditorSplitSize(const QByteArray& sizes);

    QByteArray leftSplitSize();
    void setLeftSplitSize(const QByteArray& sizes);

    QByteArray workSplitSize();
    void setWorkSplitSize(const QByteArray& sizes);

    QColor color();
    void setColor(const QColor& color);

    bool restoreFiles();
    void setRestoreFiles(bool restore);

    bool restoreProject();
    void setRestoreProject(bool restore);
};

}
#endif
