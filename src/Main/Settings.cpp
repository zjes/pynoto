#include <QDebug>
#include "Settings.h"


namespace Main {

QStringList Settings::recentProjects()
{
    QStringList out;
    int count = beginReadArray("main/recentprojects");
    for(int i = 0; i < count; ++i){
        setArrayIndex(i);
        out.append(value("project").toString());
    }
    endArray();
    return out;
}

void Settings::setRecentProjects(const QStringList& projects)
{
    beginWriteArray("main/recentprojects");
    int cnt = 0;
    foreach(QString prj, projects){
        setArrayIndex(cnt);
        setValue("project", prj);
        cnt++;
    }
    endArray();
}

QString Settings::lastProject()
{
    return value("main/last-project").toString();
}

void Settings::setLastProject(const QString& project)
{
    setValue("main/last-project", project);
    QStringList prs = recentProjects();
    if (prs.contains(project))
        prs.removeOne(project);
    prs.prepend(project);
    setRecentProjects(prs);
}


QByteArray Settings::editorSplitSize()
{
    return value("layout/editorsplitsize", QByteArray::fromHex("000000ff0000000000000002000001f50000007d01000000010100000002")).toByteArray();
}

void Settings::setEditorSplitSize(const QByteArray& sizes)
{
    setValue("layout/editorsplitsize", sizes);
}

QByteArray Settings::leftSplitSize()
{
    return value("layout/leftsplitsize", QByteArray::fromHex("000000ff000000000000000200000192000000e001000000010100000002")).toByteArray();
}

void Settings::setLeftSplitSize(const QByteArray& sizes)
{
    setValue("layout/leftsplitsize", sizes);
}

QByteArray Settings::workSplitSize()
{
    return value("layout/worksplitsize", QByteArray::fromHex("000000ff0000000000000002000000db000002e201000000010100000001")).toByteArray();
}

void Settings::setWorkSplitSize(const QByteArray& sizes)
{
    setValue("layout/worksplitsize", sizes);
}

QColor Settings::color()
{
    return QColor(value("main/color", "#000000").toString());
}

void Settings::setColor(const QColor& col)
{
    if (color().name() == col.name())
        return;
    setValue("main/color", col.name());
    emit colorChanged();
}

bool Settings::restoreFiles()
{
    return value("main/restorefiles", true).toBool();
}

void Settings::setRestoreFiles(bool restore)
{
    setValue("main/restorefiles", restore);
}

bool Settings::restoreProject()
{
    return value("main/restoreproject", true).toBool();
}

void Settings::setRestoreProject(bool restore)
{
    setValue("main/restoreproject", restore);
}

}
