#include "IssuesConsoleHeader.h"
#include "Include/Aux.h"
#include "Include/IIconProvider.h"
#include "ui_IssuesConsoleHeader.h"

namespace Main {

IssuesConsoleHeader::IssuesConsoleHeader(QWidget * parent):
    QWidget(parent),
    _ui(new Ui::IssuesConsoleHeader)
{
    _ui->setupUi(this);
    _ui->refreshBtn->setIcon(Aux::icons()->icon("reset"));
    connect(_ui->refreshBtn, SIGNAL(clicked()), SIGNAL(refresh()));
}

IssuesConsoleHeader::~IssuesConsoleHeader()
{
    delete _ui;
}

}
