#ifndef _LOGCONSOLEHEADER_H_
#define _LOGCONSOLEHEADER_H_
#include <QWidget>

namespace Ui { class LogConsoleHeader; }
namespace Main {

class LogConsoleHeader: public QWidget
{
    Q_OBJECT
public:
    LogConsoleHeader(QWidget * parent = NULL);
    virtual ~LogConsoleHeader();
signals:
    void clearConsole();
private:
    Ui::LogConsoleHeader *_ui;
};

}

#endif
