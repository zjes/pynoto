#ifndef _APPLICATIONCONSOLE_H_
#define _APPLICATIONCONSOLE_H_

#include <QWidget>
#include "IApplicationOutput.h"


namespace Main {

class ApplicationOutput;
class ApplicationConsoleHeader;


class ApplicationConsole : public IApplicationOutput
{
    Q_OBJECT
public:
    explicit ApplicationConsole(QWidget *parent = 0);
    QWidget * header();
    QWidget * control();
public slots:
    virtual void projectRunned();
    virtual void projectStoped();

    virtual void addOutput(const QString& msg);
    virtual void addError(const QString& msg);
private:
    ApplicationOutput * _output;
    ApplicationConsoleHeader * _header;
};

}

#endif
