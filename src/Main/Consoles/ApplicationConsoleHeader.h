#ifndef _APPLICATIONCONSOLEHEADER_H_
#define _APPLICATIONCONSOLEHEADER_H_

#include <QWidget>

namespace Ui {
    class ApplicationConsoleHeader;
}

namespace Main {

class ApplicationConsoleHeader: public QWidget
{
    Q_OBJECT
public:
    ApplicationConsoleHeader(QWidget * parent = 0);
    virtual ~ApplicationConsoleHeader();
    void projectRunned(bool runned);
signals:
    void run();
    void stop();
private:
    Ui::ApplicationConsoleHeader *_ui;
};

}

#endif
