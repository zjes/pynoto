#include <QDebug>
#include <QVBoxLayout>
#include "ApplicationConsole.h"
#include "ApplicationOutput.h"
#include "ApplicationConsoleHeader.h"

namespace Main {

ApplicationConsole::ApplicationConsole(QWidget *parent) :
    IApplicationOutput(parent),
    _output(new ApplicationOutput(this)),
    _header(new ApplicationConsoleHeader(this))
{
    connect(_header, SIGNAL(run()), SIGNAL(run()));
    connect(_header, SIGNAL(stop()), SIGNAL(stop()));
    connect(_output, SIGNAL(jumpToFile(QString,int)), this, SIGNAL(jumpToFile(QString, int)));
    projectStoped();
}

QWidget * ApplicationConsole::header()
{
    return _header;
}

QWidget * ApplicationConsole::control()
{
    return _output;
}

void ApplicationConsole::projectRunned()
{
    _header->projectRunned(true);
    _output->clear();
}

void ApplicationConsole::projectStoped()
{
    _header->projectRunned(false);
}

void ApplicationConsole::addOutput(const QString& msg)
{
    _output->addOutput(msg);
}

void ApplicationConsole::addError(const QString& msg)
{
    _output->addError(msg);
}


}

