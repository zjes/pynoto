#ifndef _SEARCHRESULTCONSOLE_H_
#define _SEARCHRESULTCONSOLE_H_

#include <QWidget>
#include "ISearchResultConsole.h"

namespace Main {
class SearchResultHeader;
class SearchResultConsole : public ISearchResultConsole
{
    Q_OBJECT
public:
    explicit SearchResultConsole(QWidget *parent = 0);
    QWidget * header();
    QWidget * control();
    virtual void setResult(const QList<FoundItems>& items);
signals:
    void activated();
private:
    QWidget *_result;
    SearchResultHeader *_header;
};

}
#endif
