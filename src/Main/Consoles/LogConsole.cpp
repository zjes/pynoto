#include "LogConsole.h"
#include "LogConsoleHeader.h"
#include "LogOutput.h"

namespace Main {

LogConsole::LogConsole(QWidget *parent):
    QWidget(parent),
    _header(new LogConsoleHeader(this)),
    _output(new LogOutput(this))
{
    connect(_header, SIGNAL(clearConsole()), _output, SLOT(clearLog()));
}

QWidget * LogConsole::header()
{
    return _header;
}

QWidget * LogConsole::control()
{
    return _output;
}

}
