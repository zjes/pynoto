#ifndef _ISSUESCONSOLE_H_
#define _ISSUESCONSOLE_H_

#include <QWidget>

class QAbstractItemModel;

namespace Main {

class IssuesTable;
class IssuesConsoleHeader;

class IssuesConsole : public QWidget
{
    Q_OBJECT
public:
    explicit IssuesConsole(QWidget *parent = 0);
    void setModel(QAbstractItemModel *model);
    QWidget * header();
    QWidget * control();
signals:
    void activated(int);
    void refresh();
private:
    IssuesTable * _table;
    IssuesConsoleHeader * _header;
};

}

#endif
