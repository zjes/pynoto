#include <QDebug>
#include "LogOutput.h"
#include "Include/Aux.h"
#include "Include/IProjectPlugin.h"
#include "Include/IPythonCode.h"

namespace Main {

LogOutput::LogOutput(QWidget* parent):
    QPlainTextEdit(parent)
{
    setFrameStyle(QFrame::NoFrame);
    Project::IProject *prj = Aux::plugin<Project::IProjectPlugin*>("project")->project();
    if (prj)
        connect(prj, SIGNAL(systemError(QString)), SLOT(appendLog(QString)));
}

LogOutput::~LogOutput()
{
}

void LogOutput::clearLog()
{
    clear();
}

void LogOutput::appendLog(const QString& log)
{
    appendPlainText(log);
}

}
