#include <QDebug>
#include <QHeaderView>
#include <QShowEvent>
#include <QTimer>
#include "IssuesTable.h"

namespace Main {

IssuesTable::IssuesTable(QWidget *parent) :
    QTableView(parent)
{
    verticalHeader()->hide();
    setFrameStyle(QFrame::NoFrame);
    setSelectionMode(QAbstractItemView::SingleSelection);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    //horizontalHeader()->setStretchLastSection(true);
    QFont fnt("Monospace");
    fnt.setStyleHint(QFont::TypeWriter);
    setFont(fnt);
}

void IssuesTable::setModel(QAbstractItemModel *newModel)
{
    if (newModel == model())
        return;
    QTableView::setModel(newModel);

    setColumnWidth(0, 25);
    setColumnWidth(1, width() - 100);
    setColumnWidth(2, 50);
    QTimer::singleShot(10, this, SLOT(resizeRowsToContents()));
}

void IssuesTable::currentChanged(const QModelIndex & current, const QModelIndex & /*previous*/)
{
    emit activated(current.data(Qt::EditRole).toInt());
}

void IssuesTable::resizeEvent(QResizeEvent *event)
{
    setColumnWidth(0, 25);
    setColumnWidth(1, width() - 100);
    setColumnWidth(2, 50);
    QTableView::resizeEvent(event);
}

}
