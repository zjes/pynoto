#ifndef _ISEARCHRESULTCONSOLE_H_
#define _ISEARCHRESULTCONSOLE_H_
#include <QWidget>

namespace Main {

struct FoundItems
{
    QString file;
    QString str;
    int line;
};

class ISearchResultConsole: public QWidget
{
    Q_OBJECT
public:
    explicit ISearchResultConsole(QWidget *parent = 0);
    virtual ~ISearchResultConsole();

    virtual void setResult(const QList<FoundItems>& items) = 0;
};

}
#endif
