#ifndef _IAPPLICATIONOUTPUT_H_
#define _IAPPLICATIONOUTPUT_H_

#include <QWidget>

namespace Main {

class IApplicationOutput: public QWidget
{
    Q_OBJECT
public:
    IApplicationOutput(QWidget * parent = 0);
    virtual ~IApplicationOutput();
public slots:
    virtual void projectRunned() = 0;
    virtual void projectStoped() = 0;

    virtual void addOutput(const QString& msg) = 0;
    virtual void addError(const QString& msg) = 0;
signals:
    void run();
    void stop();
    void jumpToFile(const QString& fileName, int line = 0);
};

}

#endif
