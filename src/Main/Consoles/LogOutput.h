#ifndef _LOGOUTPUT_H_
#define _LOGOUTPUT_H_
#include <QPlainTextEdit>

namespace Main {

class LogOutput: public QPlainTextEdit
{
    Q_OBJECT
public:
    LogOutput(QWidget* parent = NULL);
    virtual ~LogOutput();
public slots:
    void appendLog(const QString& log);
public slots:
    void clearLog();
};

}

#endif
