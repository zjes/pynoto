#include "SearchResultConsole.h"
#include "SearchResultHeader.h"

namespace Main {

SearchResultConsole::SearchResultConsole(QWidget *parent) :
    ISearchResultConsole(parent),
    _result(new QWidget(this)),
    _header(new SearchResultHeader(this))
{
}

QWidget * SearchResultConsole::header()
{
    return _header;
}

QWidget * SearchResultConsole::control()
{
    return _result;
}

void SearchResultConsole::setResult(const QList<FoundItems>& items)
{
    _header->setMatchCount(items.length());
    emit activated();
}


}
