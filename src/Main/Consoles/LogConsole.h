#ifndef _LOGCONSOLE_H_
#define _LOGCONSOLE_H_

#include <QWidget>


namespace Main {

class LogOutput;
class LogConsoleHeader;

class LogConsole : public QWidget
{
    Q_OBJECT
public:
    explicit LogConsole(QWidget *parent = 0);
    QWidget * header();
    QWidget * control();
private:
    LogConsoleHeader * _header;
    LogOutput * _output;
};

}

#endif
