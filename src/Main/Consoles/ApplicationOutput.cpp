#include <QDebug>
#include <QScrollBar>
#include <QTextDocumentFragment>
#include "ApplicationOutput.h"
#include "IApplicationOutput.h"

namespace Main {

ApplicationOutput::ApplicationOutput(QWidget *parent) :
    QPlainTextEdit(parent)
{
    setReadOnly(true);
    setMouseTracking(true);
    _errFmt.setForeground(Qt::red);
    _normFmt.setForeground(Qt::black);
    _fileFind.setPattern(".*File \"(.+)?\", line (\\d+).*");
}

void ApplicationOutput::addOutput(const QString& line)
{
    addLine(line, _normFmt);
}

void ApplicationOutput::addError(const QString& line)
{
    addLine(line, _errFmt);
}

void ApplicationOutput::addLine(const QString& line, const QTextCharFormat& format)
{
    _mut.lock();
    QTextCursor cursor = textCursor();

    QString insertLine = line.endsWith("\n") ? line : line+"\n";

    moveCursor(QTextCursor::End);
    cursor.beginEditBlock();
    cursor.insertText(insertLine, format);
    int pos = 0;
    if ((pos = _fileFind.indexIn(line, pos)) != -1 ){
        QString file = _fileFind.cap(1);
        QString line = _fileFind.cap(2);

        QList<QTextEdit::ExtraSelection> extra = extraSelections();
        QTextEdit::ExtraSelection sel;
        sel.format.setUnderlineStyle(QTextCharFormat::SingleUnderline);
        cursor.movePosition(QTextCursor::PreviousCharacter, QTextCursor::MoveAnchor, insertLine.length()-_fileFind.pos(1));
        cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, file.length());
        sel.cursor = cursor;

        extra.append(sel);

        setExtraSelections(extra);

        QTextCharFormat fmt = cursor.charFormat();
        fmt.setAnchorHref(QString("%1::%2").arg(file).arg(line));
        cursor.setCharFormat(fmt);
    }


    cursor.endEditBlock();
    scrollToBottom();
    _mut.unlock();
}

void ApplicationOutput::scrollToBottom()
{
    verticalScrollBar()->setValue(verticalScrollBar()->maximum());
    verticalScrollBar()->setValue(verticalScrollBar()->maximum());
}

void ApplicationOutput::mouseMoveEvent(QMouseEvent *e)
{
    QTextCursor cur = cursorForPosition(e->pos());
    QString url = cur.charFormat().anchorHref();
    if (!url.isEmpty()){
        viewport()->setCursor(Qt::PointingHandCursor);
    } else {
        viewport()->setCursor(Qt::IBeamCursor);
    }

    QPlainTextEdit::mouseMoveEvent(e);
}

void ApplicationOutput::mousePressEvent(QMouseEvent * e)
{
    QTextCursor cur = cursorForPosition(e->pos());
    QString url = cur.charFormat().anchorHref();
    if (!url.isEmpty()){
        QStringList fileSplit = url.split("::");
        QString file = fileSplit[0];
        int line = fileSplit[1].toInt();
        emit jumpToFile(file, line);
    }
    QPlainTextEdit::mousePressEvent(e);
}

void ApplicationOutput::mouseReleaseEvent(QMouseEvent *e)
{
    QPlainTextEdit::mouseReleaseEvent(e);
}

}
