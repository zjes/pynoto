#ifndef _ISSUESTABLE_H_
#define _ISSUESTABLE_H_

#include <QTableView>

namespace Main {

class IssuesTable : public QTableView
{
    Q_OBJECT
public:
    IssuesTable(QWidget *parent = 0);
    virtual void setModel(QAbstractItemModel *model);
protected:
    virtual void resizeEvent(QResizeEvent *event);
public slots:
    virtual void currentChanged(const QModelIndex & current, const QModelIndex & previous);
signals:
    void activated(int);
};

}

#endif
