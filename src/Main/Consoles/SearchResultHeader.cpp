#include "SearchResultHeader.h"
#include "ui_SearchResultHeader.h"
#include "Include/Aux.h"
#include "Include/IProjectPlugin.h"
#include "Include/IProject.h"
#include "Include/IIconProvider.h"

namespace Main {

SearchResultHeader::SearchResultHeader(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::SearchResultHeader)
{
    _ui->setupUi(this);

    _project = Aux::plugin<Project::IProjectPlugin*>("project");
    connect(_project->project(), SIGNAL(loaded()), SLOT(projectLoaded()));
    connect(_project->project(), SIGNAL(unloaded()), SLOT(projectUnLoaded()));

    _ui->projectNameLbl->setText(tr("Unknown"));
    _ui->foundCountLbl->setText("0");
    _ui->searchBtn->setIcon(Aux::icons()->icon("search"));
}

SearchResultHeader::~SearchResultHeader()
{
    delete _ui;
}

void SearchResultHeader::projectLoaded()
{
    _ui->projectNameLbl->setText(_project->project()->name());
}

void SearchResultHeader::projectUnLoaded()
{
    _ui->projectNameLbl->setText(tr("Unknown"));
}

void SearchResultHeader::setMatchCount(int count)
{
    _ui->foundCountLbl->setText(QString::number(count));
}

}
