#include "ApplicationConsoleHeader.h"
#include "Include/Aux.h"
#include "Include/IIconProvider.h"
#include "ui_ApplicationConsoleHeader.h"

namespace Main {

ApplicationConsoleHeader::ApplicationConsoleHeader(QWidget * parent):
    QWidget(parent),
    _ui(new Ui::ApplicationConsoleHeader)
{
    _ui->setupUi(this);
    _ui->stopBtn->setIcon(Aux::icons()->icon("stop"));
    _ui->runBtn->setIcon(Aux::icons()->icon("run"));

    connect(_ui->runBtn, SIGNAL(clicked()), SIGNAL(run()));
    connect(_ui->stopBtn, SIGNAL(clicked()), SIGNAL(stop()));
}

ApplicationConsoleHeader::~ApplicationConsoleHeader()
{
    delete _ui;
}

void ApplicationConsoleHeader::projectRunned(bool runned)
{
    _ui->runBtn->setEnabled(!runned);
    _ui->stopBtn->setEnabled(runned);
}

}
