#ifndef _ISSUESCONSOLEHEADER_H_
#define _ISSUESCONSOLEHEADER_H_

#include <QWidget>

namespace Ui {
    class IssuesConsoleHeader;
}

namespace Main {

class IssuesConsoleHeader: public QWidget
{
    Q_OBJECT
public:
    IssuesConsoleHeader(QWidget * parent = 0);
    virtual ~IssuesConsoleHeader();
signals:
    void refresh();
private:
    Ui::IssuesConsoleHeader *_ui;
};

}

#endif
