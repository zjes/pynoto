#include <QDebug>
#include <QVBoxLayout>
#include "IssuesConsole.h"
#include "IssuesTable.h"
#include "IssuesConsoleHeader.h"

namespace Main {

IssuesConsole::IssuesConsole(QWidget *parent) :
    QWidget(parent),
    _table(new IssuesTable(this)),
    _header(new IssuesConsoleHeader(this))
{
    connect(_table, SIGNAL(activated(int)), SIGNAL(activated(int)));
    connect(_header, SIGNAL(refresh()), SIGNAL(refresh()));
}

void IssuesConsole::setModel(QAbstractItemModel *model)
{
    _table->setModel(model);
}

QWidget * IssuesConsole::header()
{
    return _header;
}

QWidget * IssuesConsole::control()
{
    return _table;
}

}
