#include "LogConsoleHeader.h"
#include "ui_LogConsoleHeader.h"
#include "Include/Aux.h"
#include "Include/IIconProvider.h"

namespace Main {

LogConsoleHeader::LogConsoleHeader(QWidget * parent):
    QWidget(parent),
    _ui(new Ui::LogConsoleHeader)
{
    _ui->setupUi(this);

    _ui->clearBtn->setIcon(Aux::icons()->icon("edit-clear"));
    _ui->clearBtn->setToolTip(tr("Clear console"));
    connect(_ui->clearBtn, SIGNAL(clicked()), SIGNAL(clearConsole()));
}

LogConsoleHeader::~LogConsoleHeader()
{
    delete _ui;
}

}
