#ifndef _APPLICATIONOUTPUT_H_
#define _APPLICATIONOUTPUT_H_

#include <QPlainTextEdit>
#include <QTextCharFormat>
#include <QMutex>

namespace Main {

class ApplicationOutput : public QPlainTextEdit
{
    Q_OBJECT
public:
    ApplicationOutput(QWidget *parent = 0);
    void addOutput(const QString& line);
    void addError(const QString& line);
signals:
    void jumpToFile(const QString& fileName, int line = 0);
protected:
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent * e);
    void mouseReleaseEvent(QMouseEvent *e);
private:
    void scrollToBottom();
    void addLine(const QString& line, const QTextCharFormat& format);
    QTextCharFormat _errFmt;
    QTextCharFormat _normFmt;
    QMutex _mut;
    QRegExp _fileFind;
};

}

#endif
