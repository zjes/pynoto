#ifndef _SEARCHRESULTHEADER_H_
#define _SEARCHRESULTHEADER_H_

#include <QWidget>

namespace Ui {
class SearchResultHeader;
}

namespace Project {
class IProjectPlugin;
}
namespace Main {

class SearchResultHeader : public QWidget
{
    Q_OBJECT
    
public:
    explicit SearchResultHeader(QWidget *parent = 0);
    virtual ~SearchResultHeader();
    void setMatchCount(int count);
private slots:
    void projectLoaded();
    void projectUnLoaded();
private:
    Ui::SearchResultHeader *_ui;
    Project::IProjectPlugin *_project;
};

}

#endif
