#ifndef _PREFERENCES_H_
#define _PREFERENCES_H_
#include "Include/IPreferencesPage.h"

namespace Main {

class Preferences: public IPreferencesPage
{
    Q_OBJECT
public:
    virtual ~Preferences();

    virtual QString title();
    virtual QIcon icon();
    virtual QWidget* widget(QWidget * parent);

    virtual bool apply();
};

}
#endif
