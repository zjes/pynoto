#include "Include/PynotoApplication.h"
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QTextCodec>
#endif
#include "Include/IMainPlugin.h"

int main(int argc, char *argv[])
{
    #if QT_VERSION < 0x050000
    QTextCodec::setCodecForCStrings( QTextCodec::codecForName("utf8") );
    QTextCodec::setCodecForTr(QTextCodec::codecForName("utf8"));
    #endif

    PynotoApplication a(argc, argv);
    a.setOrganizationName("PyNotoQt");
    a.setApplicationName("pynoto");

    a.pluginManager->plugin<Main::IMainPlugin*>("main");
    return a.exec();
}
