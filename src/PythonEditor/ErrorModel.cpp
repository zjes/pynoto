#include "ErrorModel.h"
#include "Include/Aux.h"
#include "Include/IIconProvider.h"

ErrorModel::ErrorModel(const QList<PythonCode::IPythonCode::ErrorItem> & list, QObject * parent):
    QAbstractTableModel(parent),
    _list(list)
{
    _warn = Aux::icons()->icon("warning");
    _info = Aux::icons()->icon("codestyle");
    _error = Aux::icons()->icon("error");
    _suggestion = Aux::icons()->icon("suggestion");
}

int ErrorModel::rowCount(const QModelIndex &/*parent*/) const
{
    return _list.length();
}

int ErrorModel::columnCount(const QModelIndex &/*parent*/) const
{
    return 3;
}

QVariant ErrorModel::data(const QModelIndex &index, int role) const
{
    PythonCode::IPythonCode::ErrorItem node = _list.at(index.row());
    if (role == Qt::DisplayRole){
        switch(index.column()){
        case 1:
            return node.message;
        case 2:
            return node.line + 1;
        }
        return QVariant();
    }
    if (role == Qt::DecorationRole){
        if (index.column() == 0){
            switch(node.severity){
            case PythonCode::IPythonCode::CodeStyle:
                return _info;
            case PythonCode::IPythonCode::Warning:
                return _warn;
            case PythonCode::IPythonCode::Error:
                return _error;
            case PythonCode::IPythonCode::Suggestion:
                return _suggestion;
            }
        }
        return QVariant();
    }
    if (role == Qt::EditRole)
        return node.line;
    return QVariant();
}
QVariant ErrorModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole){
        switch(section){
        case 1:
            return tr("Message");
        case 2:
            return tr("Line");
        }
    }
    return QVariant();
}

