#include <QDebug>
#include <QTimer>
#include "PythonDocumentLayout.h"

namespace PythonEditor {

PythonDocumentLayout::PythonDocumentLayout(QTextDocument *doc):
    TextEditor::BaseDocumentLayout(doc),
    _identationSize(4)
{
}

PythonDocumentLayout::~PythonDocumentLayout()
{
}

/*bool PythonDocumentLayout::canFold(const QTextBlock& block)
{
    QString text = block.text().trimmed();
    return text.startsWith("def") || text.startsWith("class") || multiLineStarts(block) || text.startsWith("#block");
}*/

bool PythonDocumentLayout::inString(const QTextBlock& block)
{
    /*QTextBlock prev = block.previous();
    while(prev.isValid()){
        if (multiLineStarts(prev)){
            break;
        }
        prev = prev.previous();
    }
    if (prev.isValid()){
        return prev.blockNumber()+calcMultilineFoldLength(prev) > block.blockNumber();
    }*/
    return false;
}

int PythonDocumentLayout::foldingIndent(const QTextBlock& block)
{
    TextEditor::TextBlockData *data = static_cast<TextEditor::TextBlockData *>(block.userData());
    int ident = data->_foldingIndent/_identationSize;
    return ident;
}

int PythonDocumentLayout::spaces(const QTextBlock& block)
{
    TextEditor::TextBlockData *data = static_cast<TextEditor::TextBlockData *>(block.userData());
    return data->_foldingIndent;
}

}
