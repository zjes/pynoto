#include "PythonEditorImpl.h"
#include "PythonEditorWidget.h"

namespace PythonEditor {

PythonEditorImpl::PythonEditorImpl(PythonEditorWidget* widget, QWidget* parent, bool sample):
    TextEditor::TextEditorImpl(widget, parent, sample)
{
    if (sample){
        _widget->setFileName(":/sample.py");
    }
}

QAbstractTableModel* PythonEditorImpl::errorListModel()
{
    return qobject_cast<PythonEditorWidget*>(_widget)->errorListModel();
}

void PythonEditorImpl::checkIssues()
{
    qobject_cast<PythonEditorWidget*>(_widget)->checkIssues();
}

}
