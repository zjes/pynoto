#ifndef _PYTHONCOMPLITER_H_
#define _PYTHONCOMPLITER_H_
#include <QObject>
#include "Include/IPythonCode.h"

namespace Project { class IProject; }

namespace PythonEditor {

class PythonEditorWidget;
class ComplititionList;

typedef PythonCode::IPythonCode::ErrorItem ErrorItem;
typedef PythonCode::IPythonCode::Completition Completition;
class PythonCompliter: public QObject
{
    Q_OBJECT
public:
    PythonCompliter(PythonEditorWidget * parent = NULL);
    void showComplit(int pos);
    void hide();
    bool isVisible();
    void findDeclaration(int pos);
public slots:
    void onErrors(const QString& fileName, const QList<ErrorItem>& items);
    void onComplit(const QString& fileName, int pos, const QString& word, const QList<Completition>& items);
    void onDeclaration(const QString& fileName, int offset, const QString& word, const QString& url, int wbegin, int wend);
    void initPyCode();
    void checkErrors();
private:
    PythonEditorWidget * _editor;
    PythonCode::IPythonCode *_python;
    ComplititionList *_list;
    Project::IProject *_project;
};

}

#endif
