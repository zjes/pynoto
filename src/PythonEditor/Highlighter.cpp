#include <QDebug>
#include "Highlighter.h"
#include "Settings.h"

namespace PythonEditor {

Highlighter::Highlighter(QTextDocument *parent, const QString &lexer):
    TextEditor::Highlighter(parent, lexer)
{

}

void Highlighter::init(const QString &themeName)
{
    Settings set;
    Theme::ITheme *theme = themeName.isEmpty() ? set.theme() : set.themeByName(themeName);

    TextEditor::Highlighter::init(themeName);
    _formats.insert("Normal Text"           , theme->defFormat(_lexer, "NormalText", "common/dsNormal"));
    _formats.insert("Definition Keyword"    , theme->defFormat(_lexer, "DefinitionKeyword", "default/dsKeyword", true));
    _formats.insert("Operator"              , theme->defFormat(_lexer, "Operator", "default/dsKeyword", true));
    _formats.insert("String Substitution"   , theme->format(_lexer, "StringSubstitution", QColor("#0057ae")));
    _formats.insert("Command Keyword"       , theme->defFormat(_lexer, "CommandKeyword", "default/dsKeyword"));
    _formats.insert("Flow Control Keyword"  , theme->defFormat(_lexer, "FlowControlKeyword", "default/dsKeyword"));
    _formats.insert("Builtin Function"      , theme->defFormat(_lexer, "BuiltinFunction", "default/dsDataType"));
    _formats.insert("Special Variable"      , theme->defFormat(_lexer, "SpecialVariable", "default/dsOthers"));
    _formats.insert("Extensions"            , theme->format(_lexer, "Extensions", QColor("#0095ff"), true));
    _formats.insert("Exceptions"            , theme->format(_lexer, "Exceptions", QColor("#054d00"), true));
    _formats.insert("Overloaders"           , theme->format(_lexer, "Overloaders", QColor("#000e52"), true));
    _formats.insert("Preprocessor"          , theme->defFormat(_lexer, "Preprocessor", "default/dsChar"));
    _formats.insert("String Char"           , theme->defFormat(_lexer, "StringChar", "default/dsChar"));
    _formats.insert("Float"                 , theme->defFormat(_lexer, "Number", "default/dsNumber"));
    _formats.insert("Int"                   , theme->defFormat(_lexer, "Number", "default/dsNumber"));
    _formats.insert("Hex"                   , theme->defFormat(_lexer, "Number", "default/dsNumber"));
    _formats.insert("Octal"                 , theme->defFormat(_lexer, "Number", "default/dsNumber"));
    _formats.insert("Complex"               , theme->defFormat(_lexer, "Complex", "default/dsOthers"));
    _formats.insert("Comment"               , theme->defFormat(_lexer, "Comment", "default/dsComment"));
    _formats.insert("String"                , theme->defFormat(_lexer, "String", "default/dsString"));
    _formats.insert("Raw String"            , theme->defFormat(_lexer, "RawString", "default/dsString"));
    _formats.insert("Decorator"             , theme->format(_lexer, "Decorator", QColor("#8f6b32")));
    _formats.insert("BlockRegion"           , theme->defFormat(_lexer, "BlockRegion", "default/dsComment"));
}

}
