#ifndef _COMPLITITIONLIST_H_
#define _COMPLITITIONLIST_H_
#include <QListWidget>
#include <QCompleter>
#include "Include/IPythonCode.h"

namespace PythonEditor {
class PythonEditorWidget;
class ComplititionList: public QCompleter
{
    Q_OBJECT
public:
    ComplititionList(PythonEditorWidget * editor, QWidget* parent);
    void complete(int pos, const QString& word,  const QList<PythonCode::IPythonCode::Completition>& items);
protected:
    //virtual bool event(QEvent *);
private slots:
    void onEditorCursorChanged();
    void onInsertCompletion(const QString& text);
private:
    PythonEditorWidget * _editor;
    int _pos;
    QString _word;
    QMap<PythonCode::IPythonCode::CompletitionType, QIcon> _icons;
};

}
#endif
