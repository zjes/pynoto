#ifndef _PYTHONEDITORIMPL_H_
#define _PYTHONEDITORIMPL_H_
#include "TextEditor/TextEditorImpl.h"

namespace PythonEditor
{

class PythonEditorWidget;
class PythonEditorImpl : public TextEditor::TextEditorImpl
{
    Q_OBJECT
public:
    PythonEditorImpl(PythonEditorWidget* widget, QWidget* parent, bool sample = false);

    virtual QAbstractTableModel* errorListModel();
public slots:
    virtual void checkIssues();
};

}
#endif
