#ifndef _PYDOCUMENTLAYOUT_H_
#define _PYDOCUMENTLAYOUT_H_
#include <QTextBlock>
#include <QTimer>
#include "TextEditor/BaseDocumentLayout.h"

namespace PythonEditor {

class PythonDocumentLayout: public TextEditor::BaseDocumentLayout
{
    Q_OBJECT
public:
    PythonDocumentLayout(QTextDocument *doc);
    virtual ~PythonDocumentLayout();
public:
    int foldingIndent(const QTextBlock& block);
    int spaces(const QTextBlock& block);
    bool inString(const QTextBlock& block);
signals:
    void rehightBlock(const QList<QTextBlock>& blocks);
    void linesCounted();
private:
    int _identationSize;
};

}
#endif
