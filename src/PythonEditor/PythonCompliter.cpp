#include <QDebug>
#include "Include/Aux.h"
#include "Include/IProjectPlugin.h"
#include "PythonCompliter.h"
#include "PythonEditorWidget.h"
#include "ComplititionList.h"

namespace PythonEditor {

PythonCompliter::PythonCompliter(PythonEditorWidget* parent):
    QObject(parent),
    _editor(parent),
    _list(NULL)
{
    _project = Aux::plugin<Project::IProjectPlugin*>("project")->project();
    connect(_project, SIGNAL(interpretChanged()), SLOT(initPyCode()));
    connect(_project, SIGNAL(lintChanged()), SLOT(checkErrors()));
    _list = new ComplititionList(_editor, _editor->viewport());
    initPyCode();
}

void PythonCompliter::initPyCode()
{
    _python = _project->pythonCode();
    if (_python){
        connect(_python, SIGNAL(errors(QString,QList<ErrorItem>)), SLOT(onErrors(QString,QList<ErrorItem>)));
        connect(_python, SIGNAL(complitition(QString,int,QString,QList<Completition>)), SLOT(onComplit(QString,int,QString,QList<Completition>)));
        connect(_python, SIGNAL(declaration(QString,int,QString,QString,int,int)), SLOT(onDeclaration(QString,int,QString,QString,int,int)));
    }
}

void PythonCompliter::checkErrors()
{
    if (_python)
        _python->errorList(_editor->fileName(), _editor->content());
}

void PythonCompliter::showComplit(int pos)
{
    if (_python)
        _python->codeComplit(_editor->fileName(), _editor->content(), pos);
}

void PythonCompliter::hide()
{
}

void PythonCompliter::onErrors(const QString& fileName, const QList<PythonCode::IPythonCode::ErrorItem>& items)
{
    if (fileName == _editor->fileName()){
        _editor->setErrors(items);
    }
}

void PythonCompliter::onComplit(const QString& fileName, int pos, const QString& word, const QList<Completition>& items)
{
    if (fileName != _editor->fileName())
        return;
    hide();
    _list->complete(pos, word, items);
}

bool PythonCompliter::isVisible()
{
    return _list->popup()->isVisible();
}

void PythonCompliter::findDeclaration(int pos)
{
    if (_python)
        _python->findDeclaration(_editor->fileName(), _editor->content(), pos);
}

void PythonCompliter::onDeclaration(const QString& fileName, int offset, const QString& word, const QString& url, int wbegin, int wend)
{
    if (fileName != _editor->fileName())
        return;
    _editor->declaration(wbegin, wend, offset, url);
}

}
