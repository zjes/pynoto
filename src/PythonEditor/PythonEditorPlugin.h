#ifndef _PYTHONEDITORPLUGIN_H_
#define _PYTHONEDITORPLUGIN_H_
#include "Include/IEditorPlugin.h"

namespace PythonEditor {

class PythonEditorPlugin: public Editors::IEditorPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Pynoto.IEditorPlugin")
    Q_INTERFACES(Editors::IEditorPlugin)
public:
    virtual ~PythonEditorPlugin(){}
    virtual Editors::IEditor * createEditor(const QString& fileName, QWidget* parent, bool sample = false);
    virtual bool initialize();
};

}
#endif
