#ifndef _PYHIGHLIGHTER_H_
#define _PYHIGHLIGHTER_H_
#include "TextEditor/Highlighter/Highlighter.h"

namespace PythonEditor {

class Highlighter: public TextEditor::Highlighter
{
    Q_OBJECT
public:
    Highlighter(QTextDocument *parent, const QString &lexer);
    virtual void init(const QString &themeName = "");
};

}

#endif
