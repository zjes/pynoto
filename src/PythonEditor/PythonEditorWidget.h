#ifndef _PYTHONEDITORWIDGET_H_
#define _PYTHONEDITORWIDGET_H_
#include <QTimer>
#include "TextEditor/TextEditorWidget.h"
#include "Include/IPythonCode.h"

class QAbstractTableModel;

namespace PythonEditor {

class PythonCompliter;
class Highlighter;

class PythonEditorWidget: public TextEditor::TextEditorWidget
{
    Q_OBJECT
public:
    PythonEditorWidget(const QString& fileName, bool sample = false, QWidget* parent = NULL);
    virtual void editorLoaded();
    virtual TextEditor::Highlighter* hightlighter();
    void setErrors(const QList<PythonCode::IPythonCode::ErrorItem>& items);
    QAbstractTableModel* errorListModel();
    void declaration(int wordBegin, int wordEnd, int offset, const QString& url);
public slots:
    virtual bool reset();
    virtual void checkIssues();
protected:
    virtual void paintEvent(QPaintEvent *e);
    virtual void keyPressEvent(QKeyEvent *e);
    virtual void keyReleaseEvent(QKeyEvent *e);
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void mousePressEvent(QMouseEvent *e);
    virtual void focusOutEvent(QFocusEvent *e);
private slots:
    void onTextChanged();
protected slots:
    virtual void preferencesChanged(const QString& what);
private:
    void autoIndentNewLine();
private:
    PythonCompliter * _compliter;
    QAbstractTableModel * _errorListModel;
    bool _showIdent;
    bool _isSample;
    QTimer _updateErrorsTimer;
    QColor _identColor;
    struct CodeDecl {
        QString wordHight;
        QString file;
        int offset;
        int wbegin;
        int wend;
    } _codeDecl;
};

}

#endif
