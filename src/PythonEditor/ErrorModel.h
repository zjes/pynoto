#ifndef _ERRORMODEL_H_
#define _ERRORMODEL_H_
#include <QAbstractTableModel>
#include <QIcon>
#include "Include/IPythonCode.h"

class ErrorModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    ErrorModel(const QList<PythonCode::IPythonCode::ErrorItem> & list, QObject * parent = NULL);

    int rowCount(const QModelIndex &/*parent*/) const;
    int columnCount(const QModelIndex &/*parent*/) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
private:
    QList<PythonCode::IPythonCode::ErrorItem> _list;
    QIcon _warn;
    QIcon _info;
    QIcon _error;
    QIcon _suggestion;
};

#endif // ERRORMODEL_H
