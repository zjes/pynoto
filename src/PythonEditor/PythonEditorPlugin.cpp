#include <QDebug>
#include <QtPlugin>
#include "PythonEditorPlugin.h"
#include "PythonEditorImpl.h"
#include "PythonEditorWidget.h"

namespace PythonEditor {

Editors::IEditor * PythonEditorPlugin::createEditor(const QString& fileName, QWidget* parent, bool sample)
{
    return new PythonEditorImpl(new PythonEditorWidget(fileName, sample), parent, sample);
}

bool PythonEditorPlugin::initialize()
{
    initialized = true;
    return true;
}

}
