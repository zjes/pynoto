pyNoto
======

PyQt / python editor with embeded Designer like qtcreator.
This is a pre-alpha version. Some functional is not implemented yet. Use it at your own risk. 

Features:

1. Autocomplete
2. Error checking
3. Python 3 projects' support
4. Code navigation
5. Embeded Qt Designer
6. Color themes
7. Run projects 
8. Etc...

Screenshots:
![pynoto.png](https://bitbucket.org/repo/ekbnBK/images/4159662972-pynoto.png)